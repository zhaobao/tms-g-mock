package com.eaero;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;

import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.text.StrFormatter;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.eaero.entity.*;
import com.eaero.service.*;
import com.eaero.service.pick.PickService;
import com.eaero.service.util.FileUtil;
import com.eaero.util.IdGenUtils;
import com.google.gson.Gson;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * @author luofeng
 * @create 2023/2/24 11:11
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class PickTest {

    private Gson gson = new Gson();

    @Autowired
    PickService pickService;

    @Test
    public void getPick() {
        CommonResp commonResp = new CommonResp();
        commonResp.setRetCode("0");
        commonResp.setRetMsg("成功");
        commonResp.setDataList(pickService.getToolPick());
        try {
            FileUtil.writeFile("Pick.json", gson.toJson(commonResp));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getPickDetail() {
        CommonResp commonResp = new CommonResp();
        commonResp.setRetCode("0");
        commonResp.setRetMsg("成功");
        commonResp.setDataList(pickService.getPickDetail());
        try {
            FileUtil.writeFile("PickDetail.json", gson.toJson(commonResp));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Autowired
    private ITraceToolService toolService;
    @Autowired
    private ITraceToolDeliverService deliverService;
    @Autowired
    private ITraceToolDeliverDetailService deliverDetailService;
    @Autowired
    private ITraceUserService userService;
    @Autowired
    private ITraceWarehouseService warehouseService;

    @Test
    public void mockDeliver() {
        List<TraceTool> tools = toolService.list(Wrappers.<TraceTool>lambdaQuery().last("limit 5"));
        List<TraceToolDeliver> delivers = deliverService.list();
        List<TraceUser> users = userService.list();
        List<TraceWarehouse> warehouses = warehouseService.list();

        Map<String, TraceWarehouse> warehouseMap = CollStreamUtil.toMap(warehouses, TraceWarehouse::getWarehouse, Function.identity());
        List<TraceToolDeliverDetail> deliverDetails = new ArrayList<>();
        for (TraceToolDeliver deliver : delivers) {
            int i = RandomUtil.randomInt(0, users.size());
            TraceUser user = users.get(i);
            i = RandomUtil.randomInt(0, users.size());
            String userCode1 = StrFormatter.format("{} {}", user.getUserCode(), user.getUserName());
            user = users.get(i);
            String userCode2 = StrFormatter.format("{} {}", user.getUserCode(), user.getUserName());
            for (TraceTool tool : tools) {

                TraceToolDeliverDetail deliverDetail = new TraceToolDeliverDetail();
                deliverDetail.setId(IdGenUtils.genId());
                deliverDetail.setDeliverCode(deliver.getDeliverCode());
                deliverDetail.setWorkorderno("9527");
                deliverDetail.setJobcard("9527-01");
                deliverDetail.setPartno(tool.getPartno());
                deliverDetail.setSerialno("");
                deliverDetail.setPartdescr(tool.getPartdescr());
                deliverDetail.setCatalogueType(tool.getCatalogueType());
                deliverDetail.setTailNo(deliver.getTailNo());
                deliverDetail.setVisitDesc("A");
                deliverDetail.setIssueUser(deliver.getCreateUser());
                deliverDetail.setIssueDate(deliver.getCreateDate());
                deliverDetail.setWarehouse(deliver.getWarehouse());
                deliverDetail.setUpdateUser(userCode1);
                deliverDetail.setUpdateDate(DateUtil.format(DateTime.now(), "YYYY-MM-dd HH:mm:ss"));
                deliverDetail.setReqBy(userCode2);
                deliverDetail.setComMobile(user.getComMobile());
                deliverDetail.setStatus(deliver.getStatus());
                deliverDetail.setDeliverUser(user.getUserCode());
                deliverDetail.setDeliverDate(DateUtil.format(DateTime.now(), "YYYY-MM-dd HH:mm:ss"));
                deliverDetail.setTlFlag("");
                deliverDetail.setImgFlag("N");
                deliverDetail.setRid(tool.getRid());
                deliverDetail.setUnitofmeas(tool.getUnitofmeas());
                deliverDetail.setBarcode("");
                deliverDetail.setRfidId(tool.getRfidId());
                deliverDetail.setParentPartno(tool.getParentPartno());
                deliverDetail.setParentTrno(tool.getParentTrno());
                deliverDetail.setParentId(tool.getRid());
                deliverDetail.setWhseDesc(tool.getWhseDesc());
                deliverDetail.setStation(warehouseMap.get(deliver.getWarehouse()).getStation());
                deliverDetail.setAlternatePartno(tool.getPartno());
                deliverDetail.setStoresloc(tool.getStoresloc());
                deliverDetail.setChildNum(StrUtil.str(tool.getChildNum(), StandardCharsets.UTF_8));
                deliverDetail.setMeasuringNo(tool.getMeasuringNo());
                deliverDetail.setStandByPn(tool.getStandByPn());
                deliverDetail.setTlStatus(tool.getTlStatus());
                deliverDetail.setCreateTime(LocalDateTime.now());
                deliverDetail.setModifyTime(LocalDateTime.now());
                deliverDetail.setReqNo("700000063");
                deliverDetail.setEmpCode(userCode1);
                deliverDetail.setCreateDate(DateUtil.format(DateTime.now(), "YYYY-MM-dd HH:mm:ss"));
                deliverDetail.setTpNo("");
                deliverDetail.setVid("3");
                deliverDetail.setOpDate(DateUtil.format(DateTime.now(), "YYYY-MM-dd HH:mm:ss"));
                deliverDetail.setIssueNum("");
                deliverDetail.setTdId("10");
                deliverDetail.setCreateUser(userCode2);
                deliverDetail.setMechanic("");
                deliverDetail.setIssueRid("68772");

                deliverDetails.add(deliverDetail);
            }
        }
        deliverDetailService.saveBatch(deliverDetails);
    }
}
