package com.eaero;

import com.eaero.service.deliverd.DeliverService;
import com.eaero.service.util.FileUtil;
import com.google.gson.Gson;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author luofeng
 * @create 2023/2/24 11:12
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class DeliverTest {

    private Gson gson = new Gson();

    @Autowired
    DeliverService deliverService;

    @Test
    public void getPick() {
        CommonResp commonResp = new CommonResp();
        commonResp.setRetCode("0");
        commonResp.setRetMsg("成功");
        commonResp.setDataList(deliverService.getDeliverDetail());
        try {
            FileUtil.writeFile("Deliver.json", gson.toJson(commonResp));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
