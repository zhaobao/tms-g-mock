package com.eaero;

import cn.hutool.core.date.DateUtil;
import com.eaero.entity.TraceTool;
import com.eaero.entity.TraceWarehouse;
import com.eaero.service.ITraceToolService;
import com.eaero.service.ITraceWarehouseService;
import com.eaero.service.tool.pojo.ToolInfo;
import com.eaero.service.util.FileUtil;
import com.eaero.util.IdGenUtils;
import com.google.gson.Gson;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zb
 * @date 2023/02/24 024 13:30
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class ToolTest {
    private Gson gson = new Gson();
    @Autowired
    ITraceWarehouseService warehouseService;

    @Autowired
    ITraceToolService traceToolService;
    @Test
    public void addTool() {
        List<TraceTool> toolInfoList = new ArrayList<>();
        suitTool(toolInfoList,"warehouse1");
        traceToolService.saveBatch(toolInfoList);
    }
    private void suitTool(List<TraceTool> toolInfoList, String warehouse) {
        List<TraceTool> parentTool = new ArrayList<>();
        List<TraceWarehouse> warehouseList = warehouseService.getWarehouseInfo(warehouse);
        String time = DateUtil.format(LocalDateTime.now(), "yyyyMMddHHmmss");
        String storesloc = "location1";
        for (int i = 1; i <= 2; i++) {
            TraceTool toolInfo = new TraceTool();
            toolInfo.setId(IdGenUtils.genId());
            toolInfo.setWarehouse(warehouseList.get(0).getWarehouse());
            toolInfo.setWhseDesc(warehouseList.get(0).getCommentText());
            toolInfo.setRid("RID" + time + "_" + i);
            toolInfo.setPartno("JHP" + time + "_" + i);
            toolInfo.setParentPartno("");
            toolInfo.setTrno("KHP" + time + "_" + i);
            toolInfo.setParentTrno("");
            toolInfo.setPartdescr("父工具" + "_" + i);
            toolInfo.setRfidId("RFID" + time + "_" + i);
            toolInfo.setBarcode("barcode" + time + "_" + i);
            toolInfo.setIssueRemind("");
            toolInfo.setUnitofmeas("EA");
            toolInfo.setSearchKeyTl("");
            toolInfo.setMeasuringNo("计量号" + i);
            //工具类型：SPTL/CMTL
            toolInfo.setCatalogueType(i % 2 == 1 ? "SPTL" : "CMTL");
            toolInfo.setStandByPn("TDJH" + time+ "_" + i);
            toolInfo.setStoresloc(storesloc);
            toolInfo.setChildNum(5);
            toolInfo.setTlFlag("");
            toolInfo.setImgFlag("Y");
            toolInfo.setTlStatus("在库");
            toolInfo.setOnhand(6);
            parentTool.add(toolInfo);
        }
        List<TraceTool> childTool = new ArrayList<>();
        for (TraceTool parent : parentTool) {
            for (int i = 1; i <= 5; i++) {
                TraceTool toolInfoChild = new TraceTool();
                toolInfoChild.setId(IdGenUtils.genId());
                toolInfoChild.setWarehouse(parent.getWarehouse());
                toolInfoChild.setWhseDesc(parent.getWhseDesc());
                toolInfoChild.setRid(parent.getRid() + "_C" + i);
                toolInfoChild.setPartno(parent.getPartno() + "_C" + i);
                toolInfoChild.setParentPartno(parent.getPartno());
                toolInfoChild.setTrno(parent.getTrno() + "_C" + i);
                toolInfoChild.setParentTrno(parent.getTrno());
                toolInfoChild.setPartdescr("子工具" + i);
                toolInfoChild.setRfidId(parent.getRfidId() + "_C" + i);
                toolInfoChild.setBarcode(parent.getBarcode() + "_C" + i);
                toolInfoChild.setIssueRemind("");
                toolInfoChild.setUnitofmeas("EA");
                toolInfoChild.setSearchKeyTl("");
                toolInfoChild.setMeasuringNo("计量号" + i);
                //工具类型：SPTL/CMTL
                toolInfoChild.setCatalogueType(i % 2 == 1 ? "SPTL" : "CMTL");
                toolInfoChild.setStandByPn("替代件号" + parent.getStandByPn() + "_C" + i);
                toolInfoChild.setStoresloc(parent.getStoresloc());
                toolInfoChild.setChildNum(0);
                toolInfoChild.setTlStatus("在库");
                //故障状态（空值/故障信息）
                toolInfoChild.setTlFlag("");
                toolInfoChild.setImgFlag("Y");
                toolInfoChild.setOnhand(3);
                childTool.add(toolInfoChild);
            }
        }
        toolInfoList.addAll(parentTool);
        toolInfoList.addAll(childTool);
    }

}
