package com.eaero;

import com.eaero.service.loanTool.LoanToolService;
import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

/**
 * @author zb
 * @date 2023/03/03 003 15:29
 */
@SpringBootTest
public class LoanToolTest {
    private Gson gson = new Gson();
    @Autowired
    LoanToolService loanToolService;

    @Test
    void getAllLoanTool() throws IOException {
        CommonResp resp = loanToolService.getLoanToolList();
        String json = gson.toJson(resp);
        System.out.println(json);
    }
}
