package com.eaero;

import com.eaero.service.user.UserInfoService;
import com.eaero.service.user.pojo.UserInfo;
import com.eaero.service.util.FileUtil;
import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;
import java.util.List;

/**
 * Description:
 *
 * @author Ye Zhihong
 * @date 2023/2/24 10:41
 */
@SpringBootTest
public class UserInfoTest {
    private Gson gson = new Gson();
    @Autowired
    UserInfoService userInfoService;

    @Test
    void testUserInfo() throws IOException {
        CommonResp commonResp = new CommonResp();
        List<UserInfo> userInfoAll = userInfoService.getUserInfoAll();
        commonResp.setRetCode("0");
        commonResp.setRetMsg("成功");
        commonResp.setDataList(userInfoAll);
        String json = gson.toJson(commonResp);
        System.out.println(gson.toJson(commonResp));

        FileUtil.writeFile("UserInfoAll.json", json);
    }

    @Test
    void testGetUserInfoAllByUserCode() throws IOException {
        CommonResp commonResp = new CommonResp();
        List<UserInfo> userInfoAll = userInfoService.getUserInfoAllByUserCode("user001");
        commonResp.setRetCode("0");
        commonResp.setRetMsg("成功");
        commonResp.setDataList(userInfoAll);
        String json = gson.toJson(commonResp);
        System.out.println(gson.toJson(commonResp));

        FileUtil.writeFile("UserInfoByUserCode.json", json);
    }
}
