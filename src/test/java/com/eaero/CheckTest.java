package com.eaero;

import com.eaero.service.check.CheckService;
import com.eaero.service.check.pojo.CheckInfo;
import com.eaero.service.check.pojo.CheckInfoDetail;
import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;
import java.util.List;

/**
 * @description: 盘点
 * @author: WBL
 * @date: 2023-02-24
 * @time: 17:52
 */
@SpringBootTest
public class CheckTest {

    @Autowired
    private CheckService checkService;

    private Gson gson = new Gson();

    @Test
    void test1() throws IOException {
        CommonResp com = new CommonResp();
        List<CheckInfo> checkInfo = checkService.getCheckInfo();
        com.setRetCode("0");
        com.setRetMsg("成功");
        com.setDataList(checkInfo);
        String json = gson.toJson(com);

        List<CheckInfoDetail> detailAll = checkService.getCheckInfoDetailAll(checkInfo);
        CommonResp comm = new CommonResp();
        comm.setRetCode("0");
        comm.setRetMsg("成功");
        comm.setDataList(detailAll);
        String detailListJson = gson.toJson(comm);

        String infoListJson = gson.toJson(checkInfo);
        String dateilJson = gson.toJson(detailAll);

        File orderFile = new File("src/main/resources/checkInfos.json");
        FileOutputStream f = new FileOutputStream(orderFile);
        BufferedWriter b = new BufferedWriter(new OutputStreamWriter(f));
        b.write(infoListJson);

        File file = new File("src/main/resources/checkInfoAll.json");
        FileOutputStream f1 = new FileOutputStream(file);
        BufferedWriter b1 = new BufferedWriter(new OutputStreamWriter(f1));
        b1.write(json);

        File detailListFile = new File("src/main/resources/checkInfoDetailAll.json");
        FileOutputStream f2 = new FileOutputStream(detailListFile);
        BufferedWriter b2 = new BufferedWriter(new OutputStreamWriter(f2));
        b2.write(detailListJson);

        File detailFile = new File("src/main/resources/checkInfoDetails.json");
        FileOutputStream f3 = new FileOutputStream(detailFile);
        BufferedWriter b3 = new BufferedWriter(new OutputStreamWriter(f3));
        b3.write(dateilJson);

        b.close();
        b1.close();
        b2.close();
        b3.close();
    }

    @Test
    void test2() throws IOException {
        List<CheckInfo> checkInfoAll = checkService.getCheckInfoAll();
        System.out.println(gson.toJson(checkInfoAll));
    }

    @Test
    void test3() throws IOException {
        List<CheckInfoDetail> checkInfoDetail = checkService.getCheckInfoDetail("RT40542827");
        System.out.println(gson.toJson(checkInfoDetail));
    }
}
