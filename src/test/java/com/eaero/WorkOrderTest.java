package com.eaero;

import com.eaero.service.ITraceWorkOrderDetailService;
import com.eaero.service.ITraceWorkOrderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.*;
import java.time.format.DateTimeFormatter;


/**
 * @description:
 * @author: WBL
 * @date: 2023-02-24
 * @time: 13:26
 */
@SpringBootTest
public class WorkOrderTest {
    @Autowired
    private ITraceWorkOrderService traceWorkOrderService;

    @Autowired
    private ITraceWorkOrderDetailService workOrderDetailService;
    @Test
    void genWorkOrder(){
        //traceWorkOrderService.genWorkOrder();
        LocalDateTime localDate = LocalDateTime.parse("2023-03-08 15:34:07", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:ss:mm"));

        System.out.println(localDate.toInstant(ZoneOffset.UTC).toEpochMilli());
        long f = 1678246127;
        System.out.println(LocalDateTime.ofInstant(Instant.ofEpochMilli(f), ZoneId.systemDefault()));
    }

    @Test
    void genWorkOrderDetail(){
        workOrderDetailService.genWorkOrderDetail();
    }
}
