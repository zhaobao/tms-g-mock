package com.eaero;

import com.eaero.service.ITraceHandoverDetailService;
import com.eaero.service.ITraceHandoverService;
import com.eaero.service.handover.WarehouseHandoverService;
import com.eaero.service.handover.pojo.Shift;
import com.eaero.service.handover.pojo.ShiftOrder;
import com.eaero.service.util.FileUtil;
import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.List;

/**
 * Description:
 *
 * @author Ye Zhihong
 * @date 2023/2/24 14:53
 */
@SpringBootTest
public class WarehouseHandoverTest {
    private Gson gson = new Gson();
    @Autowired
    private WarehouseHandoverService warehouseHandoverService;

    @Autowired
    private ITraceHandoverService traceHandoverService;

    @Autowired
    private ITraceHandoverDetailService traceHandoverDetailService;

    /**
     * 查询交接班单
     */
    @Test
    void testGetHandoverInfoAll() throws IOException {
        CommonResp commonResp = new CommonResp();
        List<Shift> handoverInfoAll = warehouseHandoverService.getHandoverInfoAll();
        commonResp.setRetCode("0");
        commonResp.setRetMsg("成功");
        commonResp.setDataList(handoverInfoAll);
        String json = gson.toJson(commonResp);
        System.out.println(json);

        FileUtil.writeFile("HandoverInfoAll.json", json);
    }

    /**
     * 查询交接班明细
     */
    @Test
    void testGetHandoverDtlInfoAll() throws IOException {
        CommonResp commonResp = new CommonResp();
        List<ShiftOrder> handoverDtlInfoAll = warehouseHandoverService.getHandoverDtlInfoAll();
        commonResp.setRetCode("0");
        commonResp.setRetMsg("成功");
        commonResp.setDataList(handoverDtlInfoAll);
        String json = gson.toJson(commonResp);
        System.out.println(json);

        FileUtil.writeFile("HandoverDtlInfoAll.json", json);
    }

    /**
     * 交班信息mock
     */
    @Test
    void testHandoverDtlInfoAll() {

    }
}
