package com.eaero;

import com.eaero.service.tool.InventoryService;
import com.eaero.service.tool.pojo.InventoryInfo;
import com.eaero.service.util.FileUtil;
import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.List;

/**
 * @author zzj
 * @date 2023/2/24 16:36
 * @description
 */
@SpringBootTest
public class InventoryInfoTest {

    private Gson gson = new Gson();
    @Autowired
    InventoryService inventoryService;

    @Test
    void getAllInventory() throws IOException {
        CommonResp commonResp = new CommonResp();
        List<InventoryInfo> allInventory = inventoryService.getAllInventory();
        commonResp.setRetCode("0");
        commonResp.setRetMsg("成功");
        commonResp.setDataList(allInventory);
        String json = gson.toJson(commonResp);
        System.out.println(json);
        FileUtil.writeFile("AllInventory.json", json);
    }

    @Test
    void getInventoryByRid() throws IOException {
        CommonResp commonResp = new CommonResp();
        List<InventoryInfo> inventoryInfos = inventoryService.getInventoryByRid("20230224001");
        commonResp.setRetCode("0");
        commonResp.setRetMsg("成功");
        commonResp.setDataList(inventoryInfos);
        String json = gson.toJson(commonResp);
        System.out.println(json);
        //FileUtil.writeFile("InventoryByRid.json", json);
    }

    @Test
    void getInventoryByRfidId() throws IOException {
        CommonResp commonResp = new CommonResp();
        List<InventoryInfo> inventoryInfos = inventoryService.getInventoryByRfidId("RFID20230224002");
        commonResp.setRetCode("0");
        commonResp.setRetMsg("成功");
        commonResp.setDataList(inventoryInfos);
        String json = gson.toJson(commonResp);
        System.out.println(json);
        //FileUtil.writeFile("InventoryByRfidId.json", json);
    }

    @Test
    void getInventoryByPartnoAndTrno() throws IOException {
        CommonResp commonResp = new CommonResp();
        List<InventoryInfo> inventoryInfos = inventoryService.getInventoryByPartnoAndTrno("003", "003");
        commonResp.setRetCode("0");
        commonResp.setRetMsg("成功");
        commonResp.setDataList(inventoryInfos);
        String json = gson.toJson(commonResp);
        System.out.println(json);
        //FileUtil.writeFile("InventoryByPartnoAndTrno.json", json);
    }

    @Test
    void getInventoryByPartdescr() throws IOException {
        CommonResp commonResp = new CommonResp();
        List<InventoryInfo> inventoryInfos = inventoryService.getInventoryByPartdescr("工具5");
        commonResp.setRetCode("0");
        commonResp.setRetMsg("成功");
        commonResp.setDataList(inventoryInfos);
        String json = gson.toJson(commonResp);
        System.out.println(json);
        //FileUtil.writeFile("InventoryByPartdescr.json", json);
    }

    @Test
    void getInventoryByBarcode() throws IOException {
        CommonResp commonResp = new CommonResp();
        List<InventoryInfo> inventoryInfos = inventoryService.getInventoryByBarcode("barcode20230224007");
        commonResp.setRetCode("0");
        commonResp.setRetMsg("成功");
        commonResp.setDataList(inventoryInfos);
        String json = gson.toJson(commonResp);
        System.out.println(json);
        //FileUtil.writeFile("InventoryByBarcode.json", json);
    }

    @Test
    void getInventoryBySearchKeyTl() throws IOException {
        CommonResp commonResp = new CommonResp();
        List<InventoryInfo> inventoryInfos = inventoryService.getInventoryBySearchKeyTl("关键字");
        commonResp.setRetCode("0");
        commonResp.setRetMsg("成功");
        commonResp.setDataList(inventoryInfos);
        String json = gson.toJson(commonResp);
        System.out.println(json);
        //FileUtil.writeFile("InventoryBySearchKeyTl.json", json);
    }

    @Test
    void getInventoryByWarehouseAndStoresIoc() throws IOException {
        CommonResp commonResp = new CommonResp();
        List<InventoryInfo> inventoryInfos = inventoryService.getInventoryByWarehouseAndStoresIoc("warehouse1", "1号仓库3定位");
        commonResp.setRetCode("0");
        commonResp.setRetMsg("成功");
        commonResp.setDataList(inventoryInfos);
        String json = gson.toJson(commonResp);
        System.out.println(json);
        //FileUtil.writeFile("InventoryByWarehouseAndStoresIoc.json", json);
    }

}
