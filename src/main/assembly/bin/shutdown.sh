#!/bin/sh

BIN_DIR=$(cd `dirname $0`; pwd)
cd $BIN_DIR; cd ..
BASE_DIR=`pwd`

PID=`ps -ef | grep $BASE_DIR | grep -v grep | awk '{print $2}'`

if [ -n "$PID" ]; then
  kill -9 $PID
else
  echo "The application didn't startup!"
fi