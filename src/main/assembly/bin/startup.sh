#!/bin/sh

BIN_DIR=$(cd `dirname $0`; pwd)
cd $BIN_DIR; cd ..
BASE_DIR=`pwd`

LIB_DIR=$BASE_DIR/lib
CONF_DIR=$BASE_DIR/config
LOGS_DIR=$BASE_DIR/logs
APP_NAME=tms-bootstrap

# create logs directory if not exists
if [ ! -d "$LOGS_DIR" ]; then
  mkdir -p $LOGS_DIR
fi

LIB_JARS=`ls $LIB_DIR| grep .jar| awk '{print "'$LIB_DIR'/"$0}' | tr "\n" ":"`
JAVA_OPTS=" -server -Xms512m -Xmx2g -XX:+UseG1GC -XX:MaxMetaspaceSize=128m -XX:+HeapDumpOnOutOfMemoryError -XX:MaxGCPauseMillis=200 -Dfile.encoding=UTF-8"
let type=prod
if [ ! -n "$1" ]; then
    type=prod
else
    type=$1
fi
JAVA_OPTS=" $JAVA_OPTS -Dspring.profiles.active=$type  -Dlog.base.path=$LOGS_DIR "
nohup java $JAVA_OPTS -classpath $CONF_DIR:$LIB_JARS com.eaero.TmsGMockApplication > $LOGS_DIR/$APP_NAME.out 2>&1 &