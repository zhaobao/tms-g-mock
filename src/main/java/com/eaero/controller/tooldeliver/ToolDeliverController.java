package com.eaero.controller.tooldeliver;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.eaero.CommonResp;
import com.eaero.controller.tooldeliver.pojo.TraceToolDeliverDetailInfoVo;
import com.eaero.entity.TraceToolDeliver;
import com.eaero.service.ITraceToolDeliverDetailService;
import com.eaero.service.ITraceToolDeliverService;
import com.eaero.service.ITraceToolDeliverSignService;
import com.eaero.vo.ToolDeliverDetail;
import com.eaero.vo.ToolDeliverDetailRequest;
import com.eaero.vo.ToolDeliverRequest;
import com.eaero.vo.ToolDeliverSign;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zzj
 * @date 2023/3/14 15:36
 * @description
 */
@CrossOrigin
@RequestMapping
@Slf4j
@RestController
public class ToolDeliverController {

    private final Gson gson = new Gson();

    @Autowired
    private ITraceToolDeliverService traceToolDeliverService;

    @Autowired
    private ITraceToolDeliverDetailService traceToolDeliverDetailService;

    @Autowired
    private ITraceToolDeliverSignService traceToolDeliverSignService;

    /**
     * 46. 查询工具配送单据
     * @param request
     * @return
     */
    @GetMapping("/GetToolDeliver.do")
    public CommonResp getToolDeliver(ToolDeliverRequest request) {
        CommonResp resp = CommonResp.buildFailMsg("没有该配送单");
        try {
            List<TraceToolDeliver> traceToolDelivers = traceToolDeliverService.getToolDeliver(request);
            if (CollUtil.isNotEmpty(traceToolDelivers)) {
                resp = CommonResp.buildSucsDataListMsg(traceToolDelivers, "数据获取成功！");
            }
        } catch (Exception e) {
            log.error("ToolDeliverController getToolDeliver error: ", e);
        }
        return resp;
    }

    /**
     * 47. 查询工具配送明细
     * @param request
     * @return
     */
    @GetMapping("/GetToolDeliverdetail.do")
    public CommonResp getToolDeliverDetail(ToolDeliverDetailRequest request) {
        CommonResp resp = CommonResp.buildFailMsg("没有该配送单明细");
        try {
            List<ToolDeliverDetail> traceToolDelivers = traceToolDeliverDetailService.getToolDeliverDetail(request);
            if (CollUtil.isNotEmpty(traceToolDelivers)) {
                resp = CommonResp.buildSucsDataListMsg(traceToolDelivers, "数据获取成功！");
            }
        } catch (Exception e) {
            log.error("ToolDeliverController getToolDeliverDetail error: ", e);
        }
        return resp;
    }

    /**
     * 48. 查询配送流水（不需要，废弃）
     * @param tddId
     * @return
     */
    @GetMapping("/GetToolDeliverSign.do")
    public CommonResp getToolDeliverSign(@RequestParam String tddId) {
        CommonResp resp = CommonResp.buildFailMsg("没有该配送单明细");
        try {
            ToolDeliverSign toolDeliverSign = traceToolDeliverSignService.getDeliverSign(tddId);
            if (ObjectUtil.isNotEmpty(toolDeliverSign)) {
                return CommonResp.buildSucsDetailMsg(toolDeliverSign, "数据获取成功！");
            }
        } catch (Exception e) {
            log.error("ToolDeliverController getToolDeliverSign error: ", e);
        }
        return resp;
    }

}
