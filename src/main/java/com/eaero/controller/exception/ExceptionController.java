package com.eaero.controller.exception;

import cn.hutool.core.collection.CollUtil;
import com.eaero.CommonResp;
import com.eaero.service.ITraceToolService;
import com.eaero.vo.DiscardRequest;
import com.eaero.vo.DiscardVo;
import com.eaero.vo.ExceptionFeelBackRequest;
import com.google.common.collect.Lists;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author luofeng
 * @create 2023/3/14 15:02
 */
@CrossOrigin
@RestController
@RequestMapping
public class ExceptionController {

    private final Logger log = LoggerFactory.getLogger(ExceptionController.class);

    @Autowired
    private ITraceToolService toolService;

    @PostMapping("/ExceptionFeelBack.do")
    public CommonResp exceptionFeelBack(@RequestBody ExceptionFeelBackRequest request) {
        CommonResp resp = CommonResp.buildFailMsg("故障反馈失败");
        try {
            resp = toolService.exceptionFeelBack(request);
        } catch (Exception e) {
            log.error("exceptionFeelBack error: ", e);
        }
        return resp;
    }

    @PostMapping("/DoTlDiscard.do")
    public CommonResp doTlDiscard(@RequestBody DiscardRequest request) {
        CommonResp resp = CommonResp.buildFailMsg("提交工具报废信息失败");
        try {
            List<DiscardVo> discardList = request.getDiscardList();
            List<String> trno = Lists.newArrayList();
            for (DiscardVo discardVo : discardList) {
                if (CollUtil.contains(trno, discardVo.getTrno())) {
                    return CommonResp.buildFailMsg("清单中件号刻号重复！");
                }
                trno.add(discardVo.getTrno());
            }
            toolService.doTlDiscard(trno);
            return CommonResp.buildSucsDataListMsg(null, "提交工具报废信息成功!");
        } catch (Exception e) {
            log.error("doTlDiscard error: ", e);
        }
        return resp;
    }
}
