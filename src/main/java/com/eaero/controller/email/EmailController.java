package com.eaero.controller.email;

import com.eaero.CommonResp;
import com.eaero.service.email.EmailService;
import com.eaero.vo.SendEmailRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author zzj
 * @date 2023/3/20 9:34
 * @description
 */
@CrossOrigin
@RestController
@RequestMapping
public class EmailController {

    private final Logger log = LoggerFactory.getLogger(EmailController.class);

    @Autowired
    private EmailService emailService;

    /**
     * 18. 发送邮件
     * @param request
     * @return
     */
    @PostMapping("/sendEmail.do")
    public CommonResp sendEmail(@RequestBody SendEmailRequest request) {
        CommonResp resp = CommonResp.buildFailMsg("邮件发送失败");
        try {
            resp = emailService.sendEmail(request);
        } catch (Exception e) {
            log.error("sendEmail error: ", e);
        }
        return resp;
    }

}
