package com.eaero.controller.tool;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.io.resource.ResourceUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.eaero.CommonResp;
import com.eaero.controller.tool.pojo.IssueRequest;
import com.eaero.controller.tool.pojo.ReturnRequest;
import com.eaero.controller.tool.pojo.ToolDetail;
import com.eaero.entity.TraceJobCard;
import com.eaero.entity.TraceTool;
import com.eaero.service.ITraceToolService;
import com.eaero.vo.BindRFID;
import com.eaero.vo.GetImpageByPartnoVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.hc.core5.http.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;


/**
 * @author zb
 * @date 2023/03/14 014 10:21
 */
@CrossOrigin
@RequestMapping
@Slf4j
@RestController
public class ToolController {
    @Autowired
    private ITraceToolService traceToolService;
    /**
     * 查询工具件号信息
     1.partno(String) 件号 支持模糊查询, trno(String) 刻号；
     2.partdescr(String) 件号描述 支持模糊查询；
     3.barcode(String) 条形码；
     4.rfidId(String);
     5.searchKeyTl(String)查询关键字 支持模糊查询 ;
     6.warehouse(String)仓库；
     *1 至 6 点至少保留一组作为查询条件的必填项
     * @return 数据获取成功！
     */
    @GetMapping("/GetToolInfo.do")
    public CommonResp getToolInfo(
            @RequestParam(required = false) String rfidId,
            @RequestParam(required = false) String partno,
            @RequestParam(required = false) String trno,
            @RequestParam(required = false) String partdescr,
            @RequestParam(required = false) String barcode,
            @RequestParam(required = false) String searchKeyTl,
            @RequestParam(required = false) String warehouse
    ) {
        if(StrUtil.isEmpty(rfidId)
                && StrUtil.isEmpty(partno)  && StrUtil.isEmpty(trno)
                && StrUtil.isEmpty(partdescr) && StrUtil.isEmpty(barcode)
                && StrUtil.isEmpty(searchKeyTl) && StrUtil.isEmpty(warehouse)
        ){
            return CommonResp.buildFailMsg("查询条件不能为空！");
        }
        List<TraceTool> toolList = traceToolService.getToolInfo(rfidId,partno,trno,partdescr,barcode,searchKeyTl,warehouse);
        if(CollectionUtil.isEmpty(toolList)){
            return CommonResp.buildFailMsg("没有该工具件号的信息！！");
        }
        return CommonResp.buildSucsDataListMsg(toolList,"数据获取成功！");
    }



    /**
     * 获取库存信息
     * 1. rid(String) 工具顺序号
     * 2. rfidId(String) RFID 号
     * 3. partno(String) 件号 支持模糊查询, trno(String) 刻号（支持带.的多参数左右模糊查询）；
     * 4. partdescr(String) 件号描述 支持模糊查询,
     * 5. barcode(String) 条形码
     * 6. searchKeyTl(String)查询关键字 支持模糊查询 ;
     * 7. warehouse(String)仓库；storesloc(String)定位;
     *  1 至7 点至少保留一组作为查询条件的必填项
     * @return
     */
    @GetMapping("/GetInventoryInfo.do")
    public CommonResp getInventoryInfo(
            @RequestParam(required = false) String rid,
            @RequestParam(required = false) String rfidId,
            @RequestParam(required = false) String partno,
            @RequestParam(required = false) String trno,
            @RequestParam(required = false) String partdescr,
            @RequestParam(required = false) String barcode,
            @RequestParam(required = false) String searchKeyTl,
            @RequestParam(required = false) String warehouse,
            @RequestParam(required = false) String storesloc
    ) {
        if(StrUtil.isEmpty(rfidId)
                && StrUtil.isEmpty(partno)  && StrUtil.isEmpty(trno)
                && StrUtil.isEmpty(partdescr) && StrUtil.isEmpty(barcode)
                && StrUtil.isEmpty(searchKeyTl) && StrUtil.isEmpty(warehouse)
        ){
            return CommonResp.buildFailMsg("查询条件不能为空！");
        }
        List<TraceTool> toolList = traceToolService.getInventoryInfo(rid,rfidId,partno,trno,partdescr,barcode,searchKeyTl,warehouse,storesloc);
        if(CollectionUtil.isEmpty(toolList)){
            return CommonResp.buildFailMsg("没有该工具件号的信息！！");
        }
        return CommonResp.buildSucsDataListMsg(toolList,"数据获取成功！");
    }
    /**
     * 查询借出工具信息
     * 1. rid(String) 工具顺序号
     * 2. rfidId(String) RFID 号
     * 3. partno(String) 件号 支持模糊查询, trno(String) 刻号（支持带.的多参数左右模糊查询）；
     * 4. partdescr(String) 件号描述 支持模糊查询,
     * 5. barcode(String) 条形码
     * 6. searchKeyTl(String)查询关键字 支持模糊查询 ;
     * 7. warehouse(String)仓库；storesloc(String)定位;
     *  1 至7 点至少保留一组作为查询条件的必填项
     * @return
     */
    @GetMapping("/GetLoanToolsAll.do")
    public CommonResp getLoanToolsAll(
            @RequestParam(required = false) String issueTo,
            @RequestParam(required = false) String rfidId,
            @RequestParam(required = false) String partno,
            @RequestParam(required = false) String trno,
            @RequestParam(required = false) String partdescr,
            @RequestParam(required = false) String barcode,
            @RequestParam(required = false) String issueDate,
            @RequestParam(required = false) String issueRid,
            @RequestParam(required = false) String jcWo,
            @RequestParam(required = false) String Acno,
            @RequestParam(required = false) String tpNo,
            @RequestParam(required = false) String warehouse,
            @RequestParam(required = false) String storesloc,
            @RequestParam(required = false) String searchKeyTl
    ) {
//        if(StrUtil.isEmpty(rfidId)
//                && StrUtil.isEmpty(partno)  && StrUtil.isEmpty(trno)
//                && StrUtil.isEmpty(partdescr) && StrUtil.isEmpty(barcode)
//                && StrUtil.isEmpty(issueTo) && StrUtil.isEmpty(warehouse)
//                && StrUtil.isEmpty(issueRid) && ObjectUtil.isEmpty(issueDate)
//                && StrUtil.isEmpty(jcWo) && StrUtil.isEmpty(Acno)
//                && StrUtil.isEmpty(tpNo)&& StrUtil.isEmpty(storesloc)
//        ){
//            return CommonResp.buildFailMsg("查询条件不能为空！");
//        }
        List<TraceTool> toolList = traceToolService.GetLoanToolsAll(issueTo,rfidId,partno,trno,partdescr,barcode,issueDate,warehouse,storesloc,issueRid,jcWo,Acno,tpNo,searchKeyTl);
        if(CollectionUtil.isEmpty(toolList)){
            return CommonResp.buildFailMsg("没有该工具信息！！");
        }
        return CommonResp.buildSucsDataListMsg(toolList,"数据获取成功！");
    }


    /**
     * 查询套装工具明细
     *1.rid(String) 工具顺序号 ，
     * 2.partno(String) 件号 ,trno(String) 刻号;
     *  1 至7 点至少保留一组作为查询条件的必填项
     * @return
     */
    @GetMapping("/GetTooldetail.do")
    public CommonResp GetTooldetail(
            @RequestParam(required = false) String rid,
            @RequestParam(required = false) String partno,
            @RequestParam(required = false) String trno
    ) {
        List<ToolDetail> toolList = traceToolService.getTooldetail(rid,trno,partno);
        if(CollectionUtil.isEmpty(toolList)){
            return CommonResp.buildFailMsg("①套装工具明细信息获取失败！！");
        }
        return CommonResp.buildSucsDataListMsg(toolList,"数据获取成功！");
    }

    @PostMapping("/ISSUE.do")
    public CommonResp ISSUE(@RequestBody @Validated IssueRequest request) {
        CommonResp resp = CommonResp.buildFailMsg("发料失败");
        try {
            resp = traceToolService.issueTool(request);
        } catch (Exception e) {
            log.error("ISSUE error: ", e);
        }
        return resp;
    }
    @PostMapping("/RETURN.do")
    public CommonResp RETURN(@RequestBody ReturnRequest request) {
        CommonResp resp = CommonResp.buildFailMsg("退料失败");
        try {
            resp = traceToolService.returnTool(request);
        } catch (Exception e) {
            log.error("RETURN error: ", e);
        }
        return resp;
    }

    /**
     * 获取工卡指令信息
     * @param workorderno
     * @return
     */
    @GetMapping("/GetWorkorderno.do")
    public CommonResp GetWorkorderno(
            @RequestParam String workorderno
    ) {
        if(StrUtil.isEmpty(workorderno)){
            return CommonResp.buildFailMsg("查询条件不能为空！");
        }
        List<TraceJobCard> workOrderInfoList = traceToolService.GetWorkorderno(workorderno);
        if (CollectionUtil.isNotEmpty(workOrderInfoList)) {
            return CommonResp.buildSucsDataListMsg(workOrderInfoList,"数据获取成功！");
        }else {
            return CommonResp.buildFailMsg("没有该指令的工卡信息！");
        }
    }
    /**
     * 获取工具图片
     * 根据件号的信息，返回图片流，如无图片名称则默认返回第一张图片
     */
    @PostMapping("/GetImpageByPartno.do")
    public void getImpageByPartno(GetImpageByPartnoVo impageByPartnoVo, HttpServletResponse response) throws Exception{
        byte[] bytes = ResourceUtil.readBytes("img/test.jpg");

        response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("test.jpg", "UTF-8"));
        // 告知浏览器文件的大小
        response.addHeader("Content-Length", "" + bytes.length);
        response.setHeader("Access-Control-Expose-Headers", "ticket, Content-Disposition");
        response.setContentType(ContentType.IMAGE_JPEG.toString());
        // 返回数据到输出流对象中
        IoUtil.write(response.getOutputStream(), true, bytes);
    }

    /**
     * 6. 标签绑定
     */

    @PostMapping("/BindRFID.do")
    public CommonResp bindRFID(BindRFID request, @RequestParam("codeAndNmae") String codeAndNmae) {
        CommonResp resp = CommonResp.buildFailMsg("标签绑定失败！");
        try {
            if (StrUtil.isEmpty(request.getRid()) || StrUtil.isEmpty(request.getRfidId()) || StrUtil.isEmpty(codeAndNmae)) {
                return CommonResp.buildFailMsg("rid或rfidId或codeAndNmae参数不能为空");
            }
            resp = traceToolService.bindRFID(request);
        } catch (Exception e) {
            log.error("bindRFID error: ", e);
        }
        return resp;
    }

    /**
     * 7. 标签绑定
     */

    @PostMapping("/UnBindRFID.do")
    public CommonResp unBindRFID(BindRFID request) {
        CommonResp resp = CommonResp.buildFailMsg("标签绑定失败！");
        try {
            if (StrUtil.isEmpty(request.getRid()) || StrUtil.isEmpty(request.getRfidId()) || StrUtil.isEmpty(request.getCodeAndNmae())) {
                return CommonResp.buildFailMsg("rid或rfidId或codeAndNmae参数不能为空");
            }
            resp = traceToolService.unBindRFID(request);
        } catch (Exception e) {
            log.error("unBindRFID error: ", e);
        }
        return resp;
    }
}
