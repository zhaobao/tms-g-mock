package com.eaero.controller.tool.pojo;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author zb
 * @date 2023/03/15 015 10:18
 */
@Data
public class IssueRequest {
    @NotBlank(message = "来源，必填")
    private String origin;
    @NotBlank(message = "是否配送，必填（Y:配送，N:不配送）")
    private String deliver;
    @NotBlank(message = "工作者员工号，必填")
    private String userCode;
    @NotBlank(message = "操作人员工号，必填")
    private String empCode;
    private String jcWo;
    private String reg;
    @NotBlank(message = " 是否在线，必填，必填")
    private String onlineFlag;
    @Valid
    private List<ToolIssue> toolIssue;
}
