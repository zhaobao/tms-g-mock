package com.eaero.controller.tool.pojo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author zb
 * @date 2023/03/15 015 10:24
 */
@Data
public class ToolReturn {

    @NotBlank(message = "顺序号，必填")
    private String rid;
    @NotBlank(message = "件号，必填")
    private String partno;
    @NotBlank(message = "刻号，必填")
    private String trno;
    @NotBlank(message = "数量，必填")
    private String qty;
    private String opRemark;
}
