/**
 * Copyright 2023 bejson.com
 */
package com.eaero.controller.tool.pojo;

import lombok.Data;


@Data
public class ToolDetail {

    private String station;
    private String issueRemind;
    private String partno;
    private String parenetrid;
    private String partentPartno;
    private String alternatePartno;
    private String trno;
    private String warehouse;
    private String issueRid;
    private String parentTrno;
    private String commentText;
    private String measuringNo;
    private String rid;
    private String partdescr;
    private String standByPn;
    private String flagIssue;
    private String storesloc;
    private String onhand;
    private String tlFlag;
    private String tlStatus;
    private String imgFlag;


}