package com.eaero.controller.tool.pojo;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author zb
 * @date 2023/03/15 015 10:23
 */
@Data
public class ReturnRequest {
    @NotBlank(message = "来源，必填")
    private String origin;
    @NotBlank(message = "工作者员工号，必填")
    private String userCode;
    @NotBlank(message = "操作人员工号，必填")
    private String empCode;
    @Valid
    private List<ToolReturn> toolReturn;
}
