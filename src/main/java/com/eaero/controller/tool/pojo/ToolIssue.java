package com.eaero.controller.tool.pojo;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

/**
 * @author zb
 * @date 2023/03/15 015 10:20
 */
@Data
public class ToolIssue {
    @NotBlank(message = "顺序号，必填")
    private String rid;
    @NotBlank(message = "件号，必填")
    private String partno;
    @NotBlank(message = "刻号，必填")
    private String trno;
    @NotBlank(message = "数量，必填")
    @Max(value = 1,message = "件数量必须>0")
    private String qty;
    private String opRemark;
    private String planReturn;
}
