package com.eaero.controller.tool.pojo;

import lombok.Data;

/**
 * @author zb
 * @date 2023/03/15 015 17:19
 */
@Data
public class WorkOrderInfo {
    private String workorderno;
    private String reg;
    private String jobcard;
}
