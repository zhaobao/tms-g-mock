package com.eaero.controller.tool.pojo;

/**
 * @author zb
 * @date 2023/03/14 014 10:34
 */
public class TraceToolRequest {
    private String rid;
    private String rfidId;
    private String partno;
    private String trno;
    private String partdescr;
    private String barcode;
    private String searchKeyTl;
    private String warehouse;
    private String storesloc;

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getRfidId() {
        return rfidId;
    }

    public void setRfidId(String rfidId) {
        this.rfidId = rfidId;
    }

    public String getPartno() {
        return partno;
    }

    public void setPartno(String partno) {
        this.partno = partno;
    }

    public String getTrno() {
        return trno;
    }

    public void setTrno(String trno) {
        this.trno = trno;
    }

    public String getPartdescr() {
        return partdescr;
    }

    public void setPartdescr(String partdescr) {
        this.partdescr = partdescr;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getSearchKeyTl() {
        return searchKeyTl;
    }

    public void setSearchKeyTl(String searchKeyTl) {
        this.searchKeyTl = searchKeyTl;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public String getStoresloc() {
        return storesloc;
    }

    public void setStoresloc(String storesloc) {
        this.storesloc = storesloc;
    }
}
