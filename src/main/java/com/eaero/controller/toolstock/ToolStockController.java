package com.eaero.controller.toolstock;

import com.eaero.CommonResp;
import com.eaero.service.ITraceToolStockService;
import com.eaero.vo.CancelTlStockRequest;
import com.eaero.vo.DoTlStockRequest;
import com.eaero.vo.GoTlStockRequest;
import com.eaero.vo.ToolStockRequest;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Description:
 *
 * @author Ye Zhihong
 * @date 2023/3/17 16:56
 */
@CrossOrigin
@RequestMapping
@RestController
public class ToolStockController {
    private final Logger log = LoggerFactory.getLogger(ToolStockController.class);

    @Autowired
    private ITraceToolStockService toolStockService;

    /**
     * 55. 查询工具盘点单
     *
     * @param request
     * @return
     */
    @GetMapping("/GetTlStock.do")
    public CommonResp getToolStock(ToolStockRequest request) {
        CommonResp resp = new CommonResp();
        try {
            Gson gson = new Gson();
            String decode = URLDecoder.decode(gson.toJson(request), "utf-8");
            ToolStockRequest toolStockRequest = gson.fromJson(decode, ToolStockRequest.class);
            resp = toolStockService.getToolStock(toolStockRequest);
        } catch (Exception e) {
            log.error("getToolStock error: ", e);
        }
        return resp;
    }

    /**
     * 56. 查询工具盘点明细
     *
     * @param request
     * @return
     */
    @GetMapping("/GetTlStockDetail.do")
    public CommonResp getToolStockDetail(ToolStockRequest request) {
        CommonResp resp = new CommonResp();
        try {
            resp = toolStockService.getToolStockDetail(request);
        } catch (Exception e) {
            log.error("getToolStockDetail error: ", e);
        }
        return resp;
    }

    /**
     * 56. 创建工具盘点单
     *
     * @param request
     * @return
     */
    @PostMapping("/goTlStock.do")
    public CommonResp goTlStock(@RequestBody @Validated GoTlStockRequest request) {
        CommonResp resp = new CommonResp();
        try {
            resp = toolStockService.goTlStock(request);
        } catch (Exception e) {
            log.error("goTlStock error: ", e);
        }
        return resp;
    }

    /**
     * 58. 作废工具盘点单
     *
     * @param request
     * @return
     */
    @PostMapping("/CancelTlStock.do")
    public CommonResp CancelTlStock(@RequestBody @Validated CancelTlStockRequest request) {
        CommonResp resp = new CommonResp();
        try {
            resp = toolStockService.CancelTlStock(request);
        } catch (Exception e) {
            log.error("CancelTlStock error: ", e);
        }
        return resp;
    }

    /**
     * 59. 提交工具盘点信息
     *
     * @param request
     * @return
     */
    @PostMapping("/DoTlStock.do")
    public CommonResp DoTlStock(@RequestBody @Validated DoTlStockRequest request) {
        CommonResp resp = new CommonResp();
        try {
            resp = toolStockService.DoTlStockRequest(request);
        } catch (Exception e) {
            log.error("DoTlStockRequest error: ", e);
        }
        return resp;
    }
}
