package com.eaero.controller.pick;

import cn.hutool.core.util.StrUtil;
import com.eaero.CommonResp;
import com.eaero.service.ITraceToolPickService;
import com.eaero.vo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author luofeng
 * @create 2023/3/14 16:20
 */
@CrossOrigin
@RestController
@RequestMapping
public class PickController {


    private final Logger log = LoggerFactory.getLogger(PickController.class);

    @Autowired
    private ITraceToolPickService pickService;

    @GetMapping("/GetToolPickHD.do")
    public CommonResp getToolPickHD(@RequestParam(required = false) String tpNo,
                                    @RequestParam(required = false) String createdUser,
                                    @RequestParam(required = false) String createdDate,
                                    @RequestParam(required = false) String status,
                                    @RequestParam(required = false) String updatedUsers
    ) {
        CommonResp resp = CommonResp.buildFailMsg("获取数据失败");
        try {
            if (StrUtil.isEmpty(tpNo) && StrUtil.isEmpty(createdUser) && StrUtil.isEmpty(createdDate) && StrUtil.isEmpty(status) && StrUtil.isEmpty(updatedUsers)) {
                return resp;
            }
            resp = pickService.getToolPickHD(tpNo, createdUser, createdDate, status, updatedUsers);
        } catch (Exception e) {
            log.error("getToolPickHD error: ", e);
        }
        return resp;
    }

    @PostMapping("/CancelToolPickHD.do")
    public CommonResp cancelToolPickHD(@Validated @RequestBody CancelToolPickRequest request) {
        CommonResp resp = CommonResp.buildFailMsg("作废拣料单失败！");
        try {
            resp = pickService.cancelToolPickHD(request);
        } catch (Exception e) {
            log.error("cancelToolPickHD error: ", e);
        }
        return resp;
    }

    @GetMapping("/GetToolPickdetail.do")
    public CommonResp getToolPickdetail(@RequestParam(required = false) String tpId,
                                    @RequestParam(required = false) String tpNo
    ) {
        CommonResp resp = CommonResp.buildFailMsg("数据获取失败");
        try {
            if (StrUtil.isEmpty(tpId) && StrUtil.isEmpty(tpNo)) {
                return resp;
            }
            resp = pickService.getToolPickdetail(tpId, tpNo);
        } catch (Exception e) {
            log.error("getToolPickdetail error: ", e);
        }
        return resp;
    }

    @PostMapping("/DeleteToolPickDetail")
    public CommonResp deleteToolPickDetail(@Validated @RequestBody DeleteToolPickDetailRequest request) {
        CommonResp resp = CommonResp.buildFailMsg("删除拣料明细失败！");
        try {
            resp = pickService.deleteToolPickDetail(request);
        } catch (Exception e) {
            log.error("deleteToolPickDetail error: ", e);
        }
        return resp;
    }

    @PostMapping("/UpdateToolPickdetailPn.do")
    public CommonResp updateToolPickdetailPn(@Validated @RequestBody UpdateToolPickdetailPnRequest request) {
        CommonResp resp = CommonResp.buildFailMsg("更新件号失败！");
        try {
            resp = pickService.updateToolPickdetailPn(request);
        } catch (Exception e) {
            log.error("updateToolPickdetailPn error: ", e);
        }
        return resp;
    }

    @PostMapping("/UpdateToolPickdetailList.do")
    public CommonResp updateToolPickdetailList(@Validated @RequestBody UpdateToolPickdetailListRequest request) {
        CommonResp resp = CommonResp.buildFailMsg("指定建议批号失败！");
        try {
            resp = pickService.updateToolPickdetailList(request);
        } catch (Exception e) {
            log.error("updateToolPickdetailList error: ", e);
        }
        return resp;
    }

    @PostMapping("/PickIssue.do")
    public CommonResp pickIssue(@Validated @RequestBody PickIssueRequest request) {
        CommonResp resp = CommonResp.buildFailMsg("发料失败！");
        try {
            resp = CommonResp.buildSucsDataListMsg(null, "发料成功！[68477]");
        } catch (Exception e) {
            log.error("pickIssue error: ", e);
        }
        return resp;
    }

    /**
     * 31. 生成拣料单
     * @param request
     * @return
     */
    @PostMapping("/GoPickTools.do")
    public CommonResp goPickTools(@RequestBody @Validated PickToolsRequest request) {
        CommonResp resp = CommonResp.buildFailMsg("生成拣料单失败！");
        try {
            resp = pickService.goPickTools(request);
        } catch (Exception e) {
            log.error("goPickTools error: ", e);
        }
        return resp;
    }

    /**
     * 42. 删除拣料单明细的建议批号
     * @param request
     * @return
     */
    @PostMapping("/DeleteToolPickDetailList.do")
    public CommonResp deleteToolPickDetailList(@RequestBody @Validated DeleteToolPickDetailListRequest request) {
        CommonResp resp = CommonResp.buildFailMsg("删除建议批号失败！");
        try {
            resp = pickService.deleteToolPickDetailList(request);
        } catch (Exception e) {
            log.error("deleteToolPickDetailList error: ", e);
        }
        return resp;
    }

}
