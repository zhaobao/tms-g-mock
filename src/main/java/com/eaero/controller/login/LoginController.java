package com.eaero.controller.login;

import cn.hutool.core.util.StrUtil;
import com.eaero.CommonResp;
import com.eaero.service.ITraceUserService;
import com.eaero.vo.Login;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author luofeng
 * @create 2023/3/14 14:03
 */
@CrossOrigin
@RestController
@RequestMapping
public class LoginController {

    private final Logger log = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private ITraceUserService userService;

    @PostMapping("/login.do")
    public CommonResp login(Login login) {
        CommonResp resp = CommonResp.buildFailMsg("用户账号验证失败！");
        try {
            if (StrUtil.isEmpty(login.getUserCode()) || StrUtil.isEmpty(login.getUserPassword())) {
                return resp;
            }
            resp = userService.login(login);
        } catch (Exception e) {
            log.error("login error: ", e);
        }
        return resp;
    }

    @GetMapping("/loginByCard.do")
    public CommonResp loginByCard(@RequestParam String cardId) {
        CommonResp resp = CommonResp.buildFailMsg("该员工卡无效！");
        try {
            resp = userService.loginByCard(cardId);
        } catch (Exception e) {
            log.error("loginByCard error: ", e);
        }
        return resp;
    }
}
