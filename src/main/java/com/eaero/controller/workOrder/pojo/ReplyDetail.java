package com.eaero.controller.workOrder.pojo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author zb
 * @date 2023/03/15 015 15:58
 */
@Data
public class ReplyDetail {
    @NotBlank(message = " 申请单明细 id，必填")
    private String rid;
}
