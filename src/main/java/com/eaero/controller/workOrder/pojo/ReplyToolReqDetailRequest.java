package com.eaero.controller.workOrder.pojo;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author zb
 * @date 2023/03/15 015 15:58
 */
@Data
public class ReplyToolReqDetailRequest {
    @NotBlank(message = " 员工号，必填")
    private String userCode;
    @NotBlank(message = " 处理回复内容 id，必填")
    private String msg;
    @Valid
    private List<String> replyDetail;
}
