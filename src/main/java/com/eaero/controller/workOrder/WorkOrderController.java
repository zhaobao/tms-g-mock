package com.eaero.controller.workOrder;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.eaero.CommonResp;
import com.eaero.controller.tool.pojo.ReturnRequest;
import com.eaero.controller.workOrder.pojo.ReplyToolReqDetailRequest;
import com.eaero.service.ITraceWorkOrderDetailService;
import com.eaero.service.ITraceWorkOrderService;
import com.eaero.service.workorder.pojo.CancelWorkOrderRequest;
import com.eaero.service.workorder.pojo.WorkOrderDetailRequset;
import com.eaero.service.workorder.pojo.WorkOrderRequest;
import com.eaero.vo.GetToolReqdetailSumRequest;
import com.eaero.vo.UpdateToolPickdetailListRequest;
import com.eaero.vo.UpdateToolReqdetailPnRequest;
import com.eaero.vo.UpdateWhsTypeRequest;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * @description:
 * @author: WBL
 * @date: 2023-03-14
 * @time: 10:34
 */
@RestController
@RequestMapping
public class WorkOrderController {
    private final Logger log = LoggerFactory.getLogger(WorkOrderController.class);
    private Gson gson = new Gson();
    @Autowired
    private ITraceWorkOrderService traceWorkOrderService;
    @Autowired
    private ITraceWorkOrderDetailService traceWorkOrderDetailService;

    @GetMapping("/GetToolReqHD.do")
    public CommonResp getWorkOrder(
            @RequestParam(required = false) String reqNo,
            @RequestParam(required = false) String reqBy,
            @RequestParam(required = false) String crtDate,
            @RequestParam(required = false) String reqSts,
            @RequestParam(required = false) String sndBy,
            @RequestParam(required = false) String sndDate,
            @RequestParam(required = false) String origin
    ) {
        CommonResp resp = CommonResp.buildFailMsg("查询工具申请单失败");
        try {
            HashMap<String, String> request = Maps.newHashMap();
            if (StrUtil.isNotBlank(reqNo)) {
                request.put("reqNo", reqNo);
            }
            if (StrUtil.isNotBlank(reqBy)) {
                request.put("reqBy", reqBy);
            }
            if (StrUtil.isNotBlank(crtDate)) {
                request.put("crtDate", crtDate);
            }
            if (StrUtil.isNotBlank(reqSts)) {
                request.put("reqSts", reqSts);
            }
            if (StrUtil.isNotBlank(sndBy)) {
                request.put("sndBy", sndBy);
            }
            if (StrUtil.isNotBlank(sndDate)) {
                request.put("sndDate", sndDate);
            }
            if (StrUtil.isNotBlank(origin)) {
                request.put("origin", origin);
            }
            WorkOrderRequest workOrderRequest = null;
            if (ObjectUtil.isNotEmpty(request)) {
                String reqJson = gson.toJson(request);
                workOrderRequest = gson.fromJson(reqJson, WorkOrderRequest.class);
            }
            resp = traceWorkOrderService.getWorkOrder(workOrderRequest);
        } catch (Exception e) {
            log.error("getWorkOrder error: ", e);
        }
        return resp;
    }


    @PostMapping("/CancelToolReqHD.do")
    public CommonResp cancellationWorkOrder(@RequestBody CancelWorkOrderRequest request) {
        CommonResp resp = CommonResp.buildFailMsg("作废申请单失败");
        try {
            resp = traceWorkOrderService.cancellationWorkOrder(request);
        } catch (Exception e) {
            log.error("cancellationWorkOrder error: ", e);
        }
        return resp;
    }

    /**
     * 查询申请单明细
     * @param reqNo
     * @param tailNo
     * @param jcWorkorder
     * @param visitDesc
     * @param dtlSts
     * @param pn
     * @param reqDate
     * @param reqrcvByNo
     * @param whsType
     * @param msg
     * @param sndDate
     * @return
     */
    @GetMapping("/GetToolReqdetail.do")
    public CommonResp getWorkOrderDetail(@RequestParam(required = false) String reqNo,
                                         @RequestParam(required = false) String tailNo,
                                         @RequestParam(required = false) String jcWorkorder,
                                         @RequestParam(required = false) String visitDesc,
                                         @RequestParam(required = false) String dtlSts,
                                         @RequestParam(required = false) String pn,
                                         @RequestParam(required = false) String reqDate,
                                         @RequestParam(required = false) String reqrcvByNo,
                                         @RequestParam(required = false) String whsType,
                                         @RequestParam(required = false) String msg,
                                         @RequestParam(required = false) String sndDate
    ) {
        CommonResp resp = CommonResp.buildFailMsg("查询申请单明细失败");
        try {
            HashMap<String, String> request = Maps.newHashMap();
            if (StrUtil.isNotBlank(reqNo)) {
                request.put("reqNo", reqNo);
            }
            if (StrUtil.isNotBlank(tailNo)) {
                request.put("tailNo", tailNo);
            }
            if (StrUtil.isNotBlank(jcWorkorder)) {
                request.put("jcWorkorder", jcWorkorder);
            }
            if (StrUtil.isNotBlank(visitDesc)) {
                request.put("visitDesc", visitDesc);
            }
            if (StrUtil.isNotBlank(dtlSts)) {
                request.put("dtlSts", dtlSts);
            }
            if (StrUtil.isNotBlank(sndDate)) {
                request.put("sndDate", sndDate);
            }
            if (StrUtil.isNotBlank(pn)) {
                request.put("pn", pn);
            }
            if (StrUtil.isNotBlank(reqDate)) {
                request.put("reqDate", reqDate);
            }
            if (StrUtil.isNotBlank(reqrcvByNo)) {
                request.put("reqrcvByNo", reqrcvByNo);
            }
            if (StrUtil.isNotBlank(whsType)) {
                request.put("whsType", whsType);
            }
            if (StrUtil.isNotBlank(msg)) {
                request.put("msg", msg);
            }
            WorkOrderDetailRequset orderDetailRequset = null;
            if (ObjectUtil.isNotEmpty(request)) {
                String reqJson = gson.toJson(request);
                orderDetailRequset = gson.fromJson(reqJson, WorkOrderDetailRequset.class);
            }
            resp = traceWorkOrderDetailService.getWorkOrderDetail(orderDetailRequset);
        } catch (Exception e) {
            log.error("cancellationWorkOrder error: ", e);
        }
        return resp;
    }

    @PostMapping("/UpdateToolReqdetailPn.do")
    public CommonResp updateToolReqdetailPn(@Validated @RequestBody UpdateToolReqdetailPnRequest request) {
        CommonResp resp = CommonResp.buildFailMsg("更新件号和数量失败！");
        try {
            resp = traceWorkOrderDetailService.updateToolReqdetailPn(request);
        } catch (Exception e) {
            log.error("updateToolReqdetailPn error: ", e);
        }
        return resp;
    }

    @PostMapping("/UpdateWhsType.do")
    public CommonResp updateWhsType(@Validated @RequestBody UpdateWhsTypeRequest request) {
        CommonResp resp = CommonResp.buildFailMsg("指定拣料仓库失败！");
        try {
            resp = traceWorkOrderDetailService.updateWhsType(request);
        } catch (Exception e) {
            log.error("updateWhsType error: ", e);
        }
        return resp;
    }

    /**
     * 回复申请单明细
     */
    @PostMapping("/ReplyToolReqdetail.do")
    public CommonResp replyToolReqDetail(@RequestBody ReplyToolReqDetailRequest request) {
        CommonResp resp = CommonResp.buildFailMsg("退料失败");
        try {
            resp = traceWorkOrderDetailService.replyToolReqDetail(request);
        } catch (Exception e) {
            log.error("ReplyToolReqdetail error: ", e);
        }
        return resp;
    }

    /**
     * 查询明细汇总数据
     * 1.tailNo(String) 机尾号 支持模糊查询
     * 2.rcvBy(String) 工具收货人 支持模糊查询
     * 3.reqDate(String) 需求日期(yyyy-mm-dd)
     * 4.whsType(String) 拣料仓库
     * 必须至少传一个参数
     */
    @GetMapping("/GetToolReqdetailSum.do")
    public CommonResp getToolReqdetailSum(GetToolReqdetailSumRequest request) {
        CommonResp resp = CommonResp.buildFailMsg("没有数据");
        try {
            if (StrUtil.isEmpty(request.getTailNo()) && StrUtil.isEmpty(request.getRcvBy()) && StrUtil.isEmpty(request.getReqDate()) && StrUtil.isEmpty(request.getWhsType())) {
                return resp;
            }
            resp = traceWorkOrderDetailService.getToolReqdetailSum(request);
        } catch (Exception e) {
            log.error("getToolReqdetailSum error: ", e);
        }
        return resp;
    }
}
