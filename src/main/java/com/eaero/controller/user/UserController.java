package com.eaero.controller.user;

import com.eaero.CommonResp;
import com.eaero.service.ITraceUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author luofeng
 * @create 2023/3/14 13:19
 */
@CrossOrigin
@RestController
@RequestMapping
public class UserController {

    private final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private ITraceUserService userService;

    @GetMapping("/GetUserInfoAll.do")
    public CommonResp getUserInfoAll(@RequestParam(required = false) String userCode) {
        CommonResp resp = new CommonResp();
        try {
            resp = userService.getUserInfoAll(userCode);
        } catch (Exception e) {
            log.error("getUserInfoAll error: ", e);
        }
        return resp;
    }

    /**
     * 21. 获取工具房操作人信息
     * @return
     */
    @GetMapping("/getOperatorInfo.do")
    public CommonResp getOperatorInfo() {
        CommonResp resp = CommonResp.buildFailMsg("操作人信息获取失败！");
        try {
            Map<String, String> map = new HashMap<>();
            map.put("empCode", "user001");
            resp = CommonResp.buildSucsDetailMsg(map, "操作人信息获取成功！");
        } catch (Exception e) {
            log.error("getOperatorInfo error: ", e);
        }
        return resp;
    }
}
