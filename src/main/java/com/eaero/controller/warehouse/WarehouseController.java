package com.eaero.controller.warehouse;

import cn.hutool.core.collection.CollUtil;
import com.eaero.CommonResp;
import com.eaero.entity.TraceStoresloc;
import com.eaero.entity.TraceWarehouse;
import com.eaero.service.ITraceStoreslocService;
import com.eaero.service.ITraceWarehouseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zb
 * @date 2023/03/14 014 10:22
 */
@CrossOrigin
@RequestMapping
@Slf4j
@RestController
public class WarehouseController {

    @Autowired
    private ITraceWarehouseService traceWarehouseService;

    @Autowired
    private ITraceStoreslocService traceStoreslocService;

    /**
     * 19. 获取仓库信息
     * @param warehouse
     * @return
     */
    @GetMapping("/GetWarehouseInfo.do")
    public CommonResp getWarehouseInfo(@RequestParam(required = false) String warehouse) {
        CommonResp resp = CommonResp.buildFailMsg("仓库信息获取失败！");
        try {
            List<TraceWarehouse> traceWarehouseList = traceWarehouseService.getWarehouseInfo(warehouse);
            if (CollUtil.isNotEmpty(traceWarehouseList)) {
                resp = CommonResp.buildSucsDataListMsg(traceWarehouseList, "数据获取成功！");
            }
        } catch (Exception e) {
            log.error("WarehouseController getWarehouseInfo error: ", e);
        }
        return resp;
    }

    /**
     * 59. 获取定位信息
     * @param warehouse
     * @param storesloc
     * @return
     */
    @GetMapping("/GetStoresloc.do")
    public CommonResp getStoresloc(@RequestParam String warehouse, @RequestParam(required = false) String storesloc) {
        CommonResp resp = CommonResp.buildFailMsg("库存信息获取失败！");
        try {
            List<TraceStoresloc> traceStoreslocs = traceStoreslocService.getStoresloc(warehouse, storesloc);
            if (CollUtil.isNotEmpty(traceStoreslocs)) {
                resp = CommonResp.buildSucsDataListMsg(traceStoreslocs, "数据获取成功！");
            }
        } catch (Exception e) {
            log.error("WarehouseController getStoresloc error: ", e);
        }
        return resp;
    }

}
