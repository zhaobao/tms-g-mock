package com.eaero.controller.exit.pojo;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 1. userCode(String) 员工号，必填
 * 2. returnRemark(String) 退料单备注
 * 3. goReturn（json）退料明细数据集
 * 4. rid(String) 工具顺序号，必填
 * 5. remark(String) 退料明细备注
 * 6. origin(String) 来源，必填（P:PC;A:ATM;H:手持设备;D:工作台）
 *
 * @author zb
 * @date 2023/7/4 16:24
 */
public class TraceCreateToolExitRequest {
    @NotEmpty(message = "来源必填")
    private String origin;
    @NotEmpty(message = "员工号必填")
    private String userCode;
    private String returnRemark;
    @Valid
    private List<TraceExitToolInfoVo> goReturn;

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getReturnRemark() {
        return returnRemark;
    }

    public void setReturnRemark(String returnRemark) {
        this.returnRemark = returnRemark;
    }

    public List<TraceExitToolInfoVo> getGoReturn() {
        return goReturn;
    }

    public void setGoReturn(List<TraceExitToolInfoVo> goReturn) {
        this.goReturn = goReturn;
    }
}
