package com.eaero.controller.exit.pojo;

/**
 * trlId 退料明细 ID
 * trId 退料单 ID
 * returnPnum 退料单号
 * workorderno 工卡指令号
 * partno 工具件号
 * serialno 工具刻号
 * num 退料数量
 * remark 退料备注
 * tpNo 发料单号
 * issueRid 工具发料 RID
 * tlFlag 故障状态（空值/故障信息）
 * imgFlag 图片（Y/N）
 * rid 工具顺序号
 * unitofmeas 单位
 * barcode 条形码
 * rfidId RFID 号；
 * parentPartno 父件号；
 * parentTrno 父刻号；
 * parent 父件顺序号
 * whseDesc 仓库描述, station 站点
 * alternatePartno 真实件号, storesloc 定位
 * warehouse 仓库
 * childNum 子件数量
 * measuringNo 计量号
 * standByPn 替代件号
 * partdescr 件号描述
 * catalogueType 工具类型
 * tlStatus 工具状态（借出/在库）
 * 退料单明细
 */
public class TraceToolExitDetailInfoVo {
    private String tlFlag;
    private String whseDesc;
    private String imgFlag;
    private String station;
    private String remark;
    private String partno;
    private String returnPnum;
    private String tlStatus;
    private String barcode;
    private String workorderno;
    private String warehouse;
    private String issueRid;
    private String parentTrno;
    private String parent;
    private String trId;
    private String tpNo;
    private String rfidId;
    private String unitofmeas;
    private String num;
    private String trlId;
    private String measuringNo;
    private String rid;
    private String catalogueType;
    private String partdescr;
    private String standByPn;
    private String storesloc;
    private String parentPartno;
    private Integer childNum;
    private String serialno;

    public String getTlFlag() {
        return tlFlag;
    }

    public void setTlFlag(String tlFlag) {
        this.tlFlag = tlFlag;
    }

    public String getWhseDesc() {
        return whseDesc;
    }

    public void setWhseDesc(String whseDesc) {
        this.whseDesc = whseDesc;
    }

    public String getImgFlag() {
        return imgFlag;
    }

    public void setImgFlag(String imgFlag) {
        this.imgFlag = imgFlag;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPartno() {
        return partno;
    }

    public void setPartno(String partno) {
        this.partno = partno;
    }

    public String getReturnPnum() {
        return returnPnum;
    }

    public void setReturnPnum(String returnPnum) {
        this.returnPnum = returnPnum;
    }

    public String getTlStatus() {
        return tlStatus;
    }

    public void setTlStatus(String tlStatus) {
        this.tlStatus = tlStatus;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getWorkorderno() {
        return workorderno;
    }

    public void setWorkorderno(String workorderno) {
        this.workorderno = workorderno;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public String getIssueRid() {
        return issueRid;
    }

    public void setIssueRid(String issueRid) {
        this.issueRid = issueRid;
    }

    public String getParentTrno() {
        return parentTrno;
    }

    public void setParentTrno(String parentTrno) {
        this.parentTrno = parentTrno;
    }

    public String getTrId() {
        return trId;
    }

    public void setTrId(String trId) {
        this.trId = trId;
    }

    public String getTpNo() {
        return tpNo;
    }

    public void setTpNo(String tpNo) {
        this.tpNo = tpNo;
    }

    public String getRfidId() {
        return rfidId;
    }

    public void setRfidId(String rfidId) {
        this.rfidId = rfidId;
    }

    public String getUnitofmeas() {
        return unitofmeas;
    }

    public void setUnitofmeas(String unitofmeas) {
        this.unitofmeas = unitofmeas;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getTrlId() {
        return trlId;
    }

    public void setTrlId(String trlId) {
        this.trlId = trlId;
    }

    public String getMeasuringNo() {
        return measuringNo;
    }

    public void setMeasuringNo(String measuringNo) {
        this.measuringNo = measuringNo;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getCatalogueType() {
        return catalogueType;
    }

    public void setCatalogueType(String catalogueType) {
        this.catalogueType = catalogueType;
    }

    public String getPartdescr() {
        return partdescr;
    }

    public void setPartdescr(String partdescr) {
        this.partdescr = partdescr;
    }

    public String getStandByPn() {
        return standByPn;
    }

    public void setStandByPn(String standByPn) {
        this.standByPn = standByPn;
    }

    public String getStoresloc() {
        return storesloc;
    }

    public void setStoresloc(String storesloc) {
        this.storesloc = storesloc;
    }

    public String getParentPartno() {
        return parentPartno;
    }

    public void setParentPartno(String parentPartno) {
        this.parentPartno = parentPartno;
    }

    public Integer getChildNum() {
        return childNum;
    }

    public void setChildNum(Integer childNum) {
        this.childNum = childNum;
    }

    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }
}
