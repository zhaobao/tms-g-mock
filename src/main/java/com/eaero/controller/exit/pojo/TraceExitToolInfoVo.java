package com.eaero.controller.exit.pojo;

import javax.validation.constraints.NotEmpty;

/**
 * @author zb
 * @date 2023/7/5 10:49
 */
public class TraceExitToolInfoVo {
    @NotEmpty(message = "工具需要必填")
    private String rid;
    private String remark;

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
