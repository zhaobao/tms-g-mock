package com.eaero.controller.exit.pojo;

/**
 * 退料单
 */
public class TraceToolExitInfoVo {
    /**
     * 退料单号
     */
    private String returnPnum;
    /**
     * 状态（草稿/提交退料/配送中/关闭）
     */
    private String status;
    /**
     * 退料备注
     */
    private String remark;
    /**
     * 更新时间
     * "updated": "2022-09-20 11:20:42.0",
     */
    private String updated;
    /**
     * 创建时间
     * "created": "2022-09-15 09:39:04.0",
     */
    private String created;
    /**
     * 来源
     */
    private String origin;
    /**
     * 更新人 "700642 叶家荣" 或者 "700642"
     */
    private String updatedUser;
    /**
     * 仓库
     */
    private String type;
    /**
     * 创建人 "700642 叶家荣" 或者 "700642"
     */
    private String createdUser;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getReturnPnum() {
        return returnPnum;
    }

    public void setReturnPnum(String returnPnum) {
        this.returnPnum = returnPnum;
    }


}
