package com.eaero.controller.exit;

import com.eaero.CommonResp;
import com.eaero.controller.exit.pojo.TraceCreateToolExitRequest;
import com.eaero.controller.exit.pojo.TraceToolExitDetailInfoVo;
import com.eaero.controller.exit.pojo.TraceToolExitInfoVo;
import com.eaero.service.ITraceExitRecordService;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zzj
 * @date 2023/3/14 15:36
 * @description
 */
@CrossOrigin
@RequestMapping
@Slf4j
@RestController
public class ToolExitController {

    private final Gson gson = new Gson();

    @Autowired
    private ITraceExitRecordService traceExitRecordService;

    /**
     * 57. 创建退料单
     *
     * @param request
     * @return
     */
    @PostMapping("/goTlReturn.do")
    public CommonResp postGoTlReturn(@RequestBody TraceCreateToolExitRequest request) {
        CommonResp resp = CommonResp.buildFailMsg("生成退料单失败！");
        try {
            return traceExitRecordService.postGoTlReturn(request);
        } catch (Exception e) {
            log.error("ToolExitController postGoTlReturn error: ", e);
        }
        return resp;
    }

    /**
     * 55. 获取退料单
     *
     * @return
     */
    @GetMapping("/goTlReturn.do")
    public CommonResp getGoTlReturn(@RequestParam(required = false) String returnPnum,
                                    @RequestParam(required = false) String createUser,
                                    @RequestParam(required = false) String created,
                                    @RequestParam(required = false) String status,
                                    @RequestParam(required = false) String origin,
                                    @RequestParam(required = false) String type) {
        CommonResp resp = CommonResp.buildFailMsg("没有该退料单信息！");
        try {
            if (StringUtils.isAllEmpty(returnPnum, createUser, created, status, origin, type)) {
                return CommonResp.buildFailMsg("至少传一个参数");
            }
            return traceExitRecordService.getGoTlReturn(returnPnum, createUser, created, status, origin, type);
        } catch (Exception e) {
            log.error("ToolExitController getGoTlReturn error: ", e);
        }
        return resp;
    }

    /**
     * 58. 查询工具退料明细
     *
     * @param returnPnum
     * @return
     */
    @GetMapping("/getTlReturnList.do")
    public CommonResp getTlReturnList(@RequestParam String returnPnum) {
        CommonResp resp = CommonResp.buildFailMsg("没有该退料单信息！");
        try {
            if (StringUtils.isAllEmpty(returnPnum)) {
                return CommonResp.buildFailMsg("参数必传");
            }

            return traceExitRecordService.getTlReturnList(returnPnum);
        } catch (Exception e) {
            log.error("ToolExitController getTlReturnList error: ", e);
        }
        return resp;
    }

}
