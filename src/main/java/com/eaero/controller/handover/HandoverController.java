package com.eaero.controller.handover;

import com.eaero.CommonResp;
import com.eaero.service.ITraceHandoverDetailService;
import com.eaero.service.ITraceHandoverService;
import com.eaero.vo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Description:
 *
 * @author Ye Zhihong
 * @date 2023/3/14 14:29
 */
@CrossOrigin
@RestController
@RequestMapping
public class HandoverController {
    private final Logger log = LoggerFactory.getLogger(HandoverController.class);

    @Autowired
    private ITraceHandoverService handoverService;

    @Autowired
    private ITraceHandoverDetailService handoverDetailService;


    /**
     * 生成交班信息
     * @param request
     * @return
     */
    @PostMapping("/HandingOf.do")
    public CommonResp handingOf(HandingOfRequest request) {
        CommonResp resp = new CommonResp();
        try {
            resp = handoverService.handingOf(request);
        } catch (Exception e) {
            log.error("handingOf error: ", e);
        }
        return resp;
    }

    /**
     * 查询交接班单
     * @param request
     * @return
     */
    @GetMapping("/GetHandoverInfo.do")
    public CommonResp getHandoverInfo(HandoverInfoRequest request) {
        CommonResp resp = new CommonResp();
        try {
            resp = handoverService.getHandoverInfo(request);
        } catch (Exception e) {
            log.error("getHandoverInfo error: ", e);
        }
        return resp;
    }

    /**
     * 查询交接班明细
     * @param request
     * @return
     */
    @GetMapping("/GetHandoverDtlInfo.do")
    public CommonResp getHandoverDtl(HandoverDtlRequest request) {
        CommonResp resp = new CommonResp();
        try {
            resp = handoverDetailService.getHandoverDtlInfo(request);
        } catch (Exception e) {
            log.error("getHandoverDtl error: ", e);
        }
        return resp;
    }

    /**
     * 查询交接班明细定位
     * @param request
     * @return
     */
    @GetMapping("/GetHandoverDtlLoc.do")
    public CommonResp getHandoverDtlLoc(HandoverDtlLocRequest request) {
        CommonResp resp = new CommonResp();
        try {
            resp = handoverDetailService.getHandoverDtlLoc(request);
        } catch (Exception e) {
            log.error("getHandoverDtlLoc error: ", e);
        }
        return resp;
    }

    /**
     * 作废交接班单
     * @param request
     * @return
     */
    @PostMapping("/CancelHandoverDtlInfo.do")
    public CommonResp CancelHandoverDtlInfo(@RequestBody CancelHandoverRequest request) {
        CommonResp resp = new CommonResp();
        try {
            resp = handoverService.cancelHandoverInfo(request);
        } catch (Exception e) {
            log.error("CancelHandoverDtlInfo error: ", e);
        }
        return resp;
    }

    /**
     * 接班
     * @param request
     * @return
     */
    @PostMapping("/doHandover.do")
    public CommonResp doHandover(@RequestBody DoHandoverRequest request) {
        CommonResp resp = new CommonResp();
        try {
            resp = handoverDetailService.doHandover(request);
        } catch (Exception e) {
            log.error("doHandover error: ", e);
        }
        return resp;
    }
}
