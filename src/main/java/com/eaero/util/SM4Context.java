package com.eaero.util;

/**
 * @Title: SM4Context
 * @Description: 功能概述:
 * @Author: Reda.Wupengfei
 * @version:
 * @Date: 2019/7/8
 * @Copyright: Copyright(c)2019 RedaFlight.com All Rights Reserved
 */
public class SM4Context {
    public int mode;

    public int[] sk;

    public boolean isPadding;

    public SM4Context()
    {
        this.mode = 1;
        this.isPadding = true;
        this.sk = new int[32];
    }
}
