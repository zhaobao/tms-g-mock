package com.eaero.util;

import cn.hutool.core.date.DateUtil;
import org.apache.commons.lang3.RandomStringUtils;

import java.time.LocalDateTime;

/**
 * @author zb
 */
public class IdGenUtils {

    public static String genId() {
        return RandomStringUtils.randomAlphanumeric(32);
    }

    /**
     * 生成8位的加密盐
     *
     * @return
     */
    public static String genSalt() {
        return RandomStringUtils.randomAlphanumeric(8);
    }

    public static String genItemNo() {
        return RandomStringUtils.randomNumeric(4);
    }

    /**
     * 取件码6位code
     * @return
     */
    public static String genFetchCode(){return RandomStringUtils.randomNumeric(6);}

    /**
     * 验证码6位code
     * @return
     */
    public static String genVerificationCode(){return RandomStringUtils.randomNumeric(6);}
    /**
     * 生成业务code
     *
     * @return
     */
    public static String genBizCode() {
        String dateStr = DateUtil.format(LocalDateTime.now(), "yyyyMMddHHmmssSSS");
        dateStr = dateStr + RandomStringUtils.randomAlphanumeric(3).toUpperCase();
        return dateStr;
    }

    /**
     * 生成业务code
     *
     * @return
     */
    public static String genCode() {
        String dateStr = DateUtil.format(LocalDateTime.now(), "yyyyMMddHHmmss");
        //dateStr = dateStr + RandomStringUtils.randomAlphanumeric(3).toUpperCase();
        return dateStr;
    }

    /**
     * 生成verifyCodeTicket
     *
     * @return
     */
    public static String genTicket() {
        return RandomStringUtils.randomAlphanumeric(24);
    }
}
