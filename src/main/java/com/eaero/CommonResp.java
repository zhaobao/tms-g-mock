package com.eaero;

import com.eaero.vo.consts.Constants;
import lombok.Data;

@Data
public class CommonResp {

    private String retCode;

    private String retMsg;


    private Object dataList;

    private Object detail;

    public static CommonResp buildSucsDataListMsg(Object data, String message) {
        CommonResp ajaxMessage = new CommonResp();
        ajaxMessage.setRetCode(Constants.ResponseCode.SUCCESS);
        ajaxMessage.setDataList(data);
        ajaxMessage.setRetMsg(message);
        return ajaxMessage;
    }

    public static CommonResp buildSucsDetailMsg(Object data, String message) {
        CommonResp ajaxMessage = new CommonResp();
        ajaxMessage.setRetCode(Constants.ResponseCode.SUCCESS);
        ajaxMessage.setDetail(data);
        ajaxMessage.setRetMsg(message);
        return ajaxMessage;
    }

    public static CommonResp buildFailMsg(String data) {
        return buildFailMsg(Constants.ResponseCode.ERROR, data);
    }
    public static CommonResp buildSuccessMsg(String data) {
        CommonResp ajaxMessage = new CommonResp();
        ajaxMessage.setRetCode(Constants.ResponseCode.SUCCESS);
        ajaxMessage.setRetMsg(data);
        return ajaxMessage;
    }

    public static CommonResp buildFailMsg(String code, String message) {
        CommonResp ajaxMessage = new CommonResp();
        ajaxMessage.setRetCode(code);
        ajaxMessage.setRetMsg(message);
        return ajaxMessage;
    }
}