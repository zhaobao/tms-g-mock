package com.eaero.mapper;

import com.eaero.entity.TraceToolStock;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Ye Zhihong
* @description 针对表【trace_tool_stock】的数据库操作Mapper
* @createDate 2023-03-17 16:24:50
* @Entity com.eaero.entity.TraceToolStock
*/
public interface TraceToolStockMapper extends BaseMapper<TraceToolStock> {

}




