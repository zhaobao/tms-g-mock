package com.eaero.mapper;

import com.eaero.entity.TraceWarehouse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
@Mapper
public interface TraceWarehouseMapper extends BaseMapper<TraceWarehouse> {

}
