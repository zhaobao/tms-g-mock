package com.eaero.mapper;

import com.eaero.entity.TraceExitRecordDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author luofeng
 * @since 2023-07-07
 */
@Mapper
public interface TraceExitRecordDetailMapper extends BaseMapper<TraceExitRecordDetail> {

}
