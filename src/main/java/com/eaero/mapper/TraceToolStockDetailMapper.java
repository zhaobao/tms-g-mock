package com.eaero.mapper;

import com.eaero.entity.TraceToolStockDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Ye Zhihong
* @description 针对表【trace_tool_stock_detail】的数据库操作Mapper
* @createDate 2023-03-17 16:26:35
* @Entity com.eaero.entity.TraceToolStockDetail
*/
public interface TraceToolStockDetailMapper extends BaseMapper<TraceToolStockDetail> {

}




