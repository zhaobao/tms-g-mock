package com.eaero.mapper;

import com.eaero.entity.TraceToolPick;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
@Mapper
public interface TraceToolPickMapper extends BaseMapper<TraceToolPick> {

    List<TraceToolPick> getToolPickHD(@Param("tpNo") String tpNo,
                                      @Param("createdUser") String createdUser,
                                      @Param("createdDate") String createdDate,
                                      @Param("status") String status,
                                      @Param("updatedUsers") String updatedUsers);
}
