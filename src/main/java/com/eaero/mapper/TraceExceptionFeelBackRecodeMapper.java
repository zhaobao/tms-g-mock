package com.eaero.mapper;

import com.eaero.entity.TraceExceptionFeelBackRecode;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zb
 * @since 2023-04-08
 */
@Mapper
public interface TraceExceptionFeelBackRecodeMapper extends BaseMapper<TraceExceptionFeelBackRecode> {

}
