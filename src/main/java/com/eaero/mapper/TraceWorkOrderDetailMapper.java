package com.eaero.mapper;

import com.eaero.entity.TraceWorkOrderDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.eaero.vo.WorkOrderDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
@Mapper
public interface TraceWorkOrderDetailMapper extends BaseMapper<TraceWorkOrderDetail> {

    /**
     * 根据申请明细 ID获取list
     * @param rids
     * @return
     */
    List<WorkOrderDetail> getWorkOrderDetailByRid(@Param("rids") List<String> rids);

}
