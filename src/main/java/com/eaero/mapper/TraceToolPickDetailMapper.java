package com.eaero.mapper;

import com.eaero.entity.TraceToolPickDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
@Mapper
public interface TraceToolPickDetailMapper extends BaseMapper<TraceToolPickDetail> {

}
