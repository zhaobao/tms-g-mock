package com.eaero.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.eaero.entity.TraceJobCard;
import com.eaero.entity.TraceUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
@Mapper
public interface TraceJobCardMapper extends BaseMapper<TraceJobCard> {

}
