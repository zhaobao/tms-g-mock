package com.eaero;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TmsGMockApplication {

    public static void main(String[] args) {
        SpringApplication.run(TmsGMockApplication.class, args);
    }

}
