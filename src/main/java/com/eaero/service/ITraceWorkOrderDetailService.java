package com.eaero.service;

import com.eaero.CommonResp;
import com.eaero.controller.workOrder.pojo.ReplyToolReqDetailRequest;
import com.eaero.entity.TraceWorkOrderDetail;
import com.baomidou.mybatisplus.extension.service.IService;
import com.eaero.service.workorder.pojo.WorkOrderDetailRequset;
import com.eaero.vo.GetToolReqdetailSumRequest;
import com.eaero.vo.UpdateToolReqdetailPnRequest;
import com.eaero.vo.UpdateWhsTypeRequest;
import com.eaero.vo.WorkOrderDetail;

import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
public interface ITraceWorkOrderDetailService extends IService<TraceWorkOrderDetail> {
    /**
     * 查询工具申请单明细
     *
     * @param orderDetailRequset
     * @return
     */
    CommonResp getWorkOrderDetail(WorkOrderDetailRequset orderDetailRequset);

    /**
     * 生成工具申请单明细
     */
    void genWorkOrderDetail();

    CommonResp replyToolReqDetail(ReplyToolReqDetailRequest request);

    CommonResp updateToolReqdetailPn(UpdateToolReqdetailPnRequest request);
    CommonResp updateWhsType(UpdateWhsTypeRequest request);

    CommonResp getToolReqdetailSum(GetToolReqdetailSumRequest request);

    /* List<RequisitionWorkDetail> getWorkOrderDetailByWorkOrderCode(String workOrderCode) throws IOException;
    CommonResp updateToolReqdetailPn(UpdateToolReqdetailPnRequest request);

    CommonResp updateWhsType(UpdateWhsTypeRequest request);

    /* List<RequisitionWorkDetail> getWorkOrderDetailByWorkOrderCode(String workOrderCode) throws IOException;

    *//**
     * 明细汇总
     * @return
     * @throws IOException
     *//*
    List<RequisitionWorkDetailCollect> getRequisitionWorkDetailCollect() throws IOException;

    List<RequisitionWorkDetailCollect> getRequisitionWorkDetailCollectByTailNo(String tailNo) throws IOException;*/

    /**
     * 根据申请明细 ID获取list
     * @param rids
     * @return
     */
    List<WorkOrderDetail> getWorkOrderDetailByRid(List<String> rids);
}
