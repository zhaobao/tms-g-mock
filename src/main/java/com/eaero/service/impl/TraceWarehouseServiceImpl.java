package com.eaero.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.eaero.entity.TraceWarehouse;
import com.eaero.mapper.TraceWarehouseMapper;
import com.eaero.service.ITraceWarehouseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
@Service
public class TraceWarehouseServiceImpl extends ServiceImpl<TraceWarehouseMapper, TraceWarehouse> implements ITraceWarehouseService {

    @Override
    public List<TraceWarehouse> getWarehouseInfo(String warehouse) {
        List<TraceWarehouse> traceWarehouseList  =  this.list(Wrappers.<TraceWarehouse>lambdaQuery()
                .select(TraceWarehouse::getWarehouse, TraceWarehouse::getCommentText, TraceWarehouse::getStation, TraceWarehouse::getStationFacility)
                .eq(StrUtil.isNotBlank(warehouse),TraceWarehouse::getWarehouse, warehouse)
        );
        return traceWarehouseList;
    }

}
