package com.eaero.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.eaero.entity.TraceToolDeliver;
import com.eaero.mapper.TraceToolDeliverMapper;
import com.eaero.service.ITraceToolDeliverService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.eaero.vo.ToolDeliverRequest;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
@Service
public class TraceToolDeliverServiceImpl extends ServiceImpl<TraceToolDeliverMapper, TraceToolDeliver> implements ITraceToolDeliverService {

    @Override
    public List<TraceToolDeliver> getToolDeliver(ToolDeliverRequest request) {
        return this.list(Wrappers.<TraceToolDeliver>lambdaQuery()
                .select(TraceToolDeliver::getTpNo, TraceToolDeliver::getDeliverCode, TraceToolDeliver::getTailNo, TraceToolDeliver::getCreateUser, TraceToolDeliver::getCreateDate, TraceToolDeliver::getWarehouse, TraceToolDeliver::getStatus)
                .like(StrUtil.isNotBlank(request.getDeliverCode()), TraceToolDeliver::getDeliverCode, request.getDeliverCode())
                .like(StrUtil.isNotBlank(request.getTpNo()), TraceToolDeliver::getTpNo, request.getTpNo())
                .like(StrUtil.isNotBlank(request.getCreateUser()), TraceToolDeliver::getCreateUser, request.getCreateUser())
                .like(StrUtil.isNotBlank(request.getCreateDate()), TraceToolDeliver::getCreateDate, request.getCreateDate())
                .eq(StrUtil.isNotBlank(request.getStatus()), TraceToolDeliver::getStatus, request.getStatus())
                .eq(StrUtil.isNotBlank(request.getTailNo()), TraceToolDeliver::getTailNo, request.getTailNo())
                .eq(StrUtil.isNotBlank(request.getWarehouse()), TraceToolDeliver::getWarehouse, request.getWarehouse())
        );
    }
}
