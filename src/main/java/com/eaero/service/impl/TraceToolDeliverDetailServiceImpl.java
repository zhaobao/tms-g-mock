package com.eaero.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.eaero.entity.TraceToolDeliverDetail;
import com.eaero.mapper.TraceToolDeliverDetailMapper;
import com.eaero.service.ITraceToolDeliverDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.eaero.vo.ToolDeliverDetail;
import com.eaero.vo.ToolDeliverDetailRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
@Service
public class TraceToolDeliverDetailServiceImpl extends ServiceImpl<TraceToolDeliverDetailMapper, TraceToolDeliverDetail> implements ITraceToolDeliverDetailService {

    @Override
    public List<ToolDeliverDetail> getToolDeliverDetail(ToolDeliverDetailRequest request) {
        List<TraceToolDeliverDetail> traceToolDeliverDetails = this.list(Wrappers.<TraceToolDeliverDetail>lambdaQuery()
                .like(StrUtil.isNotBlank(request.getDeliverCode()), TraceToolDeliverDetail::getDeliverCode, request.getDeliverCode())
                .like(StrUtil.isNotBlank(request.getIssueUser()), TraceToolDeliverDetail::getIssueUser, request.getIssueUser())
                .eq(StrUtil.isNotBlank(request.getIssueDate()), TraceToolDeliverDetail::getIssueDate, request.getIssueDate())
                .like(StrUtil.isNotBlank(request.getCreateUser()), TraceToolDeliverDetail::getCreateUser, request.getCreateUser())
                .eq(StrUtil.isNotBlank(request.getStatus()), TraceToolDeliverDetail::getStatus, request.getStatus())
                .like(StrUtil.isNotBlank(request.getWorkorderno()), TraceToolDeliverDetail::getWorkorderno, request.getWorkorderno())
                .like(StrUtil.isNotBlank(request.getTailNo()), TraceToolDeliverDetail::getTailNo, request.getTailNo())
                .like(StrUtil.isNotBlank(request.getVisitDesc()), TraceToolDeliverDetail::getVisitDesc, request.getVisitDesc())
                .like(StrUtil.isNotBlank(request.getPartno()), TraceToolDeliverDetail::getPartno, request.getPartno())
                .like(StrUtil.isNotBlank(request.getSerialno()), TraceToolDeliverDetail::getSerialno, request.getSerialno())
                .like(StrUtil.isNotBlank(request.getDeliverUser()), TraceToolDeliverDetail::getDeliverUser, request.getDeliverUser())
                .eq(StrUtil.isNotBlank(request.getDeliverDate()), TraceToolDeliverDetail::getDeliverDate, request.getDeliverDate())
                .eq(StrUtil.isNotBlank(request.getUpdateDate()), TraceToolDeliverDetail::getUpdateDate, request.getUpdateDate())
                .like(StrUtil.isNotBlank(request.getUpdateUser()), TraceToolDeliverDetail::getUpdateUser, request.getUpdateUser())
                .eq(StrUtil.isNotBlank(request.getWarehouse()), TraceToolDeliverDetail::getWarehouse, request.getWarehouse())
        );
        List<ToolDeliverDetail> toolDeliverDetails = new ArrayList<>();
        traceToolDeliverDetails.forEach(traceToolDeliverDetail -> {
            ToolDeliverDetail toolDeliverDetail = new ToolDeliverDetail();
            BeanUtil.copyProperties(traceToolDeliverDetail, toolDeliverDetail);
            toolDeliverDetail.setTddId(traceToolDeliverDetail.getId());
            toolDeliverDetails.add(toolDeliverDetail);
        });
        return toolDeliverDetails;
    }

}
