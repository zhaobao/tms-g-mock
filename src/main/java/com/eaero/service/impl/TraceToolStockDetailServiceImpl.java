package com.eaero.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.eaero.entity.TraceToolStockDetail;
import com.eaero.service.ITraceToolStockDetailService;
import com.eaero.mapper.TraceToolStockDetailMapper;
import org.springframework.stereotype.Service;

/**
* @author Ye Zhihong
* @description 针对表【trace_tool_stock_detail】的数据库操作Service实现
* @createDate 2023-03-17 16:26:35
*/
@Service
public class TraceToolStockDetailServiceImpl extends ServiceImpl<TraceToolStockDetailMapper, TraceToolStockDetail>
    implements ITraceToolStockDetailService {

}




