package com.eaero.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.eaero.entity.TraceToolDeliverSign;
import com.eaero.mapper.TraceToolDeliverSignMapper;
import com.eaero.service.ITraceToolDeliverSignService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.eaero.vo.ToolDeliverSign;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
@Service
public class TraceToolDeliverSignServiceImpl extends ServiceImpl<TraceToolDeliverSignMapper, TraceToolDeliverSign> implements ITraceToolDeliverSignService {

    @Override
    public ToolDeliverSign getDeliverSign(String tddId) {
        ToolDeliverSign toolDeliverSign = new ToolDeliverSign();
        List<TraceToolDeliverSign> traceToolDeliverSigns = this.list(Wrappers.<TraceToolDeliverSign>lambdaQuery()
                .eq(TraceToolDeliverSign::getTddId, tddId)
        );
        if (ObjectUtil.isEmpty(traceToolDeliverSigns)) {
            return null;
        }
        toolDeliverSign.setTddId(tddId);
        toolDeliverSign.setDeliverCode(traceToolDeliverSigns.get(0).getDeliverCode());
        List<TraceToolDeliverSign> signList = new ArrayList<>();
        traceToolDeliverSigns.forEach(traceToolDeliverSign -> {
            TraceToolDeliverSign sign = new TraceToolDeliverSign();
            sign.setUpdateUser(traceToolDeliverSign.getUpdateUser());
            sign.setUpdateDate(traceToolDeliverSign.getUpdateDate());
            signList.add(sign);
        });
        toolDeliverSign.setSignList(signList);
        return toolDeliverSign;
    }
}
