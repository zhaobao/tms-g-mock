package com.eaero.service.impl;

import com.eaero.entity.TraceExceptionFeelBackRecode;
import com.eaero.mapper.TraceExceptionFeelBackRecodeMapper;
import com.eaero.service.ITraceExceptionFeelBackRecodeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zb
 * @since 2023-04-08
 */
@Service
public class TraceExceptionFeelBackRecodeServiceImpl extends ServiceImpl<TraceExceptionFeelBackRecodeMapper, TraceExceptionFeelBackRecode> implements ITraceExceptionFeelBackRecodeService {

}
