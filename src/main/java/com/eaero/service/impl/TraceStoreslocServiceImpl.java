package com.eaero.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.eaero.entity.TraceStoresloc;
import com.eaero.mapper.TraceStoreslocMapper;
import com.eaero.service.ITraceStoreslocService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
@Service
public class TraceStoreslocServiceImpl extends ServiceImpl<TraceStoreslocMapper, TraceStoresloc> implements ITraceStoreslocService {

    @Override
    public List<TraceStoresloc> getStoresloc(String warehouse, String storesloc) {
        List<TraceStoresloc> traceStoreslocs = this.list(Wrappers.<TraceStoresloc>lambdaQuery()
                .select(TraceStoresloc::getStoresloc)
                .eq(TraceStoresloc::getWarehouse, warehouse)
                .like(StrUtil.isNotBlank(storesloc), TraceStoresloc::getStoresloc, storesloc)
        );
        return traceStoreslocs;
    }
}
