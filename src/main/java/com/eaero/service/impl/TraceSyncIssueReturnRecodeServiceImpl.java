package com.eaero.service.impl;

import com.eaero.entity.TraceSyncIssueReturnRecode;
import com.eaero.mapper.TraceSyncIssueReturnRecodeMapper;
import com.eaero.service.ITraceSyncIssueReturnRecodeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zb
 * @since 2023-04-08
 */
@Service
public class TraceSyncIssueReturnRecodeServiceImpl extends ServiceImpl<TraceSyncIssueReturnRecodeMapper, TraceSyncIssueReturnRecode> implements ITraceSyncIssueReturnRecodeService {

}
