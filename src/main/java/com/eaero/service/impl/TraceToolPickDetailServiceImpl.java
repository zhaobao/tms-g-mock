package com.eaero.service.impl;

import com.eaero.entity.TraceToolPickDetail;
import com.eaero.mapper.TraceToolPickDetailMapper;
import com.eaero.service.ITraceToolPickDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
@Service
public class TraceToolPickDetailServiceImpl extends ServiceImpl<TraceToolPickDetailMapper, TraceToolPickDetail> implements ITraceToolPickDetailService {

}
