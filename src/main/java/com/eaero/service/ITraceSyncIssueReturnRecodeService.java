package com.eaero.service;

import com.eaero.entity.TraceSyncIssueReturnRecode;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zb
 * @since 2023-04-08
 */
public interface ITraceSyncIssueReturnRecodeService extends IService<TraceSyncIssueReturnRecode> {

}
