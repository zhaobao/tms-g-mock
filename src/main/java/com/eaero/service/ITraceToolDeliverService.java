package com.eaero.service;

import com.eaero.entity.TraceToolDeliver;
import com.baomidou.mybatisplus.extension.service.IService;
import com.eaero.vo.ToolDeliverRequest;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
public interface ITraceToolDeliverService extends IService<TraceToolDeliver> {

    /**
     * 查询工具配送单据
     * @param request
     * @return
     */
    List<TraceToolDeliver> getToolDeliver(ToolDeliverRequest request);

}
