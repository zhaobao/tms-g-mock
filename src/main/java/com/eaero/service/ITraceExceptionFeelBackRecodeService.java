package com.eaero.service;

import com.eaero.entity.TraceExceptionFeelBackRecode;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zb
 * @since 2023-04-08
 */
public interface ITraceExceptionFeelBackRecodeService extends IService<TraceExceptionFeelBackRecode> {

}
