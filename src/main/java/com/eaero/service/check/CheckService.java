package com.eaero.service.check;

import com.eaero.service.check.pojo.CheckInfo;
import com.eaero.service.check.pojo.CheckInfoDetail;

import java.io.IOException;
import java.util.List;

/**
 * @description: 盘点
 * @author: WBL
 * @date: 2023-02-24
 * @time: 16:55
 */
public interface CheckService {

    /**
     * 盘点单
     * @return
     */
    List<CheckInfo> getCheckInfo();

    List<CheckInfo> getCheckInfoAll() throws IOException;

    /**
     * 盘点单明细
     * @param checkInfoNum
     * @return
     */
    List<CheckInfoDetail> getCheckInfoDetail(String checkInfoNum) throws IOException;


    List<CheckInfoDetail> getCheckInfoDetailAll(List<CheckInfo> checkInfoList);
}
