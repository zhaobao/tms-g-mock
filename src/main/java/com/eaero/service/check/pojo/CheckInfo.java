package com.eaero.service.check.pojo;

/**
 * @description: 盘点查询信息
 * @author: WBL
 * @date: 2023-02-23
 * @time: 18:12
 */
public class CheckInfo {

    private String stockPnum;
    private String createUser;
    private String created;
    private String updatedUser;
    private String updated;
    private String remark;
    private String status;
    private String origin;
    private String warehouse;

    public String getStockPnum() {
        return stockPnum;
    }

    public void setStockPnum(String stockPnum) {
        this.stockPnum = stockPnum;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    @Override
    public String toString() {
        return "CheckInfo{" +
                "stockPnum='" + stockPnum + '\'' +
                ", createUser='" + createUser + '\'' +
                ", created='" + created + '\'' +
                ", updatedUser='" + updatedUser + '\'' +
                ", updated='" + updated + '\'' +
                ", remark='" + remark + '\'' +
                ", status='" + status + '\'' +
                ", origin='" + origin + '\'' +
                ", warehouse='" + warehouse + '\'' +
                '}';
    }
}
