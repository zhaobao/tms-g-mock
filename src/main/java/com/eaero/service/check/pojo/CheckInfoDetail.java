package com.eaero.service.check.pojo;

/**
 * @description: 盘点明细
 * @author: WBL
 * @date: 2023-02-24
 * @time: 10:09
 */
public class CheckInfoDetail {

    private String tsId;
    private String tsdId;
    private String stockPnum;
    private String warehouse;
    private String partno;
    private String serialno;
    private String storesloc;
    private String checkStatus;
    private String checkDate;
    private String checkUser;
    private String checkRemark;
    private String status;

    public String getTsId() {
        return tsId;
    }

    public void setTsId(String tsId) {
        this.tsId = tsId;
    }

    public String getTsdId() {
        return tsdId;
    }

    public void setTsdId(String tsdId) {
        this.tsdId = tsdId;
    }

    public String getStockPnum() {
        return stockPnum;
    }

    public void setStockPnum(String stockPnum) {
        this.stockPnum = stockPnum;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public String getPartno() {
        return partno;
    }

    public void setPartno(String partno) {
        this.partno = partno;
    }

    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    public String getStoresloc() {
        return storesloc;
    }

    public void setStoresloc(String storesloc) {
        this.storesloc = storesloc;
    }

    public String getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(String checkStatus) {
        this.checkStatus = checkStatus;
    }

    public String getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(String checkDate) {
        this.checkDate = checkDate;
    }

    public String getCheckUser() {
        return checkUser;
    }

    public void setCheckUser(String checkUser) {
        this.checkUser = checkUser;
    }

    public String getCheckRemark() {
        return checkRemark;
    }

    public void setCheckRemark(String checkRemark) {
        this.checkRemark = checkRemark;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "CheckInfoDetail{" +
                "tsId='" + tsId + '\'' +
                ", tsdId='" + tsdId + '\'' +
                ", stockPnum='" + stockPnum + '\'' +
                ", warehouse='" + warehouse + '\'' +
                ", partno='" + partno + '\'' +
                ", serialno='" + serialno + '\'' +
                ", storesloc='" + storesloc + '\'' +
                ", checkStatus='" + checkStatus + '\'' +
                ", checkDate='" + checkDate + '\'' +
                ", checkUser='" + checkUser + '\'' +
                ", checkRemark='" + checkRemark + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
