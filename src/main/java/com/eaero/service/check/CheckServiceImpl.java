package com.eaero.service.check;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.eaero.service.check.pojo.CheckInfo;
import com.eaero.service.check.pojo.CheckInfoDetail;
import com.eaero.service.tool.ToolService;
import com.eaero.service.tool.pojo.ToolInfo;
import com.eaero.service.user.UserInfoService;
import com.eaero.service.user.pojo.UserInfo;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: WBL
 * @date: 2023-02-24
 * @time: 16:55
 */
@Service
public class CheckServiceImpl implements CheckService {

    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private ToolService toolService;

    private Gson gson = new Gson();

    @Override
    public List<CheckInfo> getCheckInfo() {
        List<UserInfo> userInfoAll = userInfoService.getUserInfoAll();
        ArrayList<String> statusList = Lists.newArrayList("开放", "盘点中", "关闭", "作废", "开放", "盘点中", "关闭", "作废");
        ArrayList<String> originList = Lists.newArrayList("P", "H", "D", "P", "H", "D", "P", "H", "D", "P", "H", "D");
        ArrayList<CheckInfo> list = Lists.newArrayList();
        for (int i = 0; i < 1; i++) {
            CheckInfo checkInfo = new CheckInfo();
            checkInfo.setStockPnum(StrUtil.format("RT{}", RandomUtil.randomNumbers(8)));
            checkInfo.setCreateUser(userInfoAll.get(0).getUserName());
            checkInfo.setCreated(DateUtil.format(LocalDateTime.now(), "yyyy-MM-dd"));
            checkInfo.setUpdatedUser(userInfoAll.get(1).getUserName());
            checkInfo.setUpdated(DateUtil.format(LocalDateTime.now(), "yyyy-MM-dd"));
            checkInfo.setRemark(StrUtil.format("盘点备注{}", RandomStringUtils.randomAlphanumeric(8)));
            checkInfo.setStatus(statusList.get(i));
            checkInfo.setOrigin(originList.get(i));
            checkInfo.setWarehouse("warehouse1");
            list.add(checkInfo);
        }
        for (int i = 0; i < 1; i++) {
            CheckInfo checkInfo = new CheckInfo();
            checkInfo.setStockPnum(StrUtil.format("RT{}", RandomUtil.randomNumbers(8)));
            checkInfo.setCreateUser(userInfoAll.get(1).getUserName());
            checkInfo.setCreated(DateUtil.format(LocalDateTime.now(), "yyyy-MM-dd"));
            checkInfo.setUpdatedUser(userInfoAll.get(2).getUserName());
            checkInfo.setUpdated(DateUtil.format(LocalDateTime.now(), "yyyy-MM-dd"));
            checkInfo.setRemark(StrUtil.format("盘点备注{}", RandomStringUtils.randomAlphanumeric(8)));
            checkInfo.setStatus(statusList.get(i));
            checkInfo.setOrigin(originList.get(i));
            checkInfo.setWarehouse("warehouse2");
            list.add(checkInfo);
        }
        for (int i = 0; i < 5; i++) {
            CheckInfo checkInfo = new CheckInfo();
            checkInfo.setStockPnum(StrUtil.format("RT{}", RandomUtil.randomNumbers(8)));
            checkInfo.setCreateUser(userInfoAll.get(2).getUserName());
            checkInfo.setCreated(DateUtil.format(LocalDateTime.now(), "yyyy-MM-dd"));
            checkInfo.setUpdatedUser(userInfoAll.get(1).getUserName());
            checkInfo.setUpdated(DateUtil.format(LocalDateTime.now(), "yyyy-MM-dd"));
            checkInfo.setRemark(StrUtil.format("盘点备注{}", RandomStringUtils.randomAlphanumeric(8)));
            checkInfo.setStatus(statusList.get(i));
            checkInfo.setOrigin(originList.get(i));
            checkInfo.setWarehouse("warehouse" + i);
            list.add(checkInfo);
        }
        return list;
    }

    @Override
    public List<CheckInfo> getCheckInfoAll() throws IOException {
        String json = new String(Files.readAllBytes(Paths.get("src/main/resources/checkInfos.json")));
        List<CheckInfo> checkInfoDetails = gson.fromJson(json, new TypeToken<List<CheckInfo>>() {
        }.getType());
        return checkInfoDetails;
    }

    @Override
    public List<CheckInfoDetail> getCheckInfoDetail(String checkInfoNum) throws IOException {
        String json = new String(Files.readAllBytes(Paths.get("src/main/resources/checkInfoDetails.json")));
        List<CheckInfoDetail> checkInfoDetails = gson.fromJson(json, new TypeToken<List<CheckInfoDetail>>() {
        }.getType());

        ArrayList<CheckInfoDetail> checkInfoDetail = Lists.newArrayList();
        for (CheckInfoDetail infoDetail : checkInfoDetails) {
            if (StrUtil.equals(infoDetail.getStockPnum(), checkInfoNum)) {
                checkInfoDetail.add(infoDetail);
            }
        }
        return checkInfoDetail;
    }

    @Override
    public List<CheckInfoDetail> getCheckInfoDetailAll(List<CheckInfo> checkInfo) {
        List<ToolInfo> allTool1 = toolService.getAllToolByWarehouse("warehouse1");
        List<ToolInfo> allTool2 = toolService.getAllToolByWarehouse("warehouse2");
        List<CheckInfoDetail> detailList = Lists.newArrayList();
        List<String> checkStatusList = Lists.newArrayList("在库", "借出", "在库", "借出", "在库", "借出", "在库", "借出", "在库", "借出",
                "在库", "借出", "在库", "借出", "在库", "借出", "在库", "借出", "在库", "借出", "在库", "借出", "在库", "借出", "在库",
                "借出", "在库", "借出", "在库", "借出","借出", "在库", "借出", "在库", "借出","借出", "在库", "借出", "在库", "借出","借出", "在库",
                "借出", "在库", "借出","借出", "在库", "借出", "在库", "借出","借出", "在库", "借出", "在库", "借出","借出", "在库"
                , "借出", "在库", "借出", "在库", "借出", "在库", "借出","借出", "在库", "借出", "在库", "借出", "在库", "借出"
                , "在库", "借出","借出", "在库", "借出", "在库", "借出", "在库", "借出", "在库", "借出","借出", "在库", "借出", "在库", "借出", "在库", "借出", "在库", "借出","借出", "在库", "借出", "在库", "借出");


        String s1 = RandomStringUtils.randomAlphanumeric(16);
        String s2 = RandomStringUtils.randomAlphanumeric(16);
        for (int i = 0; i < 15; i++) {
            CheckInfoDetail detail = new CheckInfoDetail();
            detail.setTsId(RandomStringUtils.randomAlphanumeric(16));
            detail.setTsdId(s1);
            detail.setStockPnum(checkInfo.get(0).getStockPnum());
            detail.setWarehouse(allTool1.get(i).getWarehouse());
            detail.setPartno(allTool1.get(i).getPartno());
            detail.setSerialno(allTool1.get(i).getTrno());
            detail.setStoresloc(allTool1.get(i).getStoresloc());
            detail.setCheckStatus(checkStatusList.get(i));
            ThreadUtil.sleep(1000);
            detail.setCheckDate(DateUtil.format(LocalDateTime.now(), "yyyy-MM-dd HH:mm:ss"));
            detail.setCheckUser(checkInfo.get(0).getUpdatedUser());
            detail.setCheckRemark(StrUtil.format("盘点明细备注{}", RandomStringUtils.randomAlphanumeric(8)));
            detail.setStatus(checkInfo.get(0).getStatus());
            detailList.add(detail);
            ThreadUtil.sleep(1000);
        }
        for (int i = 0; i < allTool2.size(); i++) {
            CheckInfoDetail detail = new CheckInfoDetail();
            detail.setTsId(RandomStringUtils.randomAlphanumeric(16));
            detail.setTsdId(s2);
            detail.setStockPnum(checkInfo.get(1).getStockPnum());
            detail.setWarehouse(allTool2.get(i).getWarehouse());
            detail.setPartno(allTool2.get(i).getPartno());
            detail.setSerialno(allTool2.get(i).getTrno());
            detail.setStoresloc(allTool2.get(i).getStoresloc());
            detail.setCheckStatus(checkStatusList.get(i));
            ThreadUtil.sleep(1000);
            detail.setCheckDate(DateUtil.format(LocalDateTime.now(), "yyyy-MM-dd HH:mm:ss"));
            detail.setCheckUser(checkInfo.get(1).getUpdatedUser());
            detail.setCheckRemark(StrUtil.format("盘点明细备注{}", RandomStringUtils.randomAlphanumeric(8)));
            detail.setStatus(checkInfo.get(1).getStatus());
            detailList.add(detail);
            ThreadUtil.sleep(1000);
        }
        return detailList;
    }
}
