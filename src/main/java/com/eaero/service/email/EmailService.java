package com.eaero.service.email;

import com.eaero.CommonResp;
import com.eaero.vo.SendEmailRequest;

/**
 * @author zzj
 * @date 2023/3/20 9:37
 * @description
 */
public interface EmailService {

    /**
     * 通过该接口发送指定的邮件标题、内容给指定手机号
     * 判断 标题 内容 收件人员工号不得为空
     * @param request
     * @return
     */
    CommonResp sendEmail(SendEmailRequest request);

}
