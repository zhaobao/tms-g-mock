package com.eaero.service;

import com.eaero.entity.TraceToolDeliverSign;
import com.baomidou.mybatisplus.extension.service.IService;
import com.eaero.vo.ToolDeliverSign;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
public interface ITraceToolDeliverSignService extends IService<TraceToolDeliverSign> {

    /**
     * 根据配送明细 ID，获取该配送明细的签收流水
     * @param tddId
     * @return
     */
    ToolDeliverSign getDeliverSign(String tddId);

}
