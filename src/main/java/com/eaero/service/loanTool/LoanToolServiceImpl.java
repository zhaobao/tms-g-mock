package com.eaero.service.loanTool;

import com.eaero.CommonResp ;
import com.eaero.service.loanTool.pojo.TraceLoanToolInfo;
import com.eaero.service.tool.ToolService;
import com.eaero.service.tool.pojo.ToolInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zb
 * @date 2023/03/03 003 14:46
 */
@Service
public class LoanToolServiceImpl implements LoanToolService{
    @Autowired
    ToolService toolService;

    @Override
    public CommonResp getLoanToolList() {
        CommonResp commonResp = new CommonResp();
        commonResp.setRetCode("0");
        commonResp.setRetMsg("成功");
        List<ToolInfo> list = new ArrayList<>();
        List<ToolInfo> toolInfoList1 = toolService.getAllToolByRfidId("RFIDP20230224007_C12");
        List<ToolInfo> toolInfoList2 = toolService.getAllToolByRfidId("RFIDP20230224007_C13");
        list.addAll(toolInfoList1);
        list.addAll(toolInfoList2);
        List<TraceLoanToolInfo> traceLoanToolInfoList = list.stream().map(toolInfo -> {
            TraceLoanToolInfo traceLoanToolInfo = new TraceLoanToolInfo();
            traceLoanToolInfo.setParent("P"+toolInfo.getRid());
            traceLoanToolInfo.setIssueDate("2023-02-24 15:52:11");
            traceLoanToolInfo.setIssueRid(toolInfo.getRid());
            traceLoanToolInfo.setAcno("飞机001");
            traceLoanToolInfo.setIssueTo("user001");
            traceLoanToolInfo.setJcWo("123456");
            traceLoanToolInfo.setEmpCode(toolInfo.getRfidId());
            traceLoanToolInfo.setOpRemark("借用一下");
            traceLoanToolInfo.setWhseDesc(toolInfo.getWhseDesc());
            traceLoanToolInfo.setIssueRemind(toolInfo.getIssueRemind());
            traceLoanToolInfo.setPartno(toolInfo.getPartno());
            traceLoanToolInfo.setBarcode(toolInfo.getBarcode());
            traceLoanToolInfo.setTrno(toolInfo.getTrno());
            traceLoanToolInfo.setWarehouse(toolInfo.getWarehouse());
            traceLoanToolInfo.setParentTrno(toolInfo.getParentTrno());
            traceLoanToolInfo.setRfidId(toolInfo.getRfidId());
            traceLoanToolInfo.setUnitofmeas(toolInfo.getUnitofmeas());
            traceLoanToolInfo.setSearchKeyTl(toolInfo.getSearchKeyTl());
            traceLoanToolInfo.setMeasuringNo(toolInfo.getMeasuringNo());
            traceLoanToolInfo.setRid(toolInfo.getRid());
            traceLoanToolInfo.setCatalogueType(toolInfo.getCatalogueType());
            traceLoanToolInfo.setPartdescr(toolInfo.getPartdescr());
            traceLoanToolInfo.setStandByPn(toolInfo.getStandByPn());
            traceLoanToolInfo.setStoresloc(toolInfo.getStoresloc());
            traceLoanToolInfo.setParentPartno(toolInfo.getParentPartno());
            traceLoanToolInfo.setChildNum(Integer.parseInt(toolInfo.getChildNum()));
            traceLoanToolInfo.setTlFlag(toolInfo.getTlFlag());
            traceLoanToolInfo.setImgFlag(toolInfo.getImgFlag());
            traceLoanToolInfo.setTlStatus(toolInfo.getTlStatus());
            traceLoanToolInfo.setOnhand(toolInfo.getOnhand());
            return traceLoanToolInfo;
        }).collect(Collectors.toList());
        commonResp.setDataList(traceLoanToolInfoList);
        return commonResp;
    }
}
