package com.eaero.service.loanTool.pojo;

/**

 */

public class TraceToolInfo {
    private String whseDesc;
    private String issueRemind;
    private String partno;
    private String barcode;
    private String trno;
    private String warehouse;
    private String parentTrno;
    private String rfidId;
    private String unitofmeas;
    private String searchKeyTl;
    private String measuringNo;
    private String rid;
    private String catalogueType;
    private String partdescr;
    private String standByPn;
    private String storesloc;
    private String parentPartno;
    private int childNum;
    /**
     * 故障状态（空值/故障信息）
     */
    private String tlFlag;
    private String imgFlag;

    /**
     *  工具状态（借出/在库）
     */
    private String tlStatus;
    //库存
    private String onhand;

    public String getWhseDesc() {
        return whseDesc;
    }

    public void setWhseDesc(String whseDesc) {
        this.whseDesc = whseDesc;
    }

    public String getIssueRemind() {
        return issueRemind;
    }

    public void setIssueRemind(String issueRemind) {
        this.issueRemind = issueRemind;
    }

    public String getPartno() {
        return partno;
    }

    public void setPartno(String partno) {
        this.partno = partno;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getTrno() {
        return trno;
    }

    public void setTrno(String trno) {
        this.trno = trno;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public String getParentTrno() {
        return parentTrno;
    }

    public void setParentTrno(String parentTrno) {
        this.parentTrno = parentTrno;
    }

    public String getRfidId() {
        return rfidId;
    }

    public void setRfidId(String rfidId) {
        this.rfidId = rfidId;
    }

    public String getUnitofmeas() {
        return unitofmeas;
    }

    public void setUnitofmeas(String unitofmeas) {
        this.unitofmeas = unitofmeas;
    }

    public String getSearchKeyTl() {
        return searchKeyTl;
    }

    public void setSearchKeyTl(String searchKeyTl) {
        this.searchKeyTl = searchKeyTl;
    }

    public String getMeasuringNo() {
        return measuringNo;
    }

    public void setMeasuringNo(String measuringNo) {
        this.measuringNo = measuringNo;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getCatalogueType() {
        return catalogueType;
    }

    public void setCatalogueType(String catalogueType) {
        this.catalogueType = catalogueType;
    }

    public String getPartdescr() {
        return partdescr;
    }

    public void setPartdescr(String partdescr) {
        this.partdescr = partdescr;
    }

    public String getStandByPn() {
        return standByPn;
    }

    public void setStandByPn(String standByPn) {
        this.standByPn = standByPn;
    }

    public String getStoresloc() {
        return storesloc;
    }

    public void setStoresloc(String storesloc) {
        this.storesloc = storesloc;
    }

    public String getParentPartno() {
        return parentPartno;
    }

    public void setParentPartno(String parentPartno) {
        this.parentPartno = parentPartno;
    }

    public int getChildNum() {
        return childNum;
    }

    public void setChildNum(int childNum) {
        this.childNum = childNum;
    }

    public String getTlFlag() {
        return tlFlag;
    }

    public void setTlFlag(String tlFlag) {
        this.tlFlag = tlFlag;
    }

    public String getImgFlag() {
        return imgFlag;
    }

    public void setImgFlag(String imgFlag) {
        this.imgFlag = imgFlag;
    }

    public String getTlStatus() {
        return tlStatus;
    }

    public void setTlStatus(String tlStatus) {
        this.tlStatus = tlStatus;
    }

    public String getOnhand() {
        return onhand;
    }

    public void setOnhand(String onhand) {
        this.onhand = onhand;
    }
}
