package com.eaero.service.loanTool.pojo;

/**
 * *  "parent": 20,
 *  *  "issueDate": "2022-05-12 15:28:35.0",
 *  *   "issueRid": 70011, 借出 RID
 *  *    "acno": "",飞机号
 *  *     "issueTo": "007632",借用人
 *  *     "jcWo": "",工卡指令号
 *  *      "empCode": "001234",
 *  *      "opRemark": "001234"
 * @author zb
 * @date 2023/03/03 003 9:55
 */
public class TraceLoanToolInfo extends TraceToolInfo {
    /**
     * 父件号 ID
     */
    private String parent;
    /**
     * 借出时间
     */
    private String issueDate;
    /**
     * 借出 RID
     */
    private String issueRid;
    /**
     * 飞机号
     */
    private String acno;
    /**
     * 借用人
     */
    private String issueTo;
    /**
     * 工卡指令号
     */
    private String jcWo;
    /**
     * 库房操作人
     */
    private String empCode;
    /**
     * 操作备注
     */
    private String opRemark;

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getIssueRid() {
        return issueRid;
    }

    public void setIssueRid(String issueRid) {
        this.issueRid = issueRid;
    }

    public String getAcno() {
        return acno;
    }

    public void setAcno(String acno) {
        this.acno = acno;
    }

    public String getIssueTo() {
        return issueTo;
    }

    public void setIssueTo(String issueTo) {
        this.issueTo = issueTo;
    }

    public String getJcWo() {
        return jcWo;
    }

    public void setJcWo(String jcWo) {
        this.jcWo = jcWo;
    }

    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }

    public String getOpRemark() {
        return opRemark;
    }

    public void setOpRemark(String opRemark) {
        this.opRemark = opRemark;
    }
}
