package com.eaero.service.loanTool;

import com.eaero.CommonResp ;

/**
 * @author zb
 * @date 2023/03/03 003 14:45
 */
public interface LoanToolService {
    CommonResp getLoanToolList();
}
