package com.eaero.service.pick;

import com.eaero.service.pick.pojo.PickDetail;
import com.eaero.service.pick.pojo.PickVo;

import java.util.List;

/**
 * @author luofeng
 * @create 2023/2/24 10:37
 */
public interface PickService {

    /**
     * 查询拣料单据
     * @return
     */
    List<PickVo> getToolPick();

    /**
     * 查询拣料单明细
     * @return
     */
    List<PickDetail> getPickDetail();
}
