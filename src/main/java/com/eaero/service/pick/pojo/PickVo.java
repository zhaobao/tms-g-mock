package com.eaero.service.pick.pojo;

import lombok.Getter;
import lombok.Setter;

/**
 * @author luofeng
 * @create 2023/2/24 10:39
 */
@Getter
@Setter
public class PickVo {

    private String tpNo;
    private String createdUser;
    private String createdDate;
    private String status;
    private String updatedUser;
    private String updatedDate;
    private String reqNo;
    private String reqUser;
    private String tailNo;
    private String visitDesc;

}
