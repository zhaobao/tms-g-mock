package com.eaero.service.pick.pojo;

import lombok.Getter;
import lombok.Setter;

/**
 * @author luofeng
 * @create 2023/2/24 10:47
 */
@Setter
@Getter
public class PickDetail {

    private String description;
    private String warehouse;
    private String partdescr;
    private String opSeq;
    private String alternatePartno;
    private String updatedUser;
    private String createdUser;
    private String measuringNo;
    private String createdDate;
    private String updatedDate;
    private String reqNo;
    private String visitDesc;
    private String tailNo;
    private String rcvBy;
    private String workorder;
    private String jcSeq;
    private String tpdId;
    private String parentId;
    private String seriano;
    private String tpdlId;
    private String tpId;
    private String sotrsloc;
    private String partno;
    private String unitofmeas;
    private String onhand;
    private String issueNum;
    private String qty;
    private String issueRid;
    private String tpNo;
    private String detailPartno;
    private String tdRid;
    private String tpdlRemark;

}
