package com.eaero.service.pick;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.eaero.service.pick.pojo.PickDetail;
import com.eaero.service.pick.pojo.PickVo;
import com.eaero.service.tool.ToolService;
import com.eaero.service.tool.pojo.ToolInfo;
import com.eaero.service.user.UserInfoService;
import com.eaero.service.user.pojo.UserInfo;
import com.eaero.service.workorder.pojo.RequisitionWorkDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author luofeng
 * @create 2023/2/24 10:37
 */
@Service
public class PickServiceImpl implements PickService{

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private ToolService toolService;

   /* @Autowired
    private WorkOrderService workOrderService;*/


    @Override
    public List<PickVo> getToolPick() {
        List<UserInfo> userInfos = userInfoService.getUserInfoAll();
        UserInfo first = CollUtil.getFirst(userInfos);
        UserInfo last = CollUtil.getLast(userInfos);
        List<PickVo> dataList = new ArrayList<>();
        for (int x = 1; x <= 20; x++) {

            PickVo data = new PickVo();
            // 一定不同
            data.setTpNo("86735443453" + x);

            // 使用创建用户查询
            data.setCreatedUser(first.getUserName());
            data.setCreatedDate("2023-01-06");
            if (x <= 10) {
                data.setStatus("N");
                data.setReqNo("XA77420785");
            } else {
                data.setStatus(RandomUtil.randomEle(CollUtil.newArrayList("N", "D", "U")));
                data.setReqNo(RandomUtil.randomEle(CollUtil.newArrayList("XA08814481", "XA79252406", "XA06368482")));
            }
            // 使用更新用户查询
            data.setUpdatedUser(last.getUserName());
            data.setUpdatedDate("2023-01-06 10:14");
            // 借出人加手机号
            data.setReqUser(StrUtil.format("{} {}", first.getUserName(), first.getComMobile()));
            data.setTailNo("B-1120");
            data.setVisitDesc("TR61");

            dataList.add(data);
        }
        return dataList;
    }

    @Override
    public List<PickDetail> getPickDetail() {
        return null;
    }

   /* @Override
    public List<PickDetail> getPickDetail() {
        List<RequisitionWorkDetail> requisitionWorkDetails = null;
        try {
            requisitionWorkDetails = workOrderService.getWorkOrderDetailByWorkOrderCode("XA77420785");
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<UserInfo> userInfos = userInfoService.getUserInfoAll();
        UserInfo first = CollUtil.getFirst(userInfos);
        UserInfo last = CollUtil.getLast(userInfos);

        List<ToolInfo> allTool = toolService.getAllTool();
        List<ToolInfo> page = CollUtil.page(0, 6, allTool);

        // 流水号
        String opSeqPrefix = "267489311235";
        // 拣料单
        String tpId = "410505646386";

        // 拣料明细id
        String tpdIdPrefix = "857091648768";
        // 真实件号
        String alternatePartnoPrefix = "941872555881";

        String issueRidPrefix = "474173359258";

        String tdRIdPrefix = "180963285606";

        List<PickDetail> pickDetails = new ArrayList<>();

        for (int x = 0; x < requisitionWorkDetails.size(); x++) {
            RequisitionWorkDetail requisitionWorkDetail = requisitionWorkDetails.get(x);
            ToolInfo toolInfo = page.get(x);

            PickDetail pickDetail = new PickDetail();
            // 仓库描述
            pickDetail.setDescription("机场西北仓库");
            // 仓库
            pickDetail.setWarehouse(toolInfo.getWarehouse());
            // 件号描述
            pickDetail.setPartdescr(toolInfo.getPartdescr());
            pickDetail.setOpSeq(opSeqPrefix + x);
            pickDetail.setAlternatePartno(alternatePartnoPrefix + x);
            pickDetail.setUpdatedUser(last.getUserName());
            pickDetail.setCreatedUser(first.getUserName());
            // 计量号
            pickDetail.setMeasuringNo(toolInfo.getMeasuringNo());
            pickDetail.setCreatedDate("2023-01-06 11:01:14.0");
            pickDetail.setUpdatedDate("2023-01-07 12:24:28.0");
            // 申请单号
            pickDetail.setReqNo(requisitionWorkDetail.getReqNo());
            pickDetail.setVisitDesc("STRC9");
            // 飞机号
            pickDetail.setTailNo(requisitionWorkDetail.getTailNo());
            // 申请工单中的申请人
            pickDetail.setRcvBy(requisitionWorkDetail.getSndBy());
            // 工卡指令号
            pickDetail.setWorkorder(requisitionWorkDetail.getJcWorkorder());
            // 工卡序号
            pickDetail.setJcSeq(requisitionWorkDetail.getJcSeq());
            pickDetail.setTpdId(tpdIdPrefix + x);
            pickDetail.setParentId("1");
            pickDetail.setSeriano("10000" + x);
            // ★建议批号问题
            pickDetail.setTpdlId(RandomUtil.randomEle(CollUtil.newArrayList("3843702", "3843703", "3843704", "3843705")));
            pickDetail.setTpId(tpId);
            // 定位
            pickDetail.setSotrsloc(toolInfo.getWarehouse());
            // 件号
            pickDetail.setPartno(requisitionWorkDetail.getPn());
            // 单位
            pickDetail.setUnitofmeas(requisitionWorkDetail.getUnitofmeas());
            pickDetail.setOnhand("500");
            pickDetail.setIssueNum("20");
            // 数量
            pickDetail.setQty(requisitionWorkDetail.getQty());
            pickDetail.setIssueRid(issueRidPrefix + x);
            pickDetail.setTpNo("867354434531");
            pickDetail.setDetailPartno(RandomUtil.randomNumbers(4));
            pickDetail.setTdRid(tdRIdPrefix + x);
            pickDetail.setTpdlRemark("测试测试测试");

            pickDetails.add(pickDetail);

            if (x % 2 == 0) {
                PickDetail pnPickDetail = new PickDetail();
                BeanUtil.copyProperties(pickDetail, pnPickDetail);
                pnPickDetail.setTpdlId(RandomUtil.randomEle(CollUtil.newArrayList("3843702", "3843703", "3843704", "3843705")));
                pickDetails.add(pnPickDetail);
            }
        }
        return pickDetails;
    }*/
}
