package com.eaero.service.handover;

import com.eaero.entity.TraceWarehouse;
import com.eaero.service.handover.pojo.Shift;
import com.eaero.service.handover.pojo.ShiftOrder;
import com.eaero.service.user.UserInfoService;
import com.eaero.service.user.pojo.UserInfo;
import com.eaero.service.warehouse.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Description:
 *
 * @author Ye Zhihong
 * @date 2023/2/24 14:55
 */
@Service
public class WarehouseHandoverServiceImpl implements WarehouseHandoverService {

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private WarehouseService warehouseService;

    @Override
    public List<Shift> getHandoverInfoAll() {
        List<Shift> data = new ArrayList<>();
        String[] dates = {"2023-01-06 10:14", "2023-01-06 10:15", "2023-01-06 10:16"};
        String[] shifts = {"早班", "晚班"};
        String[] status = {"交班", "接班", "作废", "关闭"};
        List<TraceWarehouse> warehouse = warehouseService.getWarehouse(null);
        List<UserInfo> userInfoAll = userInfoService.getUserInfoAll();

        for (int i = 0; i < 10; i++) {
            Shift shift = new Shift();
            shift.setHoRemark("这是一个备注");
            shift.setToRemark("这是一个备注");
            shift.setStatus(status[i % status.length]);
            shift.setStation("CAN");
            shift.setTsId("60" + i);
            shift.setHoDate(dates[i % dates.length]);
            shift.setWarehouse(warehouse.get(i).getWarehouse());
            shift.setCreatedDate("2023-01-06 10:00");
            shift.setShift(shifts[i % shifts.length]);
            shift.setCreatedUser(userInfoAll.get(i % userInfoAll.size()).getUserName());
            shift.setToUser("李四");
            shift.setHoUser(userInfoAll.get(i % userInfoAll.size()).getUserName());
            data.add(shift);
        }
        return data;
    }

    /**
     * 获取交接班单信息
     *
     * @return
     */
    @Override
    public List<ShiftOrder> getHandoverDtlInfoAll() {
        List<ShiftOrder> data = new ArrayList<>();
        List<Shift> handoverInfoAll = getHandoverInfoAll();
        for (int i = 0; i < 10; i++) {
            ShiftOrder shiftOrder = new ShiftOrder();
            shiftOrder.setCheckReq("该定位上的全部工具");
            shiftOrder.setHoRemark(handoverInfoAll.get(i).getHoRemark());
            shiftOrder.setWhseIssue(i);
            shiftOrder.setToRemark(handoverInfoAll.get(i).getToRemark());
            shiftOrder.setRemark("接班明细备注");
            shiftOrder.setTsdId("600" + i);
            shiftOrder.setPartno("113A7183-5" + i);
            shiftOrder.setWhseIn(i);
            shiftOrder.setTrno("A88889" + i);
            shiftOrder.setStoresloc(warehouseService.getWarehouseLocationByWarehouse(handoverInfoAll.get(i).getWarehouse()).get(0).getStoresloc());
            shiftOrder.setWhseCheck(i);
            shiftOrder.setWarehouse(handoverInfoAll.get(i).getWarehouse());
            data.add(shiftOrder);
        }

        return data;
    }
}
