package com.eaero.service.handover;

import com.eaero.service.handover.pojo.Shift;
import com.eaero.service.handover.pojo.ShiftOrder;

import java.util.List;

/**
 * Description:
 *
 * @author Ye Zhihong
 * @date 2023/2/24 14:54
 */
public interface WarehouseHandoverService {
    List<Shift> getHandoverInfoAll();

    List<ShiftOrder> getHandoverDtlInfoAll();
}
