package com.eaero.service.handover.pojo;

import lombok.Data;

/**
 * Description:
 *
 * @author Ye Zhihong
 * @date 2023/2/24 13:45
 */
@Data
public class Shift {
    private String tsId;
    private String createdDate;
    private String createdUser;
    private String station;
    private String shift;
    private String warehouse;
    private String hoRemark;
    private String toRemark;
    private String status;
    private String hoDate;
    private String hoUser;
    private String toUser;
}
