package com.eaero.service.handover.pojo;

import lombok.Data;

/**
 * Description:
 *
 * @author Ye Zhihong
 * @date 2023/2/24 14:38
 */
@Data
public class ShiftOrder {
    private String tsdId;
    private String warehouse;
    private String storesloc;
    private String checkReq;
    private int whseIn;
    private int whseIssue;
    private int whseCheck;
    private String remark;
    private String hoRemark;
    private String toRemark;
    private String partno;
    private String trno;
}
