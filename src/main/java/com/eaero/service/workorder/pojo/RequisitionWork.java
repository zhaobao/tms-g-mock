package com.eaero.service.workorder.pojo;

/**
 * @description: 工具申请单
 * @author: WBL
 * @date: 2023-02-23
 * @time: 13:24
 */
public class RequisitionWork {
      private String reqNo;
      private String crtDate;
      private String reqBy;
      private String uptDate;
      private String uptBy;
      private String tailNo;
      private String visitDesc;
      private String reqSts;
      private String remark;
      private String origin;

    public String getReqNo() {
        return reqNo;
    }

    public void setReqNo(String reqNo) {
        this.reqNo = reqNo;
    }

    public String getCrtDate() {
        return crtDate;
    }

    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate;
    }

    public String getReqBy() {
        return reqBy;
    }

    public void setReqBy(String reqBy) {
        this.reqBy = reqBy;
    }

    public String getUptDate() {
        return uptDate;
    }

    public void setUptDate(String uptDate) {
        this.uptDate = uptDate;
    }

    public String getUptBy() {
        return uptBy;
    }

    public void setUptBy(String uptBy) {
        this.uptBy = uptBy;
    }

    public String getTailNo() {
        return tailNo;
    }

    public void setTailNo(String tailNo) {
        this.tailNo = tailNo;
    }

    public String getVisitDesc() {
        return visitDesc;
    }

    public void setVisitDesc(String visitDesc) {
        this.visitDesc = visitDesc;
    }

    public String getReqSts() {
        return reqSts;
    }

    public void setReqSts(String reqSts) {
        this.reqSts = reqSts;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    @Override
    public String toString() {
        return "RequisitionWork{" +
                "reqNo='" + reqNo + '\'' +
                ", crtDate='" + crtDate + '\'' +
                ", reqBy='" + reqBy + '\'' +
                ", uptDate='" + uptDate + '\'' +
                ", uptBy='" + uptBy + '\'' +
                ", tailNo='" + tailNo + '\'' +
                ", visitDesc='" + visitDesc + '\'' +
                ", reqSts='" + reqSts + '\'' +
                ", remark='" + remark + '\'' +
                ", origin='" + origin + '\'' +
                '}';
    }
}
