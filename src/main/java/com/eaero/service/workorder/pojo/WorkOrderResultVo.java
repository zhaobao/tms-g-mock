package com.eaero.service.workorder.pojo;


/**
 * @description:
 * @author: WBL
 * @date: 2023-03-14
 * @time: 16:57
 */
public class WorkOrderResultVo {

    /**
     * 申请单表
     */
    private String id;

    /**
     * 申请单号
     */
    private String reqNo;

    /**
     * 机尾号
     */
    private String tailNo;

    /**
     * 定检级别
     */
    private String visitDesc;

    /**
     * 申请状态（N 草稿/R 提交申请/D 作废/P 拣料中/U 关闭）
     */
    private String reqSts;

    /**
     * 申请来源（P:PC;A:ATM;H:手持设备;D:工作台）
     */
    private String origin;

    /**
     * 申请单备注
     */
    private String remark;

    /**
     * 工具申请提交人
     */
    private String reqBy;

    /**
     * 创建时间
     */
    private String crtDate;

    /**
     * 申请明细更新人
     */
    private String uptBy;

    /**
     * 申请明细更新时间
     */
    private String uptDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReqNo() {
        return reqNo;
    }

    public void setReqNo(String reqNo) {
        this.reqNo = reqNo;
    }

    public String getTailNo() {
        return tailNo;
    }

    public void setTailNo(String tailNo) {
        this.tailNo = tailNo;
    }

    public String getVisitDesc() {
        return visitDesc;
    }

    public void setVisitDesc(String visitDesc) {
        this.visitDesc = visitDesc;
    }

    public String getReqSts() {
        return reqSts;
    }

    public void setReqSts(String reqSts) {
        this.reqSts = reqSts;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getReqBy() {
        return reqBy;
    }

    public void setReqBy(String reqBy) {
        this.reqBy = reqBy;
    }

    public String getCrtDate() {
        return crtDate;
    }

    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate;
    }

    public String getUptBy() {
        return uptBy;
    }

    public void setUptBy(String uptBy) {
        this.uptBy = uptBy;
    }

    public String getUptDate() {
        return uptDate;
    }

    public void setUptDate(String uptDate) {
        this.uptDate = uptDate;
    }
}
