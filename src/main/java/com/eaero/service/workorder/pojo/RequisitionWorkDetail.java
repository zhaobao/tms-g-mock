package com.eaero.service.workorder.pojo;

/**
 * @description: 工具申请单明细
 * @author: WBL
 * @date: 2023-02-23
 * @time: 15:23
 */
public class RequisitionWorkDetail {
    private String reqNo;
    private String rid;
    private String pn;
    private String reqPn;
    private String uptDate;
    private String uptBy;
    private String qty;
    private String unitofmeas;
    private String jcSeq;
    private String jcWorkorder;
    private String dtlSts;
    private String msg;
    private String msgUser;
    private String msgDate;
    private String whsType;
    private String remark;
    private String tailNo;
    private String partdescr;
    private String rcvBy;
    private String reqDate;
    private String sndDate;
    private String sndBy;
    private String onhandInfo;

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getReqNo() {
        return reqNo;
    }

    public void setReqNo(String reqNo) {
        this.reqNo = reqNo;
    }

    public String getPn() {
        return pn;
    }

    public void setPn(String pn) {
        this.pn = pn;
    }

    public String getReqPn() {
        return reqPn;
    }

    public void setReqPn(String reqPn) {
        this.reqPn = reqPn;
    }

    public String getUptDate() {
        return uptDate;
    }

    public void setUptDate(String uptDate) {
        this.uptDate = uptDate;
    }

    public String getUptBy() {
        return uptBy;
    }

    public void setUptBy(String uptBy) {
        this.uptBy = uptBy;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getUnitofmeas() {
        return unitofmeas;
    }

    public void setUnitofmeas(String unitofmeas) {
        this.unitofmeas = unitofmeas;
    }

    public String getJcSeq() {
        return jcSeq;
    }

    public void setJcSeq(String jcSeq) {
        this.jcSeq = jcSeq;
    }

    public String getJcWorkorder() {
        return jcWorkorder;
    }

    public void setJcWorkorder(String jcWorkorder) {
        this.jcWorkorder = jcWorkorder;
    }

    public String getDtlSts() {
        return dtlSts;
    }

    public void setDtlSts(String dtlSts) {
        this.dtlSts = dtlSts;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsgUser() {
        return msgUser;
    }

    public void setMsgUser(String msgUser) {
        this.msgUser = msgUser;
    }

    public String getMsgDate() {
        return msgDate;
    }

    public void setMsgDate(String msgDate) {
        this.msgDate = msgDate;
    }

    public String getWhsType() {
        return whsType;
    }

    public void setWhsType(String whsType) {
        this.whsType = whsType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTailNo() {
        return tailNo;
    }

    public void setTailNo(String tailNo) {
        this.tailNo = tailNo;
    }

    public String getPartdescr() {
        return partdescr;
    }

    public void setPartdescr(String partdescr) {
        this.partdescr = partdescr;
    }

    public String getRcvBy() {
        return rcvBy;
    }

    public void setRcvBy(String rcvBy) {
        this.rcvBy = rcvBy;
    }

    public String getReqDate() {
        return reqDate;
    }

    public void setReqDate(String reqDate) {
        this.reqDate = reqDate;
    }

    public String getSndDate() {
        return sndDate;
    }

    public void setSndDate(String sndDate) {
        this.sndDate = sndDate;
    }

    public String getSndBy() {
        return sndBy;
    }

    public void setSndBy(String sndBy) {
        this.sndBy = sndBy;
    }

    public String getOnhandInfo() {
        return onhandInfo;
    }

    public void setOnhandInfo(String onhandInfo) {
        this.onhandInfo = onhandInfo;
    }

    @Override
    public String toString() {
        return "RequisitionWorkDetail{" +
                "reqNo='" + reqNo + '\'' +
                ", rid='" + rid + '\'' +
                ", pn='" + pn + '\'' +
                ", reqPn='" + reqPn + '\'' +
                ", uptDate='" + uptDate + '\'' +
                ", uptBy='" + uptBy + '\'' +
                ", qty='" + qty + '\'' +
                ", unitofmeas='" + unitofmeas + '\'' +
                ", jcSeq='" + jcSeq + '\'' +
                ", jcWorkorder='" + jcWorkorder + '\'' +
                ", dtlSts='" + dtlSts + '\'' +
                ", msg='" + msg + '\'' +
                ", msgUser='" + msgUser + '\'' +
                ", msgDate='" + msgDate + '\'' +
                ", whsType='" + whsType + '\'' +
                ", remark='" + remark + '\'' +
                ", tailNo='" + tailNo + '\'' +
                ", partdescr='" + partdescr + '\'' +
                ", rcvBy='" + rcvBy + '\'' +
                ", reqDate='" + reqDate + '\'' +
                ", sndDate='" + sndDate + '\'' +
                ", sndBy='" + sndBy + '\'' +
                ", onhandInfo='" + onhandInfo + '\'' +
                '}';
    }
}
