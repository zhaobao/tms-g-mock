package com.eaero.service.workorder.pojo;

/**
 * @description: 工具申请单明细汇总
 * @author: WBL
 * @date: 2023-02-23
 * @time: 16:58
 */
public class RequisitionWorkDetailCollect {

    private String tailNo;
    private String rcvBy;
    private String reqDate;
    private String whsType;
    private String reqNum;
    private String clsNum;
    private String unNum;
    private String issueNum;

    public String getTailNo() {
        return tailNo;
    }

    public void setTailNo(String tailNo) {
        this.tailNo = tailNo;
    }

    public String getRcvBy() {
        return rcvBy;
    }

    public void setRcvBy(String rcvBy) {
        this.rcvBy = rcvBy;
    }

    public String getReqDate() {
        return reqDate;
    }

    public void setReqDate(String reqDate) {
        this.reqDate = reqDate;
    }

    public String getWhsType() {
        return whsType;
    }

    public void setWhsType(String whsType) {
        this.whsType = whsType;
    }

    public String getReqNum() {
        return reqNum;
    }

    public void setReqNum(String reqNum) {
        this.reqNum = reqNum;
    }

    public String getClsNum() {
        return clsNum;
    }

    public void setClsNum(String clsNum) {
        this.clsNum = clsNum;
    }

    public String getUnNum() {
        return unNum;
    }

    public void setUnNum(String unNum) {
        this.unNum = unNum;
    }

    public String getIssueNum() {
        return issueNum;
    }

    public void setIssueNum(String issueNum) {
        this.issueNum = issueNum;
    }

    @Override
    public String toString() {
        return "RequisitionWorkDetailCollect{" +
                "tailNo='" + tailNo + '\'' +
                ", rcvBy='" + rcvBy + '\'' +
                ", reqDate='" + reqDate + '\'' +
                ", whsType='" + whsType + '\'' +
                ", reqNum='" + reqNum + '\'' +
                ", clsNum='" + clsNum + '\'' +
                ", unNum='" + unNum + '\'' +
                ", issueNum='" + issueNum + '\'' +
                '}';
    }
}
