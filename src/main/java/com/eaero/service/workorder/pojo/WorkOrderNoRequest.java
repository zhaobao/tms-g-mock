package com.eaero.service.workorder.pojo;


/**
 * @description:
 * @author: WBL
 * @date: 2023-03-14
 * @time: 16:29
 */
public class WorkOrderNoRequest {

    private String reqNo;

    public String getReqNo() {
        return reqNo;
    }

    public void setReqNo(String reqNo) {
        this.reqNo = reqNo;
    }
}
