package com.eaero.service.workorder.pojo;


import java.util.List;

/**
 * @description:
 * @author: WBL
 * @date: 2023-03-14
 * @time: 16:28
 */
public class CancelWorkOrderRequest {
    private String userCode;
    private List<WorkOrderNoRequest> cancelReq;

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public List<WorkOrderNoRequest> getCancelReq() {
        return cancelReq;
    }

    public void setCancelReq(List<WorkOrderNoRequest> cancelReq) {
        this.cancelReq = cancelReq;
    }
}
