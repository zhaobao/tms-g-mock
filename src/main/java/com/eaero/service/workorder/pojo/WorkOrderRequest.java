package com.eaero.service.workorder.pojo;



/**
 * @description:
 * @author: WBL
 * @date: 2023-03-14
 * @time: 13:12
 */
public class WorkOrderRequest {

    private String reqNo;
    private String reqBy;
    private String crtDate;
    private String reqSts;
    private String sndBy;
    private String sndDate;
    private String origin;

    public String getReqNo() {
        return reqNo;
    }

    public void setReqNo(String reqNo) {
        this.reqNo = reqNo;
    }

    public String getReqBy() {
        return reqBy;
    }

    public void setReqBy(String reqBy) {
        this.reqBy = reqBy;
    }

    public String getCrtDate() {
        return crtDate;
    }

    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate;
    }

    public String getReqSts() {
        return reqSts;
    }

    public void setReqSts(String reqSts) {
        this.reqSts = reqSts;
    }

    public String getSndBy() {
        return sndBy;
    }

    public void setSndBy(String sndBy) {
        this.sndBy = sndBy;
    }

    public String getSndDate() {
        return sndDate;
    }

    public void setSndDate(String sndDate) {
        this.sndDate = sndDate;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }
}
