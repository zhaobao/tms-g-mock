package com.eaero.service.workorder.pojo;

/**
 * @description:
 * @author: WBL
 * @date: 2023-03-14
 * @time: 17:28
 */
public class WorkOrderDetailRequset {

    private String reqNo;
    private String tailNo;
    private String jcWorkorder;
    private String visitDesc;
    private String dtlSts;
    private String pn;
    private String reqDate;
    private String rcvBy;
    private String whsType;
    private String msg;
    private String sndDate;

    public String getReqNo() {
        return reqNo;
    }

    public void setReqNo(String reqNo) {
        this.reqNo = reqNo;
    }

    public String getTailNo() {
        return tailNo;
    }

    public void setTailNo(String tailNo) {
        this.tailNo = tailNo;
    }

    public String getJcWorkorder() {
        return jcWorkorder;
    }

    public void setJcWorkorder(String jcWorkorder) {
        this.jcWorkorder = jcWorkorder;
    }

    public String getVisitDesc() {
        return visitDesc;
    }

    public void setVisitDesc(String visitDesc) {
        this.visitDesc = visitDesc;
    }

    public String getDtlSts() {
        return dtlSts;
    }

    public void setDtlSts(String dtlSts) {
        this.dtlSts = dtlSts;
    }

    public String getPn() {
        return pn;
    }

    public void setPn(String pn) {
        this.pn = pn;
    }

    public String getReqDate() {
        return reqDate;
    }

    public void setReqDate(String reqDate) {
        this.reqDate = reqDate;
    }

    public String getRcvBy() {
        return rcvBy;
    }

    public void setRcvBy(String rcvBy) {
        this.rcvBy = rcvBy;
    }

    public String getWhsType() {
        return whsType;
    }

    public void setWhsType(String whsType) {
        this.whsType = whsType;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSndDate() {
        return sndDate;
    }

    public void setSndDate(String sndDate) {
        this.sndDate = sndDate;
    }
}
