package com.eaero.service;

import com.eaero.entity.TraceToolDeliverDetail;
import com.baomidou.mybatisplus.extension.service.IService;
import com.eaero.vo.ToolDeliverDetail;
import com.eaero.vo.ToolDeliverDetailRequest;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
public interface ITraceToolDeliverDetailService extends IService<TraceToolDeliverDetail> {

    /**
     * 根据配送单据的条件（参考 Trace)查询单据明细
     * @param request
     * @return
     */
    List<ToolDeliverDetail> getToolDeliverDetail(ToolDeliverDetailRequest request);

}
