package com.eaero.service;

import com.eaero.CommonResp;
import com.eaero.entity.TraceUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.eaero.vo.Login;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
public interface ITraceUserService extends IService<TraceUser> {

    /**
     * 查询用户
     * @param userCode
     * @return
     */
    CommonResp getUserInfoAll(String userCode);

    /**
     * 考勤卡认证
     * @param cardId
     * @return
     */
    CommonResp loginByCard(String cardId);

    /**
     * AD认证接口
     */
    CommonResp login(Login login);
}
