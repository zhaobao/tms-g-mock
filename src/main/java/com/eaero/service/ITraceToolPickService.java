package com.eaero.service;

import com.eaero.CommonResp;
import com.eaero.entity.TraceToolPick;
import com.baomidou.mybatisplus.extension.service.IService;
import com.eaero.vo.*;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
public interface ITraceToolPickService extends IService<TraceToolPick> {

    CommonResp getToolPickHD(String tpNo, String createdUser, String createdDate, String status, String updatedUsers);

    CommonResp cancelToolPickHD(CancelToolPickRequest request);

    CommonResp getToolPickdetail(String tpId, String tpNo);

    CommonResp deleteToolPickDetail(DeleteToolPickDetailRequest request);

    CommonResp updateToolPickdetailPn(UpdateToolPickdetailPnRequest request);

    CommonResp updateToolPickdetailList(UpdateToolPickdetailListRequest request);

    /**
     * 提交一个或多个申请单明细，提交对应的仓库，生成对应仓库下的拣料单据
     * @param request
     * @return
     */
    CommonResp goPickTools(PickToolsRequest request);

    CommonResp deleteToolPickDetailList(DeleteToolPickDetailListRequest request);
}
