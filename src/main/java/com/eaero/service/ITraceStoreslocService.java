package com.eaero.service;

import com.eaero.entity.TraceStoresloc;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
public interface ITraceStoreslocService extends IService<TraceStoresloc> {

    /**
     * 根据仓库获取其下定位信息
     * @param warehouse
     * @param storesloc
     * @return
     */
    List<TraceStoresloc> getStoresloc(String warehouse, String storesloc);

}
