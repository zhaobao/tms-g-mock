package com.eaero.service;

import com.eaero.entity.TraceToolStockDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Ye Zhihong
* @description 针对表【trace_tool_stock_detail】的数据库操作Service
* @createDate 2023-03-17 16:26:35
*/
public interface ITraceToolStockDetailService extends IService<TraceToolStockDetail> {

}
