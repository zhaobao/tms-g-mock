package com.eaero.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.eaero.CommonResp;
import com.eaero.entity.TraceHandover;
import com.eaero.vo.CancelHandoverRequest;
import com.eaero.vo.HandingOfRequest;
import com.eaero.vo.HandoverInfoRequest;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
public interface ITraceHandoverService extends IService<TraceHandover> {

    /**
     * 生成交班信息
     *
     * @param request
     * @return
     */
    CommonResp handingOf(HandingOfRequest request);

    /**
     * 查询交接班单
     *
     * @param request
     * @return
     */
    CommonResp getHandoverInfo(HandoverInfoRequest request);

    /**
     * 作废交接班单
     *
     * @param request
     * @return
     */
    CommonResp cancelHandoverInfo(CancelHandoverRequest request);
}
