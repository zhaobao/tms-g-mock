package com.eaero.service.tool.pojo;

import lombok.Data;

@Data
public class ToolInfo {

    private String rid;
    private String whseDesc;
    private String issueRemind;
    private String partno;
    private String barcode;
    private String trno;

    private String warehouse;
    private String parentTrno;
    private String rfidId;
    private String unitofmeas;
    private String searchKeyTl;
    private String measuringNo;

    private String catalogueType;
    private String partdescr;
    private String standByPn;
    private String storesloc;
    private String parentPartno;
    private String childNum;
    private String tlFlag;
    private String imgFlag;

    //工具状态（借出/在库）
    private String tlStatus;
    //库存
    private String onhand;
}
