package com.eaero.service.tool.pojo;

import lombok.Data;

/**
 * @author zzj
 * @date 2023/2/24 15:16
 * @description
 */
@Data
public class InventoryInfo {

    private String whseDesc;
    private String issueRemind;
    private String partno;
    private String tlStatus;
    private String barcode;
    private String trno;
    private String warehouse;
    private String parentTrno;
    private String rfidId;
    private String unitofmeas;
    private String searchKeyTl;
    private String measuringNo;
    private String rid;
    private String catalogueType;
    private String partdescr;
    private String standByPn;
    private String storesloc;
    private String parentPartno;
    private String childNum;
    private String onhand;
    private String tlFlag;
    private String imgFlag;

}
