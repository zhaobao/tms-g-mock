package com.eaero.service.tool;

import com.eaero.entity.TraceWarehouse;
import com.eaero.service.tool.pojo.InventoryInfo;
import com.eaero.service.warehouse.WarehouseService;
import com.eaero.service.warehouse.pojo.WarehouseInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zzj
 * @date 2023/2/24 15:18
 * @description
 */
@Service
public class InventoryServiceImpl implements InventoryService {

    @Autowired
    WarehouseService warehouseService;

    @Override
    public List<InventoryInfo> getAllInventory() {
        List<InventoryInfo> inventoryInfos = new ArrayList<>();
        singleTool(inventoryInfos, 0);
        suitTool(inventoryInfos,0);
        return inventoryInfos;
    }

    /**
     * 套装
     *
     * @param toolInfoList
     */
    private void suitTool(List<InventoryInfo> toolInfoList, int warehouseNum) {
        List<InventoryInfo> parentTool = new ArrayList<>();
        List<TraceWarehouse> warehouse = warehouseService.getWarehouse(null);
        for (int i = 1; i <= 10; i++) {
            String storesloc1 = warehouseService.getWarehouseLocationByWarehouse(warehouse.get(warehouseNum).getWarehouse()).get(i%5).getStoresloc();
            String warehouse1 = warehouse.get(warehouseNum).getWarehouse();
            String commentText1 = warehouse.get(warehouseNum).getCommentText();

            InventoryInfo toolInfo = new InventoryInfo();
            toolInfo.setWarehouse(warehouse1);
            toolInfo.setWhseDesc(commentText1);
            toolInfo.setRid("P20230224000" + i);
            toolInfo.setPartno("JHP20230224000" + i);
            toolInfo.setParentPartno("");
            toolInfo.setTrno("KHP20230224000" + i);
            toolInfo.setParentTrno("");
            toolInfo.setPartdescr("父工具" + i);
            toolInfo.setRfidId("RFIDP2023022400" + i);
            toolInfo.setBarcode("barcodeP2023022400" + i);
            //工具状态（借出/在库）
            //toolInfo.setTlStatus(i % 5 == 1 ? "借出" : "在库");
            toolInfo.setIssueRemind(i % 5 == 1 ? "借出提醒" : "");

            toolInfo.setUnitofmeas("EA");
            toolInfo.setSearchKeyTl("");
            toolInfo.setMeasuringNo("计量号" + i);
            //工具类型：SPTL/CMTL
            toolInfo.setCatalogueType(i % 2 == 1 ? "SPTL" : "CMTL");
            toolInfo.setStandByPn("替代件号" + "P2023022400" + i);
            toolInfo.setStoresloc(storesloc1);
            toolInfo.setChildNum("20");
            //toolInfo.setOnhand(i + "");
            //故障状态（空值/故障信息）
            toolInfo.setTlFlag(i % 9 == 1 ? "故障了" : "");
            toolInfo.setImgFlag(i % 5 == 1 ? "N" : "Y");
            parentTool.add(toolInfo);
        }
        List<InventoryInfo> childTool = new ArrayList<>();
        for (InventoryInfo parent : parentTool) {
            for (int i = 1; i <= 20; i++) {
                InventoryInfo toolInfoChild = new InventoryInfo();
                toolInfoChild.setWarehouse(parent.getWarehouse());
                toolInfoChild.setWhseDesc(parent.getWhseDesc());
                toolInfoChild.setRid(parent.getRid()+"_C" + i);
                toolInfoChild.setPartno(parent.getPartno()+"_C" + i);
                toolInfoChild.setParentPartno(parent.getPartno());
                toolInfoChild.setTrno("KHC20230224000" + i);
                toolInfoChild.setParentTrno(parent.getTrno());
                toolInfoChild.setPartdescr("子工具" + i);
                toolInfoChild.setRfidId(parent.getRfidId()+"_C"+i);
                toolInfoChild.setBarcode(parent.getBarcode()+"_C"+i);
                //工具状态（借出/在库）
                //toolInfoChild.setTlStatus(i % 5 == 1 ? "借出" : "在库");
                toolInfoChild.setIssueRemind(i % 5 == 1 ? "借出提醒" : "");

                toolInfoChild.setUnitofmeas("EA");
                toolInfoChild.setSearchKeyTl("");
                toolInfoChild.setMeasuringNo("计量号" + i);
                //工具类型：SPTL/CMTL
                toolInfoChild.setCatalogueType(i % 2 == 1 ? "SPTL" : "CMTL");
                toolInfoChild.setStandByPn("替代件号" + parent.getStandByPn()+"_C" + i);
                toolInfoChild.setStoresloc(parent.getStoresloc());
                toolInfoChild.setChildNum("0");
                //toolInfoChild.setOnhand(i + "");
                //故障状态（空值/故障信息）
                toolInfoChild.setTlFlag(i % 9 == 1 ? "故障了" : "");
                toolInfoChild.setImgFlag(i % 5 == 1 ? "N" : "Y");
                childTool.add(toolInfoChild);
            }
        }
        toolInfoList.addAll(parentTool);
        toolInfoList.addAll(childTool);

    }

    /**
     * 单件
     *
     * @param toolInfoList
     */
    private void singleTool(List<InventoryInfo> toolInfoList, int warehouseNum) {
        List<TraceWarehouse> warehouse = warehouseService.getWarehouse("");

        //单件
        for (int i = 1; i <= 20; i++) {
            String storesloc1 = warehouseService.getWarehouseLocationByWarehouse(warehouse.get(warehouseNum).getWarehouse()).get(i % 5).getStoresloc();
            String warehouse1 = warehouse.get(warehouseNum).getWarehouse();
            String commentText1 = warehouse.get(warehouseNum).getCommentText();

            InventoryInfo toolInfo = new InventoryInfo();
            toolInfo.setWarehouse(warehouse1);
            toolInfo.setWhseDesc(commentText1);
            toolInfo.setRid("2023022400" + i);
            toolInfo.setPartno("JH2023022400" + i);
            toolInfo.setParentPartno("");
            toolInfo.setTrno("KH2023022400" + i);
            toolInfo.setParentTrno("");
            toolInfo.setPartdescr("工具" + i);
            toolInfo.setRfidId("RFID2023022400" + i);
            toolInfo.setBarcode("barcode2023022400" + i);
            //工具状态（借出/在库）
            //toolInfo.setTlStatus(i % 5 == 1 ? "借出" : "在库");
            toolInfo.setIssueRemind(i % 5 == 1 ? "借出提醒" : "");

            toolInfo.setUnitofmeas("EA");
            toolInfo.setSearchKeyTl(i % 9 == 1 ? "关键字" : "");
            toolInfo.setMeasuringNo("计量号" + i);
            //工具类型：SPTL/CMTL
            toolInfo.setCatalogueType(i % 2 == 1 ? "SPTL" : "CMTL");
            toolInfo.setStandByPn("替代件号" + "2023022400" + i);
            toolInfo.setStoresloc(storesloc1);
            toolInfo.setChildNum("0");
            //toolInfo.setOnhand(i + "");
            //故障状态（空值/故障信息）
            toolInfo.setTlFlag(i % 9 == 1 ? "故障了" : "");
            toolInfo.setImgFlag(i % 5 == 1 ? "N" : "Y");
            toolInfoList.add(toolInfo);
        }
    }

    @Override
    public List<InventoryInfo> getInventoryByRid(String rid) {
        List<InventoryInfo> inventoryInfos = new ArrayList<>();
        for (InventoryInfo inventoryInfo : getAllInventory()) {
            if (inventoryInfo.getRid().equals(rid)) {
                inventoryInfos.add(inventoryInfo);
            }
        }
        return inventoryInfos;
    }

    @Override
    public List<InventoryInfo> getInventoryByRfidId(String rfidId) {
        List<InventoryInfo> inventoryInfos = new ArrayList<>();
        for (InventoryInfo inventoryInfo : getAllInventory()) {
            if (inventoryInfo.getRfidId().equals(rfidId)) {
                inventoryInfos.add(inventoryInfo);
            }
        }
        return inventoryInfos;
    }

    @Override
    public List<InventoryInfo> getInventoryByPartnoAndTrno(String partno, String trno) {
        List<InventoryInfo> inventoryInfos = new ArrayList<>();
        for (InventoryInfo inventoryInfo : getAllInventory()) {
            if (inventoryInfo.getPartno().contains(partno) && inventoryInfo.getTrno().contains(trno)) {
                inventoryInfos.add(inventoryInfo);
            }
        }
        return inventoryInfos;
    }

    @Override
    public List<InventoryInfo> getInventoryByPartdescr(String partdescr) {
        List<InventoryInfo> inventoryInfos = new ArrayList<>();
        for (InventoryInfo inventoryInfo : getAllInventory()) {
            if (inventoryInfo.getPartdescr().contains(partdescr)) {
                inventoryInfos.add(inventoryInfo);
            }
        }
        return inventoryInfos;
    }

    @Override
    public List<InventoryInfo> getInventoryByBarcode(String barcode) {
        List<InventoryInfo> inventoryInfos = new ArrayList<>();
        for (InventoryInfo inventoryInfo : getAllInventory()) {
            if (inventoryInfo.getBarcode().equals(barcode)) {
                inventoryInfos.add(inventoryInfo);
            }
        }
        return inventoryInfos;
    }

    @Override
    public List<InventoryInfo> getInventoryBySearchKeyTl(String searchKeyTl) {
        List<InventoryInfo> inventoryInfos = new ArrayList<>();
        for (InventoryInfo inventoryInfo : getAllInventory()) {
            if (inventoryInfo.getSearchKeyTl().contains(searchKeyTl)) {
                inventoryInfos.add(inventoryInfo);
            }
        }
        return inventoryInfos;
    }

    @Override
    public List<InventoryInfo> getInventoryByWarehouseAndStoresIoc(String warehouse, String storesloc) {
        List<InventoryInfo> inventoryInfos = new ArrayList<>();
        for (InventoryInfo inventoryInfo : getAllInventory()) {
            if (inventoryInfo.getWarehouse().equals(warehouse) && inventoryInfo.getStoresloc().equals(storesloc)) {
                inventoryInfos.add(inventoryInfo);
            }
        }
        return inventoryInfos;
    }

}
