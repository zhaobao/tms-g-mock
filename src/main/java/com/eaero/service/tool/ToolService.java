package com.eaero.service.tool;

import com.eaero.service.tool.pojo.ToolInfo;

import java.util.List;

/**
 * @author zb
 * @date 2023/02/24 024 13:11
 */
public interface ToolService {

    List<ToolInfo> getAllTool();

    List<ToolInfo> getAllToolByPartnoAndTrno(String partno,String trno);

    List<ToolInfo> getAllToolByBarcode();

    List<ToolInfo> getAllToolByRfidId(String rfid);

    List<ToolInfo> getAllToolByWarehouse(String warehouse);

}
