package com.eaero.service.tool;

import com.eaero.service.tool.pojo.InventoryInfo;

import java.util.List;

/**
 * @author zzj
 * @date 2023/2/24 15:18
 * @description
 */
public interface InventoryService {

    List<InventoryInfo> getAllInventory();

    List<InventoryInfo> getInventoryByRid(String rid);

    List<InventoryInfo> getInventoryByRfidId(String rfidId);

    List<InventoryInfo> getInventoryByPartnoAndTrno(String partno, String trno);

    List<InventoryInfo> getInventoryByPartdescr(String partdescr);

    List<InventoryInfo> getInventoryByBarcode(String barcode);

    List<InventoryInfo> getInventoryBySearchKeyTl(String searchKeyTl);

    List<InventoryInfo> getInventoryByWarehouseAndStoresIoc(String warehouse, String storesloc);

}
