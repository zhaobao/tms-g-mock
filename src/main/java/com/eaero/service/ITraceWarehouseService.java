package com.eaero.service;

import com.eaero.CommonResp;
import com.eaero.entity.TraceWarehouse;
import com.baomidou.mybatisplus.extension.service.IService;
import com.eaero.service.warehouse.pojo.WarehouseInfo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
public interface ITraceWarehouseService extends IService<TraceWarehouse> {

    /**
     * 根据仓库获取其相关信息
     * @param warehouse
     * @return
     */
    List<TraceWarehouse> getWarehouseInfo(String warehouse);

}
