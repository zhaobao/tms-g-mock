package com.eaero.service.user;

import com.eaero.service.user.pojo.UserInfo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Description:
 *
 * @author Ye Zhihong
 * @date 2023/2/24 10:43
 */
@Service
public class UserInfoServiceImpl implements UserInfoService {
    @Override
    public List<UserInfo> getUserInfoAll() {
        List<UserInfo> list = new ArrayList<>();
        String[] names = new String[]{"", "张三", "李四", "王五"};
        for (int i = 1; i <= 3; i++) {
            UserInfo userInfo = new UserInfo();
            userInfo.setUserCode("user00" + i);
            userInfo.setUserName(names[i]);
            userInfo.setUserDept("IT部");
            userInfo.setComMobile("13723727482");
            userInfo.setSnrBox("66666" + i);
            list.add(userInfo);

        }
        return list;
    }

    /**
     * @param userCode
     * @return
     */
    @Override
    public List<UserInfo> getUserInfoAllByUserCode(String userCode) {
        List<UserInfo> list = new ArrayList<>();
        getUserInfoAll().forEach(new Consumer<UserInfo>() {
            @Override
            public void accept(UserInfo userInfo) {
                if (userInfo.getUserCode().equals(userCode)) list.add(userInfo);
            }
        });
        return list;
    }
}
