package com.eaero.service.user.pojo;

import cn.hutool.core.lang.Range;
import lombok.Data;

/**
 * Description:
 *
 * @author Ye Zhihong
 * @date 2023/2/24 10:39
 */
@Data
public class UserInfo {
    private String userCode;
    private String userName;
    private String comMobile;
    private String userDept;
    private String snrBox;
}
