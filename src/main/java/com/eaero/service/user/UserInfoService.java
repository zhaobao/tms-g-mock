package com.eaero.service.user;

import com.eaero.service.user.pojo.UserInfo;

import java.util.List;

/**
 * Description:
 *
 * @author Ye Zhihong
 * @date 2023/2/24 10:42
 */
public interface UserInfoService {
    List<UserInfo> getUserInfoAll();

    List<UserInfo> getUserInfoAllByUserCode(String userCode);
}
