package com.eaero.service.deliverd;

import com.eaero.service.deliverd.pojo.DeliverDetail;

import java.util.List;

/**
 * @author luofeng
 * @create 2023/2/24 10:58
 */
public interface DeliverService {

    /**
     * 查询工具配送明细
     * @return
     */
    List<DeliverDetail> getDeliverDetail();
}
