package com.eaero.service.deliverd;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.RandomUtil;
import com.eaero.service.deliverd.pojo.DeliverDetail;
import com.eaero.service.pick.PickService;
import com.eaero.service.pick.pojo.PickDetail;
import com.eaero.service.user.UserInfoService;
import com.eaero.service.user.pojo.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author luofeng
 * @create 2023/2/24 10:58
 */
@Service
public class DeliverServiceImpl implements DeliverService {

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private PickService pickService;

    @Override
    public List<DeliverDetail> getDeliverDetail() {
        // 查询拣料详情
        List<PickDetail> pickDetails = pickService.getPickDetail();

        List<UserInfo> userInfos = userInfoService.getUserInfoAll();
        UserInfo first = CollUtil.getFirst(userInfos);
        UserInfo last = CollUtil.getLast(userInfos);

        List<DeliverDetail> dataList = new ArrayList<>();
        String deliverCodePrefix = "93866481230";
        String tddIdPrefix = "838047258";
        for (int x = 0; x < pickDetails.size(); x++) {
            PickDetail pickDetail = pickDetails.get(x);
            DeliverDetail data = new DeliverDetail();
            data.setTddId(tddIdPrefix + x);
            data.setDeliverCode(deliverCodePrefix + "0");
            data.setWorkorderno(pickDetail.getWorkorder());
            data.setJobcard("123456789");
            data.setPartno(pickDetail.getPartno());
            data.setSerialno(pickDetail.getSeriano());
            data.setPartdescr(pickDetail.getPartdescr());
            data.setCatalogueType("SPTL");
            data.setTailNo(pickDetail.getTailNo());
            data.setVisitDesc(pickDetail.getVisitDesc());
            data.setIssueUser(first.getUserName());
            data.setIssueDate("2023-01-06 10:14");
            data.setWarehouse(pickDetail.getWarehouse());
            data.setUpdateUser(last.getUserName());
            data.setUpdateDate("2023-01-06 10:14");
            data.setReqBy(last.getUserName());
            data.setComMobile(last.getComMobile());
            data.setStatus(RandomUtil.randomEle(CollUtil.newArrayList("待配送", "配送中", "工作者签收", "回收中", "关闭")));
            data.setDeliverUser(first.getUserName());
            data.setDeliverDate("2023-01-06 10:14");
            dataList.add(data);
        }
        return dataList;
    }
}
