package com.eaero.service.deliverd.pojo;

import lombok.Getter;
import lombok.Setter;

/**
 * @author luofeng
 * @create 2023/2/24 11:03
 */
@Setter
@Getter
public class DeliverDetail {

    private String tddId;
    private String deliverCode;
    private String workorderno;
    private String jobcard;
    private String partno;
    private String serialno;
    private String partdescr;
    private String catalogueType;
    private String tailNo;
    private String visitDesc;
    private String issueUser;
    private String issueDate;
    private String warehouse;
    private String updateUser;
    private String updateDate;
    private String reqBy;
    private String comMobile;
    private String status;
    private String deliverUser;
    private String deliverDate;
}
