package com.eaero.service;

import com.eaero.CommonResp;
import com.eaero.entity.TraceToolStock;
import com.baomidou.mybatisplus.extension.service.IService;
import com.eaero.vo.CancelTlStockRequest;
import com.eaero.vo.DoTlStockRequest;
import com.eaero.vo.GoTlStockRequest;
import com.eaero.vo.ToolStockRequest;

/**
* @author Ye Zhihong
* @description 针对表【trace_tool_stock】的数据库操作Service
* @createDate 2023-03-17 16:24:50
*/
public interface ITraceToolStockService extends IService<TraceToolStock> {

    /**
     * 获取工具盘点单信息
     * @param request
     * @return
     */
    CommonResp getToolStock(ToolStockRequest request);

    /**
     * 根据盘点单号获取工具盘点单明细
     * @param request
     * @return
     */
    CommonResp getToolStockDetail(ToolStockRequest request);

    CommonResp goTlStock(GoTlStockRequest request);

    CommonResp CancelTlStock(CancelTlStockRequest request);

    CommonResp DoTlStockRequest(DoTlStockRequest request);
}
