package com.eaero.service.util;

import java.io.*;

/**
 * @author zb
 * @date 2023/02/24 024 11:21
 */
public class FileUtil {
    public static void writeFile(String fileName ,String json) throws IOException {
        File file = new File("src/main/resources/"+fileName);
        FileOutputStream fos = new FileOutputStream(file);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        bw.write(json);
        bw.close();
    }
}
