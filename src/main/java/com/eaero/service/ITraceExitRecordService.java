package com.eaero.service;

import com.eaero.CommonResp;
import com.eaero.controller.exit.pojo.TraceCreateToolExitRequest;
import com.eaero.entity.TraceExitRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author luofeng
 * @since 2023-07-07
 */
public interface ITraceExitRecordService extends IService<TraceExitRecord> {

    /**
     * 创建工具退库单
     * @param request
     * @return
     */
    CommonResp postGoTlReturn(TraceCreateToolExitRequest request);

    /**
     * 查询工具退库单
     * @param returnPnum
     * @param createUser
     * @param created
     * @param status
     * @param origin
     * @param type
     * @return
     */
    CommonResp getGoTlReturn(String returnPnum, String createUser, String created, String status, String origin, String type);

    /**
     * 查询工具退料明细
     * @param returnPnum
     * @return
     */
    CommonResp getTlReturnList(String returnPnum);
}
