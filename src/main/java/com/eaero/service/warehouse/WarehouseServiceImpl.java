package com.eaero.service.warehouse;

import com.eaero.entity.TraceWarehouse;
import com.eaero.service.warehouse.pojo.WarehouseInfo;
import com.eaero.service.warehouse.pojo.WarehouseLocation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zb
 * @date 2023/02/24 024 9:59
 */
@Service
public class WarehouseServiceImpl implements WarehouseService{


    @Override
    public List<TraceWarehouse> getWarehouse(String warehouse) {
        List<TraceWarehouse> warehouseIdList = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            TraceWarehouse warehouseInfo = new TraceWarehouse();
            warehouseInfo.setWarehouse("warehouse"+i);
            warehouseInfo.setCommentText(i+"号仓库");
            warehouseInfo.setStation("仓库站点"+i);
            warehouseInfo.setStationFacility("仓库设施"+i);
            warehouseIdList.add(warehouseInfo);
        }
        return warehouseIdList;
    }

    @Override
    public List<WarehouseLocation> getWarehouseLocationByWarehouse(String warehouse) {
        List<WarehouseLocation> stationList = new ArrayList<>();
        for (TraceWarehouse warehouseInfo : getWarehouse(warehouse)) {
            if(StringUtils.equals(warehouse,warehouseInfo.getWarehouse())){
                for (int i = 0; i < 5; i++) {
                    WarehouseLocation warehouseLocation = new WarehouseLocation();
                    warehouseLocation.setStoresloc(warehouseInfo.getCommentText()+i+"定位");
                    stationList.add(warehouseLocation);
                }
            }
        }
        return stationList;
    }
}
