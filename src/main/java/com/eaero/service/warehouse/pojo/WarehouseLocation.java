package com.eaero.service.warehouse.pojo;

import lombok.Data;

import java.util.List;

/**
 * @author zb
 * @date 2023/02/24 024 10:09
 */
@Data
public class WarehouseLocation {

    private String storesloc;

}
