package com.eaero.service.warehouse.pojo;

import lombok.Data;

/**
 * @author zb
 * @date 2023/02/24 024 9:54
 */
@Data
public class WarehouseInfo {
    /**
     * 仓库
     */
    private String warehouse;
    /**
     * 仓库描述
     */
    private String commentText;
    /**
     * 仓库站点
     */
    private String station;
    /**
     * 仓库设施
     */
    private String stationFacility;


}
