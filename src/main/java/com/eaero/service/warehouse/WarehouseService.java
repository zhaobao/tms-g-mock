package com.eaero.service.warehouse;

import com.eaero.entity.TraceWarehouse;
import com.eaero.service.warehouse.pojo.WarehouseInfo;
import com.eaero.service.warehouse.pojo.WarehouseLocation;

import java.util.List;

/**
 * @author zb
 * @date 2023/02/24 024 9:53
 */
public interface WarehouseService {
    List<TraceWarehouse> getWarehouse(String warehouse);

    List<WarehouseLocation> getWarehouseLocationByWarehouse(String warehouse);
}
