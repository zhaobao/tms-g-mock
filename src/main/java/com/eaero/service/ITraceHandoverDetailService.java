package com.eaero.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.eaero.CommonResp;
import com.eaero.entity.TraceHandoverDetail;
import com.eaero.vo.DoHandoverRequest;
import com.eaero.vo.HandoverDtlLocRequest;
import com.eaero.vo.HandoverDtlRequest;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
public interface ITraceHandoverDetailService extends IService<TraceHandoverDetail> {

    /**
     * 查询交接班明细
     * @param request
     * @return
     */
    CommonResp getHandoverDtlInfo(HandoverDtlRequest request);

    /**
     * 查询交接班明细定位
     * @param request
     * @return
     */
    CommonResp getHandoverDtlLoc(HandoverDtlLocRequest request);

    /**
     * 接班
     * @param request
     * @return
     */
    CommonResp doHandover(DoHandoverRequest request);
}
