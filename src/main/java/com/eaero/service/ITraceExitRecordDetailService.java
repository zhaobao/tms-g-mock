package com.eaero.service;

import com.eaero.entity.TraceExitRecordDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author luofeng
 * @since 2023-07-07
 */
public interface ITraceExitRecordDetailService extends IService<TraceExitRecordDetail> {

}
