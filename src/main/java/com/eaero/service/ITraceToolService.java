package com.eaero.service;

import com.eaero.CommonResp;
import com.eaero.controller.tool.pojo.IssueRequest;
import com.eaero.controller.tool.pojo.ReturnRequest;
import com.eaero.controller.tool.pojo.ToolDetail;
import com.eaero.controller.tool.pojo.WorkOrderInfo;
import com.eaero.entity.TraceJobCard;
import com.eaero.entity.TraceTool;
import com.baomidou.mybatisplus.extension.service.IService;
import com.eaero.vo.BindRFID;
import com.eaero.vo.ExceptionFeelBackRequest;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
public interface ITraceToolService extends IService<TraceTool> {

    /**
     * 异常
     * @param request
     * @return
     */
    CommonResp exceptionFeelBack(ExceptionFeelBackRequest request);
    List<TraceTool> getToolInfo(String rfidId, String partno, String trno, String partdescr, String barcode, String searchKeyTl, String warehouse);

    List<TraceTool> getInventoryInfo(String rid, String rfidId, String partno, String trno, String partdescr, String barcode, String searchKeyTl, String warehouse, String storesloc);

    CommonResp issueTool(IssueRequest request);

    CommonResp returnTool(ReturnRequest request);

    List<TraceJobCard> GetWorkorderno(String workorderno);

    List<TraceTool> GetLoanToolsAll(String issueTo, String rfidId, String partno, String trno, String partdescr, String barcode, String issueDate, String warehouse, String storesloc, String issueRid, String jcWo, String acno, String tpNo,String searchKeyTl);

    CommonResp bindRFID(BindRFID request);

    CommonResp unBindRFID(BindRFID request);

    void doTlDiscard(List<String> trno);

    List<ToolDetail> getTooldetail(String rid, String trno, String partno);
}
