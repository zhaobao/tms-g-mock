package com.eaero.service;

import com.eaero.CommonResp;
import com.eaero.entity.TraceWorkOrder;
import com.baomidou.mybatisplus.extension.service.IService;
import com.eaero.service.workorder.pojo.CancelWorkOrderRequest;
import com.eaero.service.workorder.pojo.WorkOrderDetailRequset;
import com.eaero.service.workorder.pojo.WorkOrderRequest;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
public interface ITraceWorkOrderService extends IService<TraceWorkOrder> {


    /**
     * 生成工单申请单
     *
     * @return
     */
    void genWorkOrder();

    /**
     * 查询工具申请单
     * @param workOrderRequest
     * @return
     */
    CommonResp getWorkOrder(WorkOrderRequest workOrderRequest);

    /**
     * 作废工具申请单
     * @param request
     * @return
     */
    CommonResp cancellationWorkOrder(CancelWorkOrderRequest request);
}
