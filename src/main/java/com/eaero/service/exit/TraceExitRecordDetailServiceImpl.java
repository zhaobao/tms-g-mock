package com.eaero.service.exit;

import com.eaero.entity.TraceExitRecordDetail;
import com.eaero.mapper.TraceExitRecordDetailMapper;
import com.eaero.service.ITraceExitRecordDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author luofeng
 * @since 2023-07-07
 */
@Service
public class TraceExitRecordDetailServiceImpl extends ServiceImpl<TraceExitRecordDetailMapper, TraceExitRecordDetail> implements ITraceExitRecordDetailService {

}
