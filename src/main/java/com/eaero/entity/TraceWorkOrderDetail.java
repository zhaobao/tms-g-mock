package com.eaero.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
@TableName("trace_work_order_detail")
public class TraceWorkOrderDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 申请单明细
     */
    private String id;

    /**
     * 申请单号
     */
    private String reqNo;

    /**
     * 申请明细 ID
     */
    private String rid;

    /**
     * 件号
     */
    private String pn;

    /**
     * 需求件号
     */
    private String reqPn;

    /**
     * 申请数量
     */
    private Integer qty;

    /**
     * 单位
     */
    private String unitofmeas;

    /**
     * 工卡序号
     */
    private String jcSeq;

    /**
     * 工卡指令号
     */
    private String jcWorkorder;

    /**
     * 明细状态（N 草稿/R 申请/P 拣料中/U 关闭/B 部分发料/缺件 L/发	料 I）
     */
    private String dtlSts;

    /**
     * 处理回复
     */
    private String msg;

    /**
     * 处理回复人
     */
    private String msgUser;

    /**
     * 处理回复时间
     */
    private LocalDateTime msgDate;

    /**
     * 拣料仓库
     */
    private String whsType;

    /**
     * 申请明细备注
     */
    private String remark;

    /**
     * 机尾号
     */
    private String tailNo;

    /**
     * 件号描述
     */
    private String partdescr;

    /**
     * 库存信息
     */
    private String onhandInfo;

    /**
     * 工具收货人
     */
    private String rcvBy;

    /**
     * 需求日期
     */
    private LocalDateTime reqDate;

    /**
     * 提交人
     */
    private String sndBy;

    /**
     * 提交时间
     */
    private LocalDateTime sndDate;

    /**
     * 更新人
     */
    private String uptBy;

    /**
     * 更新时间
     */
    private LocalDateTime uptDate;

    /**
     * 拣料人
     */
    private String pickUser;

    /**
     * 拣料时间
     */
    private LocalDateTime pickDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getReqNo() {
        return reqNo;
    }

    public void setReqNo(String reqNo) {
        this.reqNo = reqNo;
    }
    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }
    public String getPn() {
        return pn;
    }

    public void setPn(String pn) {
        this.pn = pn;
    }
    public String getReqPn() {
        return reqPn;
    }

    public void setReqPn(String reqPn) {
        this.reqPn = reqPn;
    }
    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }
    public String getUnitofmeas() {
        return unitofmeas;
    }

    public void setUnitofmeas(String unitofmeas) {
        this.unitofmeas = unitofmeas;
    }
    public String getJcSeq() {
        return jcSeq;
    }

    public void setJcSeq(String jcSeq) {
        this.jcSeq = jcSeq;
    }
    public String getJcWorkorder() {
        return jcWorkorder;
    }

    public void setJcWorkorder(String jcWorkorder) {
        this.jcWorkorder = jcWorkorder;
    }
    public String getDtlSts() {
        return dtlSts;
    }

    public void setDtlSts(String dtlSts) {
        this.dtlSts = dtlSts;
    }
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    public String getMsgUser() {
        return msgUser;
    }

    public void setMsgUser(String msgUser) {
        this.msgUser = msgUser;
    }
    public LocalDateTime getMsgDate() {
        return msgDate;
    }

    public void setMsgDate(LocalDateTime msgDate) {
        this.msgDate = msgDate;
    }
    public String getWhsType() {
        return whsType;
    }

    public void setWhsType(String whsType) {
        this.whsType = whsType;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public String getTailNo() {
        return tailNo;
    }

    public void setTailNo(String tailNo) {
        this.tailNo = tailNo;
    }
    public String getPartdescr() {
        return partdescr;
    }

    public void setPartdescr(String partdescr) {
        this.partdescr = partdescr;
    }
    public String getOnhandInfo() {
        return onhandInfo;
    }

    public void setOnhandInfo(String onhandInfo) {
        this.onhandInfo = onhandInfo;
    }
    public String getRcvBy() {
        return rcvBy;
    }

    public void setRcvBy(String rcvBy) {
        this.rcvBy = rcvBy;
    }
    public LocalDateTime getReqDate() {
        return reqDate;
    }

    public void setReqDate(LocalDateTime reqDate) {
        this.reqDate = reqDate;
    }
    public String getSndBy() {
        return sndBy;
    }

    public void setSndBy(String sndBy) {
        this.sndBy = sndBy;
    }
    public LocalDateTime getSndDate() {
        return sndDate;
    }

    public void setSndDate(LocalDateTime sndDate) {
        this.sndDate = sndDate;
    }
    public String getUptBy() {
        return uptBy;
    }

    public void setUptBy(String uptBy) {
        this.uptBy = uptBy;
    }
    public LocalDateTime getUptDate() {
        return uptDate;
    }

    public void setUptDate(LocalDateTime uptDate) {
        this.uptDate = uptDate;
    }
    public String getPickUser() {
        return pickUser;
    }

    public void setPickUser(String pickUser) {
        this.pickUser = pickUser;
    }
    public LocalDateTime getPickDate() {
        return pickDate;
    }

    public void setPickDate(LocalDateTime pickDate) {
        this.pickDate = pickDate;
    }

    @Override
    public String toString() {
        return "TraceWorkOrderDetail{" +
            "id=" + id +
            ", reqNo=" + reqNo +
            ", rid=" + rid +
            ", pn=" + pn +
            ", reqPn=" + reqPn +
            ", qty=" + qty +
            ", unitofmeas=" + unitofmeas +
            ", jcSeq=" + jcSeq +
            ", jcWorkorder=" + jcWorkorder +
            ", dtlSts=" + dtlSts +
            ", msg=" + msg +
            ", msgUser=" + msgUser +
            ", msgDate=" + msgDate +
            ", whsType=" + whsType +
            ", remark=" + remark +
            ", tailNo=" + tailNo +
            ", partdescr=" + partdescr +
            ", onhandInfo=" + onhandInfo +
            ", rcvBy=" + rcvBy +
            ", reqDate=" + reqDate +
            ", sndBy=" + sndBy +
            ", sndDate=" + sndDate +
            ", uptBy=" + uptBy +
            ", uptDate=" + uptDate +
            ", pickUser=" + pickUser +
            ", pickDate=" + pickDate +
        "}";
    }
}
