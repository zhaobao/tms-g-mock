package com.eaero.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
@TableName("trace_handover_detail")
public class TraceHandoverDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 交班单明细 ID
     */
    @TableId(value = "tsd_id", type = IdType.AUTO)
    private Integer tsdId;

    /**
     * 交班单id
     */
    private Integer tsId;

    /**
     * 仓库
     */
    private String warehouse;

    /**
     * 定位
     */
    private String storesloc;

    /**
     * 班次
     */
    private String shift;

    /**
     * 盘点需求
     */
    private String checkReq;

    /**
     * 在库数量
     */
    private Integer whseIn;

    /**
     * 发料数量
     */
    private Integer whseIssue;

    /**
     * 盘点数量
     */
    private Integer whseCheck;

    /**
     * 接班明细备注
     */
    private String remark;

    /**
     * 交班单备注
     */
    private String hoRemark;

    /**
     * 接班单备注
     */
    private String toRemark;

    /**
     * 工具件号
     */
    private String partno;

    /**
     * 工具刻号
     */
    private String trno;

    /**
     * 故障状态（空值/故障信息）
     */
    private String tlFlag;

    /**
     * 图片（Y/N）
     */
    private String imgFlag;

    /**
     * 工具顺序号
     */
    private String rid;

    /**
     * 单位
     */
    private String unitofmeas;

    /**
     * 条形码
     */
    private String barcode;

    /**
     * RFID号
     */
    private String rfidId;

    /**
     * 父件号
     */
    private String parentPartno;

    /**
     * 父刻号
     */
    private String parentTrno;

    /**
     * 父件顺序号
     */
    private String parent;

    /**
     * 仓库描述
     */
    private String whseDesc;

    /**
     * 站点
     */
    private String station;

    /**
     * 真实件号
     */
    private String alternatePartno;

    /**
     * 子件数量
     */
    private Integer childNum;

    /**
     * 计量号
     */
    private String measuringNo;

    /**
     * 替代件号
     */
    private String standByPn;

    /**
     * 件号描述
     */
    private String partdescr;

    /**
     * 工具类型
     */
    private String catalogueType;

    /**
     * 0=未接班，1=接班
     */
    @JsonIgnore
    private String isHandingOf;

    /**
     * 工具状态（借出/在库）
     */
    private String tlStatus;

    /**
     * 创建时间
     */
    @JsonIgnore
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @JsonIgnore
    private LocalDateTime updateTime;

    public Integer getTsdId() {
        return tsdId;
    }

    public void setTsdId(Integer tsdId) {
        this.tsdId = tsdId;
    }

    public Integer getTsId() {
        return tsId;
    }

    public void setTsId(Integer tsId) {
        this.tsId = tsId;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public String getStoresloc() {
        return storesloc;
    }

    public void setStoresloc(String storesloc) {
        this.storesloc = storesloc;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getCheckReq() {
        return checkReq;
    }

    public void setCheckReq(String checkReq) {
        this.checkReq = checkReq;
    }

    public Integer getWhseIn() {
        return whseIn;
    }

    public void setWhseIn(Integer whseIn) {
        this.whseIn = whseIn;
    }

    public Integer getWhseIssue() {
        return whseIssue;
    }

    public void setWhseIssue(Integer whseIssue) {
        this.whseIssue = whseIssue;
    }

    public Integer getWhseCheck() {
        return whseCheck;
    }

    public void setWhseCheck(Integer whseCheck) {
        this.whseCheck = whseCheck;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getHoRemark() {
        return hoRemark;
    }

    public void setHoRemark(String hoRemark) {
        this.hoRemark = hoRemark;
    }

    public String getToRemark() {
        return toRemark;
    }

    public void setToRemark(String toRemark) {
        this.toRemark = toRemark;
    }

    public String getPartno() {
        return partno;
    }

    public void setPartno(String partno) {
        this.partno = partno;
    }

    public String getTrno() {
        return trno;
    }

    public void setTrno(String trno) {
        this.trno = trno;
    }

    public String getTlFlag() {
        return tlFlag;
    }

    public void setTlFlag(String tlFlag) {
        this.tlFlag = tlFlag;
    }

    public String getImgFlag() {
        return imgFlag;
    }

    public void setImgFlag(String imgFlag) {
        this.imgFlag = imgFlag;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getUnitofmeas() {
        return unitofmeas;
    }

    public void setUnitofmeas(String unitofmeas) {
        this.unitofmeas = unitofmeas;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getRfidId() {
        return rfidId;
    }

    public void setRfidId(String rfidId) {
        this.rfidId = rfidId;
    }

    public String getParentPartno() {
        return parentPartno;
    }

    public void setParentPartno(String parentPartno) {
        this.parentPartno = parentPartno;
    }

    public String getParentTrno() {
        return parentTrno;
    }

    public void setParentTrno(String parentTrno) {
        this.parentTrno = parentTrno;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getWhseDesc() {
        return whseDesc;
    }

    public void setWhseDesc(String whseDesc) {
        this.whseDesc = whseDesc;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getAlternatePartno() {
        return alternatePartno;
    }

    public void setAlternatePartno(String alternatePartno) {
        this.alternatePartno = alternatePartno;
    }

    public Integer getChildNum() {
        return childNum;
    }

    public void setChildNum(Integer childNum) {
        this.childNum = childNum;
    }

    public String getMeasuringNo() {
        return measuringNo;
    }

    public void setMeasuringNo(String measuringNo) {
        this.measuringNo = measuringNo;
    }

    public String getStandByPn() {
        return standByPn;
    }

    public void setStandByPn(String standByPn) {
        this.standByPn = standByPn;
    }

    public String getPartdescr() {
        return partdescr;
    }

    public void setPartdescr(String partdescr) {
        this.partdescr = partdescr;
    }

    public String getCatalogueType() {
        return catalogueType;
    }

    public void setCatalogueType(String catalogueType) {
        this.catalogueType = catalogueType;
    }

    public String getTlStatus() {
        return tlStatus;
    }

    public String getIsHandingOf() {
        return isHandingOf;
    }

    public void setIsHandingOf(String isHandingOf) {
        this.isHandingOf = isHandingOf;
    }

    public void setTlStatus(String tlStatus) {
        this.tlStatus = tlStatus;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "TraceHandoverDetail{" +
                "tsdId=" + tsdId +
                ", warehouse=" + warehouse +
                ", storesloc=" + storesloc +
                ", checkReq=" + checkReq +
                ", whseIn=" + whseIn +
                ", whseIssue=" + whseIssue +
                ", whseCheck=" + whseCheck +
                ", remark=" + remark +
                ", hoRemark=" + hoRemark +
                ", toRemark=" + toRemark +
                ", partno=" + partno +
                ", trno=" + trno +
                ", tlFlag=" + tlFlag +
                ", imgFlag=" + imgFlag +
                ", rid=" + rid +
                ", unitofmeas=" + unitofmeas +
                ", barcode=" + barcode +
                ", rfidId=" + rfidId +
                ", parentPartno=" + parentPartno +
                ", parentTrno=" + parentTrno +
                ", parent=" + parent +
                ", whseDesc=" + whseDesc +
                ", station=" + station +
                ", alternatePartno=" + alternatePartno +
                ", childNum=" + childNum +
                ", measuringNo=" + measuringNo +
                ", standByPn=" + standByPn +
                ", partdescr=" + partdescr +
                ", catalogueType=" + catalogueType +
                ", tlStatus=" + tlStatus +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                "}";
    }
}
