package com.eaero.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

/**
 * 
 * @TableName trace_tool_stock
 */
@TableName(value ="trace_tool_stock")
public class TraceToolStock implements Serializable {
    /**
     * 盘点单 ID
     */
    @TableId(type = IdType.AUTO)
    private Integer tsId;

    /**
     * 盘点单号
     */
    private String pnum;

    /**
     * 创建者
     */
    private String createdUser;

    /**
     * 创建时间（yyyy-mm-dd）
     */
    private String created;

    /**
     * 更新者
     */
    private String updatedUser;

    /**
     * 更新时间
     */
    private String updated;

    /**
     * 盘点备注
     */
    private String remark;

    /**
     * 状态（开放/盘点中/关闭/作废）
     */
    private String status;

    /**
     * 盘点来源
     */
    private String origin;

    /**
     * 仓库
     */
    private String warehouse;

    /**
     * 员工号
     */
    private String userCode;

    /**
     * 0=作废，1=正常
     */
    private String cancellationc;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 盘点单 ID
     */
    public Integer getTsId() {
        return tsId;
    }

    /**
     * 盘点单 ID
     */
    public void setTsId(Integer tsId) {
        this.tsId = tsId;
    }

    public String getPnum() {
        return pnum;
    }

    public void setPnum(String pnum) {
        this.pnum = pnum;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    /**
     * 创建时间（yyyy-mm-dd）
     */
    public String getCreated() {
        return created;
    }

    /**
     * 创建时间（yyyy-mm-dd）
     */
    public void setCreated(String created) {
        this.created = created;
    }

    /**
     * 更新者
     */
    public String getUpdatedUser() {
        return updatedUser;
    }

    /**
     * 更新者
     */
    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    /**
     * 更新时间
     */
    public String getUpdated() {
        return updated;
    }

    /**
     * 更新时间
     */
    public void setUpdated(String updated) {
        this.updated = updated;
    }

    /**
     * 盘点备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 盘点备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 状态（开放/盘点中/关闭/作废）
     */
    public String getStatus() {
        return status;
    }

    /**
     * 状态（开放/盘点中/关闭/作废）
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 盘点来源
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * 盘点来源
     */
    public void setOrigin(String origin) {
        this.origin = origin;
    }

    /**
     * 仓库
     */
    public String getWarehouse() {
        return warehouse;
    }

    /**
     * 仓库
     */
    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    /**
     * 员工号 
     */
    public String getUserCode() {
        return userCode;
    }

    /**
     * 员工号 
     */
    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    /**
     * 0=作废，1=正常 
     */
    public String getCancellationc() {
        return cancellationc;
    }

    /**
     * 0=作废，1=正常 
     */
    public void setCancellationc(String cancellationc) {
        this.cancellationc = cancellationc;
    }


}