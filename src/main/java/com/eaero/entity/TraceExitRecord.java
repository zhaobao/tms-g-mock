package com.eaero.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author luofeng
 * @since 2023-07-07
 */
@TableName("trace_exit_record")
public class TraceExitRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 退料单 ID
     */
    private String trId;

    private String id;

    /**
     * 退料单号
     */
    private String returnPnum;

    /**
     * 来源 P:PC;A:ATM;H:手持设备;D:工作台
     */
    private String origin;

    /**
     * 退料单备注
     */
    private String returnRemark;

    /**
     * 退料单状态 草稿/提交退料/配送中/关闭
     */
    private String status;

    /**
     * 仓库
     */
    private String warehouse;

    /**
     * 更新时间
     */
    private LocalDateTime updated;

    /**
     * 更新用户
     */
    private String updateUserCode;

    /**
     * 更新用户
     */
    private String updateUserName;

    /**
     * 创建用户号
     */
    private String createUserCode;

    /**
     * 创建员工名
     */
    private String createUserName;

    private LocalDateTime created;

    public String getTrId() {
        return trId;
    }

    public void setTrId(String trId) {
        this.trId = trId;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }
    public String getReturnRemark() {
        return returnRemark;
    }

    public void setReturnRemark(String returnRemark) {
        this.returnRemark = returnRemark;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }
    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }
    public String getCreateUserCode() {
        return createUserCode;
    }

    public void setCreateUserCode(String createUserCode) {
        this.createUserCode = createUserCode;
    }
    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }
    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }


    public String getReturnPnum() {
        return returnPnum;
    }

    public void setReturnPnum(String returnPnum) {
        this.returnPnum = returnPnum;
    }

    public String getUpdateUserCode() {
        return updateUserCode;
    }

    public void setUpdateUserCode(String updateUserCode) {
        this.updateUserCode = updateUserCode;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }
}
