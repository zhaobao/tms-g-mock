package com.eaero.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
@TableName("trace_warehouse")
public class TraceWarehouse implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 仓库
     */
    private String id;

    /**
     * 仓库号
     */
    private String warehouse;

    /**
     * 仓库描述
     */
    private String commentText;

    /**
     * 仓库站点
     */
    private String station;

    /**
     * 仓库设施
     */
    private String stationFacility;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }
    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }
    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }
    public String getStationFacility() {
        return stationFacility;
    }

    public void setStationFacility(String stationFacility) {
        this.stationFacility = stationFacility;
    }

    @Override
    public String toString() {
        return "TraceWarehouse{" +
            "id=" + id +
            ", warehouse=" + warehouse +
            ", commentText=" + commentText +
            ", station=" + station +
            ", stationFacility=" + stationFacility +
        "}";
    }
}
