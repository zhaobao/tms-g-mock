package com.eaero.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zb
 * @since 2023-04-08
 */
@TableName("trace_exception_feel_back_recode")
public class TraceExceptionFeelBackRecode implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    /**
     * 反馈人
     */
    private String fbBy;

    /**
     * 来源
     */
    private String origin;

    /**
     * 件号
     */
    private String tlPn;

    /**
     * 编号/刻号
     */
    private String tlTr;

    /**
     * 反馈描述
     */
    private String fbDesc;

    /**
     * 正常损坏（Y/N）
     */
    private String normalDamage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getFbBy() {
        return fbBy;
    }

    public void setFbBy(String fbBy) {
        this.fbBy = fbBy;
    }
    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }
    public String getTlPn() {
        return tlPn;
    }

    public void setTlPn(String tlPn) {
        this.tlPn = tlPn;
    }
    public String getTlTr() {
        return tlTr;
    }

    public void setTlTr(String tlTr) {
        this.tlTr = tlTr;
    }
    public String getFbDesc() {
        return fbDesc;
    }

    public void setFbDesc(String fbDesc) {
        this.fbDesc = fbDesc;
    }
    public String getNormalDamage() {
        return normalDamage;
    }

    public void setNormalDamage(String normalDamage) {
        this.normalDamage = normalDamage;
    }

    @Override
    public String toString() {
        return "TraceExceptionFeelBackRecode{" +
            "id=" + id +
            ", fbBy=" + fbBy +
            ", origin=" + origin +
            ", tlPn=" + tlPn +
            ", tlTr=" + tlTr +
            ", fbDesc=" + fbDesc +
            ", normalDamage=" + normalDamage +
        "}";
    }
}
