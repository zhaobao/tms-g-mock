package com.eaero.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
@TableName("trace_tool_deliver")
public class TraceToolDeliver implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 工具配送单
     */
    private String id;

    /**
     * 配送单号
     */
    private String deliverCode;

    /**
     * 发料单号
     */
    private String tpNo;

    /**
     * 发料人
     */
    private String createUser;

    /**
     * 发料时间（yyyy-mm-dd）
     */
    private String createDate;

    /**
     * 状态(0-待配送、1-配送中、2-工作者签收、3-回收中、4-关闭)
     */
    private String status;

    /**
     * 飞机号
     */
    private String tailNo;

    /**
     * 发料仓库
     */
    private String warehouse;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime modifyTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getDeliverCode() {
        return deliverCode;
    }

    public void setDeliverCode(String deliverCode) {
        this.deliverCode = deliverCode;
    }
    public String getTpNo() {
        return tpNo;
    }

    public void setTpNo(String tpNo) {
        this.tpNo = tpNo;
    }
    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }
    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public String getTailNo() {
        return tailNo;
    }

    public void setTailNo(String tailNo) {
        this.tailNo = tailNo;
    }
    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    public LocalDateTime getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(LocalDateTime modifyTime) {
        this.modifyTime = modifyTime;
    }

    @Override
    public String toString() {
        return "TraceToolDeliver{" +
            "id=" + id +
            ", deliverCode=" + deliverCode +
            ", tpNo=" + tpNo +
            ", createUser=" + createUser +
            ", createDate=" + createDate +
            ", status=" + status +
            ", tailNo=" + tailNo +
            ", warehouse=" + warehouse +
            ", createTime=" + createTime +
            ", modifyTime=" + modifyTime +
        "}";
    }
}
