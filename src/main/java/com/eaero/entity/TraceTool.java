package com.eaero.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
@TableName("trace_tool")
public class TraceTool implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 工具表Id
     */
    private String id;

    private String parent;

    /**
     * 工具序号
     */
    private String rid;

    /**
     * 工具件号
     */
    private String partno;

    /**
     * 工具件号描述
     */
    private String partdescr;

    /**
     * 工具刻号
     */
    private String trno;

    /**
     * 父工具件号
     */
    private String parentPartno;

    /**
     * 父工具刻号
     */
    private String parentTrno;

    /**
     * 条形码
     */
    private String barcode;

    /**
     * rfid
     */
    private String rfidId;

    /**
     * 仓库
     */
    private String warehouse;

    /**
     * 仓库描述
     */
    private String whseDesc;

    /**
     * 单位
     */
    private String unitofmeas;

    /**
     * 计量号
     */
    private String measuringNo;

    /**
     * 工具类型SPTL/CMTL
     */
    private String catalogueType;

    /**
     * 查询关键字
     */
    private String searchKeyTl;

    /**
     * 子件数量
     */
    private Integer childNum;

    /**
     * 替代件号
     */
    private String standByPn;

    /**
     * 定位
     */
    private String storesloc;

    /**
     * 故障状态（空值/故障信息
     */
    private String tlFlag;

    /**
     * 图片（Y/N）
     */
    private String imgFlag;

    /**
     * 工具状态（借出/在库）
     */
    private String tlStatus;

    private String issueRemind;

    /**
     * 库存
     */
    private Integer onhand;

    private String issueTo;

    private String issueDate;
    private String jcWo;
    private String acno;
    private String issueRid;
    private String opRemark;
    private String empCode;

    private String flagIssue;
    /**
     * 报废，trace中无此字段，为测试实现逻辑
     */
    private String scrap;
    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }
    public String getPartno() {
        return partno;
    }

    public void setPartno(String partno) {
        this.partno = partno;
    }
    public String getPartdescr() {
        return partdescr;
    }

    public void setPartdescr(String partdescr) {
        this.partdescr = partdescr;
    }
    public String getTrno() {
        return trno;
    }

    public void setTrno(String trno) {
        this.trno = trno;
    }
    public String getParentPartno() {
        return parentPartno;
    }

    public void setParentPartno(String parentPartno) {
        this.parentPartno = parentPartno;
    }
    public String getParentTrno() {
        return parentTrno;
    }

    public void setParentTrno(String parentTrno) {
        this.parentTrno = parentTrno;
    }
    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
    public String getRfidId() {
        return rfidId;
    }

    public void setRfidId(String rfidId) {
        this.rfidId = rfidId;
    }
    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }
    public String getWhseDesc() {
        return whseDesc;
    }

    public void setWhseDesc(String whseDesc) {
        this.whseDesc = whseDesc;
    }
    public String getUnitofmeas() {
        return unitofmeas;
    }

    public void setUnitofmeas(String unitofmeas) {
        this.unitofmeas = unitofmeas;
    }
    public String getMeasuringNo() {
        return measuringNo;
    }

    public void setMeasuringNo(String measuringNo) {
        this.measuringNo = measuringNo;
    }
    public String getCatalogueType() {
        return catalogueType;
    }

    public void setCatalogueType(String catalogueType) {
        this.catalogueType = catalogueType;
    }
    public String getSearchKeyTl() {
        return searchKeyTl;
    }

    public void setSearchKeyTl(String searchKeyTl) {
        this.searchKeyTl = searchKeyTl;
    }
    public Integer getChildNum() {
        return childNum;
    }

    public void setChildNum(Integer childNum) {
        this.childNum = childNum;
    }
    public String getStandByPn() {
        return standByPn;
    }

    public void setStandByPn(String standByPn) {
        this.standByPn = standByPn;
    }
    public String getStoresloc() {
        return storesloc;
    }

    public void setStoresloc(String storesloc) {
        this.storesloc = storesloc;
    }
    public String getTlFlag() {
        return tlFlag;
    }

    public void setTlFlag(String tlFlag) {
        this.tlFlag = tlFlag;
    }
    public String getImgFlag() {
        return imgFlag;
    }

    public void setImgFlag(String imgFlag) {
        this.imgFlag = imgFlag;
    }
    public String getTlStatus() {
        return tlStatus;
    }

    public void setTlStatus(String tlStatus) {
        this.tlStatus = tlStatus;
    }
    public Integer getOnhand() {
        return onhand;
    }

    public void setOnhand(Integer onhand) {
        this.onhand = onhand;
    }

    public String getIssueRemind() {
        return issueRemind;
    }

    public void setIssueRemind(String issueRemind) {
        this.issueRemind = issueRemind;
    }

    public String getIssueTo() {
        return issueTo;
    }

    public void setIssueTo(String issueTo) {
        this.issueTo = issueTo;
    }



    public String getJcWo() {
        return jcWo;
    }

    public void setJcWo(String jcWo) {
        this.jcWo = jcWo;
    }

    public String getAcno() {
        return acno;
    }

    public void setAcno(String acno) {
        this.acno = acno;
    }

    public String getIssueRid() {
        return issueRid;
    }

    public void setIssueRid(String issueRid) {
        this.issueRid = issueRid;
    }

    public String getOpRemark() {
        return opRemark;
    }

    public void setOpRemark(String opRemark) {
        this.opRemark = opRemark;
    }

    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getScrap() {
        return scrap;
    }

    public void setScrap(String scrap) {
        this.scrap = scrap;
    }

    public String getFlagIssue() {
        return flagIssue;
    }

    public void setFlagIssue(String flagIssue) {
        this.flagIssue = flagIssue;
    }
}
