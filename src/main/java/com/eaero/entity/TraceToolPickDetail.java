package com.eaero.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
@TableName("trace_tool_pick_detail")
public class TraceToolPickDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    /**
     * 真实件号
     */
    private String alternatePartno;

    /**
     * 创建时间
     */
    private String createdDate;

    /**
     * 创建时间
     */
    private String createdUser;

    /**
     * 仓库描述
     */
    private String description;

    /**
     * 发料数量
     */
    private Integer issueNum;

    /**
     * 发料 RID
     */
    private String issueRid;

    /**
     * 工卡序号
     */
    private String jcSeq;

    /**
     * 库存数量
     */
    private Integer onhand;

    /**
     * 父件发料建议批号 ID
     */
    private String parentId;

    /**
     * 件号描述
     */
    private String partdescr;

    /**
     * 件号
     */
    private String partno;

    /**
     * 数量
     */
    private Integer qty;

    /**
     * 借用人
     */
    private String rcvBy;

    /**
     * 批号
     */
    private String seriano;

    /**
     * 定位
     */
    private String sotrsloc;

    /**
     * 飞机号
     */
    private String tailNo;

    /**
     * 申请明细 ID
     */
    private String tdRid;

    /**
     * 拣料单
     */
    private String tpId;

    /**
     * 拣料单号
     */
    private String tpNo;

    /**
     * 拣料明细 ID
     */
    private String tpdId;

    /**
     * 建议批号 ID
     */
    private String tpdlId;

    /**
     * 单位
     */
    private String unitofmeas;

    /**
     * 更新时间
     */
    private String updatedDate;

    /**
     * 更新人
     */
    private String updatedUser;

    /**
     * 工卡描述
     */
    private String visitDesc;

    /**
     * 仓库
     */
    private String warehouse;

    /**
     * 工卡指令号
     */
    private String workorder;

    /**
     * 明细件号
     */
    private String detailPartno;

    /**
     * 计量号
     */
    private String measuringNo;

    /**
     * 申请单号
     */
    private String reqNo;

    /**
     * 备注
     */
    private String tpdlRemark;

    /**
     * 流水号
     */
    private String opSeq;

    /**
     * 图片（Y/N）
     */
    private String imgFlag;

    /**
     * RFID 号
     */
    private String rfidId;

    /**
     * 状态
     */
    private String state;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getAlternatePartno() {
        return alternatePartno;
    }

    public void setAlternatePartno(String alternatePartno) {
        this.alternatePartno = alternatePartno;
    }
    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public Integer getIssueNum() {
        return issueNum;
    }

    public void setIssueNum(Integer issueNum) {
        this.issueNum = issueNum;
    }
    public String getIssueRid() {
        return issueRid;
    }

    public void setIssueRid(String issueRid) {
        this.issueRid = issueRid;
    }
    public String getJcSeq() {
        return jcSeq;
    }

    public void setJcSeq(String jcSeq) {
        this.jcSeq = jcSeq;
    }
    public Integer getOnhand() {
        return onhand;
    }

    public void setOnhand(Integer onhand) {
        this.onhand = onhand;
    }
    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
    public String getPartdescr() {
        return partdescr;
    }

    public void setPartdescr(String partdescr) {
        this.partdescr = partdescr;
    }
    public String getPartno() {
        return partno;
    }

    public void setPartno(String partno) {
        this.partno = partno;
    }
    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }
    public String getRcvBy() {
        return rcvBy;
    }

    public void setRcvBy(String rcvBy) {
        this.rcvBy = rcvBy;
    }
    public String getSeriano() {
        return seriano;
    }

    public void setSeriano(String seriano) {
        this.seriano = seriano;
    }
    public String getSotrsloc() {
        return sotrsloc;
    }

    public void setSotrsloc(String sotrsloc) {
        this.sotrsloc = sotrsloc;
    }
    public String getTailNo() {
        return tailNo;
    }

    public void setTailNo(String tailNo) {
        this.tailNo = tailNo;
    }
    public String getTdRid() {
        return tdRid;
    }

    public void setTdRid(String tdRid) {
        this.tdRid = tdRid;
    }
    public String getTpId() {
        return tpId;
    }

    public void setTpId(String tpId) {
        this.tpId = tpId;
    }
    public String getTpNo() {
        return tpNo;
    }

    public void setTpNo(String tpNo) {
        this.tpNo = tpNo;
    }
    public String getTpdId() {
        return tpdId;
    }

    public void setTpdId(String tpdId) {
        this.tpdId = tpdId;
    }
    public String getTpdlId() {
        return tpdlId;
    }

    public void setTpdlId(String tpdlId) {
        this.tpdlId = tpdlId;
    }
    public String getUnitofmeas() {
        return unitofmeas;
    }

    public void setUnitofmeas(String unitofmeas) {
        this.unitofmeas = unitofmeas;
    }
    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }
    public String getVisitDesc() {
        return visitDesc;
    }

    public void setVisitDesc(String visitDesc) {
        this.visitDesc = visitDesc;
    }
    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }
    public String getWorkorder() {
        return workorder;
    }

    public void setWorkorder(String workorder) {
        this.workorder = workorder;
    }
    public String getDetailPartno() {
        return detailPartno;
    }

    public void setDetailPartno(String detailPartno) {
        this.detailPartno = detailPartno;
    }
    public String getMeasuringNo() {
        return measuringNo;
    }

    public void setMeasuringNo(String measuringNo) {
        this.measuringNo = measuringNo;
    }
    public String getReqNo() {
        return reqNo;
    }

    public void setReqNo(String reqNo) {
        this.reqNo = reqNo;
    }
    public String getTpdlRemark() {
        return tpdlRemark;
    }

    public void setTpdlRemark(String tpdlRemark) {
        this.tpdlRemark = tpdlRemark;
    }
    public String getOpSeq() {
        return opSeq;
    }

    public void setOpSeq(String opSeq) {
        this.opSeq = opSeq;
    }
    public String getImgFlag() {
        return imgFlag;
    }

    public void setImgFlag(String imgFlag) {
        this.imgFlag = imgFlag;
    }
    public String getRfidId() {
        return rfidId;
    }

    public void setRfidId(String rfidId) {
        this.rfidId = rfidId;
    }
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "TraceToolPickDetail{" +
            "id=" + id +
            ", alternatePartno=" + alternatePartno +
            ", createdDate=" + createdDate +
            ", createdUser=" + createdUser +
            ", description=" + description +
            ", issueNum=" + issueNum +
            ", issueRid=" + issueRid +
            ", jcSeq=" + jcSeq +
            ", onhand=" + onhand +
            ", parentId=" + parentId +
            ", partdescr=" + partdescr +
            ", partno=" + partno +
            ", qty=" + qty +
            ", rcvBy=" + rcvBy +
            ", seriano=" + seriano +
            ", sotrsloc=" + sotrsloc +
            ", tailNo=" + tailNo +
            ", tdRid=" + tdRid +
            ", tpId=" + tpId +
            ", tpNo=" + tpNo +
            ", tpdId=" + tpdId +
            ", tpdlId=" + tpdlId +
            ", unitofmeas=" + unitofmeas +
            ", updatedDate=" + updatedDate +
            ", updatedUser=" + updatedUser +
            ", visitDesc=" + visitDesc +
            ", warehouse=" + warehouse +
            ", workorder=" + workorder +
            ", detailPartno=" + detailPartno +
            ", measuringNo=" + measuringNo +
            ", reqNo=" + reqNo +
            ", tpdlRemark=" + tpdlRemark +
            ", opSeq=" + opSeq +
            ", imgFlag=" + imgFlag +
            ", rfidId=" + rfidId +
            ", state=" + state +
        "}";
    }
}
