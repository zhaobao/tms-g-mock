package com.eaero.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
@TableName("trace_work_order")
public class TraceWorkOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 申请单表
     */
    private String id;

    /**
     * 申请单号
     */
    private String reqNo;

    /**
     * 机尾号
     */
    private String tailNo;

    /**
     * 定检级别
     */
    private String visitDesc;

    /**
     * 申请状态（N 草稿/R 提交申请/D 作废/P 拣料中/U 关闭）
     */
    private String reqSts;

    /**
     * 申请来源（P:PC;A:ATM;H:手持设备;D:工作台）
     */
    private String origin;

    /**
     * 申请单备注
     */
    private String remark;

    /**
     * 工具申请提交人
     */
    private String reqUserCode;
    /**
     * 工具申请提交人
     */
    private String reqUserName;

    /**
     * 工具申请提交人
     */
    private String reqBy;

    /**
     * 创建时间
     */
    private LocalDateTime crtDate;

    /**
     * 申请明细更新人
     */
    private String uptUserCode;
    /**
     * 申请明细更新人
     */
    private String uptUserName;

    /**
     * 申请明细更新人
     */
    private String uptBy;

    /**
     * 申请明细更新时间
     */
    private LocalDateTime uptDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReqNo() {
        return reqNo;
    }

    public void setReqNo(String reqNo) {
        this.reqNo = reqNo;
    }

    public String getTailNo() {
        return tailNo;
    }

    public void setTailNo(String tailNo) {
        this.tailNo = tailNo;
    }

    public String getVisitDesc() {
        return visitDesc;
    }

    public void setVisitDesc(String visitDesc) {
        this.visitDesc = visitDesc;
    }

    public String getReqSts() {
        return reqSts;
    }

    public void setReqSts(String reqSts) {
        this.reqSts = reqSts;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getReqUserCode() {
        return reqUserCode;
    }

    public void setReqUserCode(String reqUserCode) {
        this.reqUserCode = reqUserCode;
    }

    public String getReqUserName() {
        return reqUserName;
    }

    public void setReqUserName(String reqUserName) {
        this.reqUserName = reqUserName;
    }

    public String getReqBy() {
        return reqBy;
    }

    public void setReqBy(String reqBy) {
        this.reqBy = reqBy;
    }

    public LocalDateTime getCrtDate() {
        return crtDate;
    }

    public void setCrtDate(LocalDateTime crtDate) {
        this.crtDate = crtDate;
    }

    public String getUptUserCode() {
        return uptUserCode;
    }

    public void setUptUserCode(String uptUserCode) {
        this.uptUserCode = uptUserCode;
    }

    public String getUptUserName() {
        return uptUserName;
    }

    public void setUptUserName(String uptUserName) {
        this.uptUserName = uptUserName;
    }

    public String getUptBy() {
        return uptBy;
    }

    public void setUptBy(String uptBy) {
        this.uptBy = uptBy;
    }

    public LocalDateTime getUptDate() {
        return uptDate;
    }

    public void setUptDate(LocalDateTime uptDate) {
        this.uptDate = uptDate;
    }
}
