package com.eaero.entity;

/**
 * @author zb
 * @date 2023/03/15 015 17:29
 */

import com.baomidou.mybatisplus.annotation.TableName;
@TableName("trace_job_card")
public class TraceJobCard {
    private String id;
    private String workorderno;
    private String reg;
    private String jobcard;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWorkorderno() {
        return workorderno;
    }

    public void setWorkorderno(String workorderno) {
        this.workorderno = workorderno;
    }

    public String getReg() {
        return reg;
    }

    public void setReg(String reg) {
        this.reg = reg;
    }

    public String getJobcard() {
        return jobcard;
    }

    public void setJobcard(String jobcard) {
        this.jobcard = jobcard;
    }
}
