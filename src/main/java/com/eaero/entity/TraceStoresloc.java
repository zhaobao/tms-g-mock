package com.eaero.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
@TableName("trace_storesloc")
public class TraceStoresloc implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 定位
     */
    private String id;

    /**
     * 仓库
     */
    private String warehouse;

    /**
     * 定位
     */
    private String storesloc;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }
    public String getStoresloc() {
        return storesloc;
    }

    public void setStoresloc(String storesloc) {
        this.storesloc = storesloc;
    }

    @Override
    public String toString() {
        return "TraceStoresloc{" +
            "id=" + id +
            ", warehouse=" + warehouse +
            ", storesloc=" + storesloc +
        "}";
    }
}
