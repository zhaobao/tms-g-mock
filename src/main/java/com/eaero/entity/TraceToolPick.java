package com.eaero.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
@TableName("trace_tool_pick")
public class TraceToolPick implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    /**
     * 拣料单号
     */
    private String tpNo;

    /**
     * 创建人
     */
    private String createdUser;

    /**
     *  创建时间
     */
    private String createdDate;

    /**
     * 开放 N/作废 D/发料 U
     */
    private String status;

    /**
     * 更新人
     */
    private String updatedUser;

    /**
     * 更新时间（发料状态用作发料时间）
     */
    private String updatedDate;

    /**
     * 申请单号
     */
    private String reqNo;

    /**
     * 借出人+手机号
     */
    private String reqUser;

    /**
     * 飞机号
     */
    private String tailNo;

    /**
     * 定检级别
     */
    private String visitDesc;

    private LocalDateTime createTime;

    private LocalDateTime modifyTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getTpNo() {
        return tpNo;
    }

    public void setTpNo(String tpNo) {
        this.tpNo = tpNo;
    }
    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }
    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }
    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
    public String getReqNo() {
        return reqNo;
    }

    public void setReqNo(String reqNo) {
        this.reqNo = reqNo;
    }
    public String getReqUser() {
        return reqUser;
    }

    public void setReqUser(String reqUser) {
        this.reqUser = reqUser;
    }
    public String getTailNo() {
        return tailNo;
    }

    public void setTailNo(String tailNo) {
        this.tailNo = tailNo;
    }
    public String getVisitDesc() {
        return visitDesc;
    }

    public void setVisitDesc(String visitDesc) {
        this.visitDesc = visitDesc;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    public LocalDateTime getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(LocalDateTime modifyTime) {
        this.modifyTime = modifyTime;
    }

    @Override
    public String toString() {
        return "TraceToolPick{" +
            "id=" + id +
            ", tpNo=" + tpNo +
            ", createdUser=" + createdUser +
            ", createdDate=" + createdDate +
            ", status=" + status +
            ", updatedUser=" + updatedUser +
            ", updatedDate=" + updatedDate +
            ", reqNo=" + reqNo +
            ", reqUser=" + reqUser +
            ", tailNo=" + tailNo +
            ", visitDesc=" + visitDesc +
            ", createTime=" + createTime +
            ", modifyTime=" + modifyTime +
        "}";
    }
}
