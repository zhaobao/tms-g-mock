package com.eaero.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
@TableName("trace_handover")
public class TraceHandover implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 交班单id
     */
    @TableId(value = "ts_id", type = IdType.AUTO)
    private Integer tsId;

    /**
     * 创建时间
     */
    private String createdDate;

    /**
     * 创建人
     */
    private String createdUser;

    /**
     * 站点
     */
    private String station;

    /**
     * 班次
     */
    private String shift;

    /**
     * 仓库
     */
    private String warehouse;

    /**
     * 交班单备注
     */
    private String hoRemark;

    /**
     * 接班单备注
     */
    private String toRemark;

    /**
     * 状态
     */
    private String status;

    /**
     * 交班日期
     */
    private String hoDate;

    /**
     * 交班人
     */
    private String hoUser;

    /**
     * 接班人
     */
    private String toUser;

    /**
     * 0=作废，1=正常
     */
    @JsonIgnore
    private String cancellation;

    /**
     * 创建时间
     */
    @JsonIgnore
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @JsonIgnore
    private LocalDateTime updateTime;

    public Integer getTsId() {
        return tsId;
    }

    public void setTsId(Integer tsId) {
        this.tsId = tsId;
    }
    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }
    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }
    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }
    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }
    public String getHoRemark() {
        return hoRemark;
    }

    public void setHoRemark(String hoRemark) {
        this.hoRemark = hoRemark;
    }
    public String getToRemark() {
        return toRemark;
    }

    public void setToRemark(String toRemark) {
        this.toRemark = toRemark;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public String getHoDate() {
        return hoDate;
    }

    public void setHoDate(String hoDate) {
        this.hoDate = hoDate;
    }
    public String getHoUser() {
        return hoUser;
    }

    public void setHoUser(String hoUser) {
        this.hoUser = hoUser;
    }
    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public String getCancellation() {
        return cancellation;
    }

    public void setCancellation(String cancellation) {
        this.cancellation = cancellation;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "TraceHandover{" +
            "tsId=" + tsId +
            ", createdDate=" + createdDate +
            ", createdUser=" + createdUser +
            ", station=" + station +
            ", shift=" + shift +
            ", warehouse=" + warehouse +
            ", hoRemark=" + hoRemark +
            ", toRemark=" + toRemark +
            ", status=" + status +
            ", hoDate=" + hoDate +
            ", hoUser=" + hoUser +
            ", toUser=" + toUser +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
        "}";
    }
}
