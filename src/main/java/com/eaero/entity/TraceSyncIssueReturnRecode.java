package com.eaero.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zb
 * @since 2023-04-08
 */
@TableName("trace_sync_issue_return_recode")
public class TraceSyncIssueReturnRecode implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 同步借还记录表
     */
    private String id;

    /**
     * 来源
     */
    private String origin;

    /**
     * 是否配送（Y:配送，N:不配送）
     */
    private String deliver;

    /**
     * 工作者员工号
     */
    private String userCode;

    /**
     * 操作人员工号
     */
    private String empCode;

    /**
     * 是否在线（Y：在线，N：离线）
     */
    private String onlineFlag;

    /**
     * 飞机号
     */
    private String reg;

    /**
     * 工卡指令号
     */
    private String jcWo;

    /**
     * 顺序号
     */
    private String rid;

    /**
     * 件号
     */
    private String partno;

    /**
     * 刻号
     */
    private String trno;

    /**
     * 数量
     */
    private String qty;

    /**
     * 备注
     */
    private String opRemark;

    /**
     * 计划归还日期
     */
    private String planReturn;

    /**
     * 1-借出，2-归还
     */
    private String recodeType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }
    public String getDeliver() {
        return deliver;
    }

    public void setDeliver(String deliver) {
        this.deliver = deliver;
    }
    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }
    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }
    public String getOnlineFlag() {
        return onlineFlag;
    }

    public void setOnlineFlag(String onlineFlag) {
        this.onlineFlag = onlineFlag;
    }
    public String getReg() {
        return reg;
    }

    public void setReg(String reg) {
        this.reg = reg;
    }
    public String getJcWo() {
        return jcWo;
    }

    public void setJcWo(String jcWo) {
        this.jcWo = jcWo;
    }
    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }
    public String getPartno() {
        return partno;
    }

    public void setPartno(String partno) {
        this.partno = partno;
    }
    public String getTrno() {
        return trno;
    }

    public void setTrno(String trno) {
        this.trno = trno;
    }
    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }
    public String getOpRemark() {
        return opRemark;
    }

    public void setOpRemark(String opRemark) {
        this.opRemark = opRemark;
    }
    public String getPlanReturn() {
        return planReturn;
    }

    public void setPlanReturn(String planReturn) {
        this.planReturn = planReturn;
    }
    public String getRecodeType() {
        return recodeType;
    }

    public void setRecodeType(String recodeType) {
        this.recodeType = recodeType;
    }

    @Override
    public String toString() {
        return "TraceSyncIssueReturnRecode{" +
            "id=" + id +
            ", origin=" + origin +
            ", deliver=" + deliver +
            ", userCode=" + userCode +
            ", empCode=" + empCode +
            ", onlineFlag=" + onlineFlag +
            ", reg=" + reg +
            ", jcWo=" + jcWo +
            ", rid=" + rid +
            ", partno=" + partno +
            ", trno=" + trno +
            ", qty=" + qty +
            ", opRemark=" + opRemark +
            ", planReturn=" + planReturn +
            ", recodeType=" + recodeType +
        "}";
    }
}
