package com.eaero.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author luofeng
 * @since 2023-07-07
 */
@TableName("trace_exit_record_detail")
public class TraceExitRecordDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    /**
     * 退料明细id
     */
    private String trlId;

    private String exitRecordId;

    /**
     *  退料单号
     */
    private String returnPnum;

    /**
     * 工具顺序号
     */
    private String rid;

    /**
     * 工具件号
     */
    private String partno;

    /**
     * 工具刻号
     */
    private String serialno;

    /**
     * 退库工具备注
     */
    private String remark;

    /**
     * 工卡指令号
     */
    private String workorderno;

    /**
     * 退料数量
     */
    private String num;

    /**
     * 发料单号
     */
    private String tpNo;

    /**
     * 工具发料 RID
     */
    private String issueRid;

    /**
     * 故障状态 空值/故障信息
     */
    private String tlFlag;

    /**
     * 单位
     */
    private String unitofmeas;

    /**
     * 条形码
     */
    private String barcode;

    /**
     * RFID 号
     */
    private String rfidId;

    /**
     * 父件号
     */
    private String parentPartno;

    /**
     * 父刻号
     */
    private String parentTrno;

    /**
     * 父件顺序号
     */
    private String parent;

    /**
     * 仓库描述
     */
    private String whseDesc;

    /**
     * 站点
     */
    private String station;

    /**
     * 真实件号
     */
    private String alternatePartno;

    /**
     * 定位
     */
    private String storesloc;

    /**
     * 仓库
     */
    private String warehouse;

    /**
     * 子件数量
     */
    private String childNum;

    /**
     * 计量号
     */
    private String measuringNo;

    /**
     *  替代件号
     */
    private String standByPn;

    /**
     * 件号描述
     */
    private String partdescr;

    /**
     * 工具类型
     */
    private String catalogueType;

    /**
     * 工具状态（借出/在库）
     */
    private String tlStatus;

    private LocalDateTime createTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getTrlId() {
        return trlId;
    }

    public void setTrlId(String trlId) {
        this.trlId = trlId;
    }
    public String getExitRecordId() {
        return exitRecordId;
    }

    public void setExitRecordId(String exitRecordId) {
        this.exitRecordId = exitRecordId;
    }
    public String getReturnPnum() {
        return returnPnum;
    }

    public void setReturnPnum(String returnPnum) {
        this.returnPnum = returnPnum;
    }
    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }
    public String getPartno() {
        return partno;
    }

    public void setPartno(String partno) {
        this.partno = partno;
    }
    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public String getWorkorderno() {
        return workorderno;
    }

    public void setWorkorderno(String workorderno) {
        this.workorderno = workorderno;
    }
    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }
    public String getTpNo() {
        return tpNo;
    }

    public void setTpNo(String tpNo) {
        this.tpNo = tpNo;
    }
    public String getIssueRid() {
        return issueRid;
    }

    public void setIssueRid(String issueRid) {
        this.issueRid = issueRid;
    }
    public String getTlFlag() {
        return tlFlag;
    }

    public void setTlFlag(String tlFlag) {
        this.tlFlag = tlFlag;
    }
    public String getUnitofmeas() {
        return unitofmeas;
    }

    public void setUnitofmeas(String unitofmeas) {
        this.unitofmeas = unitofmeas;
    }
    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
    public String getRfidId() {
        return rfidId;
    }

    public void setRfidId(String rfidId) {
        this.rfidId = rfidId;
    }
    public String getParentPartno() {
        return parentPartno;
    }

    public void setParentPartno(String parentPartno) {
        this.parentPartno = parentPartno;
    }
    public String getParentTrno() {
        return parentTrno;
    }

    public void setParentTrno(String parentTrno) {
        this.parentTrno = parentTrno;
    }
    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }
    public String getWhseDesc() {
        return whseDesc;
    }

    public void setWhseDesc(String whseDesc) {
        this.whseDesc = whseDesc;
    }
    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }
    public String getAlternatePartno() {
        return alternatePartno;
    }

    public void setAlternatePartno(String alternatePartno) {
        this.alternatePartno = alternatePartno;
    }
    public String getStoresloc() {
        return storesloc;
    }

    public void setStoresloc(String storesloc) {
        this.storesloc = storesloc;
    }
    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }
    public String getChildNum() {
        return childNum;
    }

    public void setChildNum(String childNum) {
        this.childNum = childNum;
    }
    public String getMeasuringNo() {
        return measuringNo;
    }

    public void setMeasuringNo(String measuringNo) {
        this.measuringNo = measuringNo;
    }
    public String getStandByPn() {
        return standByPn;
    }

    public void setStandByPn(String standByPn) {
        this.standByPn = standByPn;
    }
    public String getPartdescr() {
        return partdescr;
    }

    public void setPartdescr(String partdescr) {
        this.partdescr = partdescr;
    }
    public String getCatalogueType() {
        return catalogueType;
    }

    public void setCatalogueType(String catalogueType) {
        this.catalogueType = catalogueType;
    }
    public String getTlStatus() {
        return tlStatus;
    }

    public void setTlStatus(String tlStatus) {
        this.tlStatus = tlStatus;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "TraceExitRecordDetail{" +
            "id=" + id +
            ", trlId=" + trlId +
            ", exitRecordId=" + exitRecordId +
            ", returnPnum=" + returnPnum +
            ", rid=" + rid +
            ", partno=" + partno +
            ", serialno=" + serialno +
            ", remark=" + remark +
            ", workorderno=" + workorderno +
            ", num=" + num +
            ", tpNo=" + tpNo +
            ", issueRid=" + issueRid +
            ", tlFlag=" + tlFlag +
            ", unitofmeas=" + unitofmeas +
            ", barcode=" + barcode +
            ", rfidId=" + rfidId +
            ", parentPartno=" + parentPartno +
            ", parentTrno=" + parentTrno +
            ", parent=" + parent +
            ", whseDesc=" + whseDesc +
            ", station=" + station +
            ", alternatePartno=" + alternatePartno +
            ", storesloc=" + storesloc +
            ", warehouse=" + warehouse +
            ", childNum=" + childNum +
            ", measuringNo=" + measuringNo +
            ", standByPn=" + standByPn +
            ", partdescr=" + partdescr +
            ", catalogueType=" + catalogueType +
            ", tlStatus=" + tlStatus +
            ", createTime=" + createTime +
        "}";
    }
}
