package com.eaero.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

/**
 * 
 * @TableName trace_tool_stock_detail
 */
@TableName(value ="trace_tool_stock_detail")
public class TraceToolStockDetail implements Serializable {
    /**
     * 盘点明细 ID
     */
    @TableId(type = IdType.AUTO)
    private Integer tsdId;

    /**
     * 盘点单 ID
     */
    private Integer tsId;

    /**
     * 盘点单号
     */
    private String stockPnum;

    /**
     * 仓库
     */
    private String warehouse;

    /**
     * 工具件号
     */
    private String partno;

    /**
     * 工具刻号
     */
    private String serialno;

    /**
     * 定位
     */
    private String storesloc;

    /**
     * 库存状态
     */
    private String checkStatus;

    /**
     * 盘点时间
     */
    private String checkDate;

    /**
     * 盘点人
     */
    private String checkUser;

    /**
     * 明细盘点备注
     */
    private String checkRemark;

    /**
     * 盘点状态
     */
    private String status;

    /**
     * 故障状态（空值/故障信息）
     */
    private String tlFlag;

    /**
     * 图片（Y/N）
     */
    private String imgFlag;

    /**
     * 工具顺序
     */
    private String rid;

    /**
     * 单位
     */
    private String unitofmeas;

    /**
     * 条形码
     */
    private String barcode;

    /**
     * RFID 号
     */
    private String rfidId;

    /**
     * 父件号
     */
    private String parentPartno;

    /**
     * 父刻号
     */
    private String parentTrno;

    /**
     * 父件顺序号
     */
    private String parent;

    /**
     * 仓库描述
     */
    private String whseDesc;

    /**
     * 站点
     */
    private String station;

    /**
     * 真实件号
     */
    private String alternatePartno;

    /**
     * 子件数量
     */
    private String childNum;

    /**
     * 计量号
     */
    private String measuringNo;

    /**
     * 替代件号
     */
    private String standByPn;

    /**
     * 件号描述
     */
    private String partdescr;

    /**
     * 工具类型
     */
    private String catalogueType;

    /**
     * 工具状态（借出/在库）
     */
    private String tlStatus;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 盘点明细 ID
     */
    public Integer getTsdId() {
        return tsdId;
    }

    /**
     * 盘点明细 ID
     */
    public void setTsdId(Integer tsdId) {
        this.tsdId = tsdId;
    }

    /**
     * 盘点单 ID
     */
    public Integer getTsId() {
        return tsId;
    }

    /**
     * 盘点单 ID
     */
    public void setTsId(Integer tsId) {
        this.tsId = tsId;
    }

    /**
     * 盘点单号
     */
    public String getStockPnum() {
        return stockPnum;
    }

    public void setStockPnum(String stockPnum) {
        this.stockPnum = stockPnum;
    }

    /**
     * 仓库
     */
    public String getWarehouse() {
        return warehouse;
    }

    /**
     * 仓库
     */
    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    /**
     * 工具件号
     */
    public String getPartno() {
        return partno;
    }

    /**
     * 工具件号
     */
    public void setPartno(String partno) {
        this.partno = partno;
    }

    /**
     * 工具刻号
     */
    public String getSerialno() {
        return serialno;
    }

    /**
     * 工具刻号
     */
    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    /**
     * 定位
     */
    public String getStoresloc() {
        return storesloc;
    }

    /**
     * 定位
     */
    public void setStoresloc(String storesloc) {
        this.storesloc = storesloc;
    }

    /**
     * 库存状态
     */
    public String getCheckStatus() {
        return checkStatus;
    }

    /**
     * 库存状态
     */
    public void setCheckStatus(String checkStatus) {
        this.checkStatus = checkStatus;
    }

    /**
     * 盘点时间
     */
    public String getCheckDate() {
        return checkDate;
    }

    /**
     * 盘点时间
     */
    public void setCheckDate(String checkDate) {
        this.checkDate = checkDate;
    }

    /**
     * 盘点人
     */
    public String getCheckUser() {
        return checkUser;
    }

    /**
     * 盘点人
     */
    public void setCheckUser(String checkUser) {
        this.checkUser = checkUser;
    }

    public String getCheckRemark() {
        return checkRemark;
    }

    public void setCheckRemark(String checkRemark) {
        this.checkRemark = checkRemark;
    }

    /**
     * 盘点状态
     */
    public String getStatus() {
        return status;
    }

    /**
     * 盘点状态
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 故障状态（空值/故障信息）
     */
    public String getTlFlag() {
        return tlFlag;
    }

    /**
     * 故障状态（空值/故障信息）
     */
    public void setTlFlag(String tlFlag) {
        this.tlFlag = tlFlag;
    }

    /**
     * 图片（Y/N）
     */
    public String getImgFlag() {
        return imgFlag;
    }

    /**
     * 图片（Y/N）
     */
    public void setImgFlag(String imgFlag) {
        this.imgFlag = imgFlag;
    }

    /**
     * 工具顺序
     */
    public String getRid() {
        return rid;
    }

    /**
     * 工具顺序
     */
    public void setRid(String rid) {
        this.rid = rid;
    }

    /**
     * 单位
     */
    public String getUnitofmeas() {
        return unitofmeas;
    }

    /**
     * 单位
     */
    public void setUnitofmeas(String unitofmeas) {
        this.unitofmeas = unitofmeas;
    }

    /**
     * 条形码
     */
    public String getBarcode() {
        return barcode;
    }

    /**
     * 条形码
     */
    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    /**
     * RFID 号
     */
    public String getRfidId() {
        return rfidId;
    }

    /**
     * RFID 号
     */
    public void setRfidId(String rfidId) {
        this.rfidId = rfidId;
    }

    /**
     * 父件号
     */
    public String getParentPartno() {
        return parentPartno;
    }

    /**
     * 父件号
     */
    public void setParentPartno(String parentPartno) {
        this.parentPartno = parentPartno;
    }

    /**
     * 父刻号
     */
    public String getParentTrno() {
        return parentTrno;
    }

    /**
     * 父刻号
     */
    public void setParentTrno(String parentTrno) {
        this.parentTrno = parentTrno;
    }

    /**
     * 父件顺序号
     */
    public String getParent() {
        return parent;
    }

    /**
     * 父件顺序号
     */
    public void setParent(String parent) {
        this.parent = parent;
    }

    /**
     * 仓库描述
     */
    public String getWhseDesc() {
        return whseDesc;
    }

    /**
     * 仓库描述
     */
    public void setWhseDesc(String whseDesc) {
        this.whseDesc = whseDesc;
    }

    /**
     * 站点
     */
    public String getStation() {
        return station;
    }

    /**
     * 站点
     */
    public void setStation(String station) {
        this.station = station;
    }

    /**
     * 真实件号
     */
    public String getAlternatePartno() {
        return alternatePartno;
    }

    /**
     * 真实件号
     */
    public void setAlternatePartno(String alternatePartno) {
        this.alternatePartno = alternatePartno;
    }

    /**
     * 子件数量
     */
    public String getChildNum() {
        return childNum;
    }

    /**
     * 子件数量
     */
    public void setChildNum(String childNum) {
        this.childNum = childNum;
    }

    /**
     * 计量号
     */
    public String getMeasuringNo() {
        return measuringNo;
    }

    /**
     * 计量号
     */
    public void setMeasuringNo(String measuringNo) {
        this.measuringNo = measuringNo;
    }

    /**
     * 替代件号
     */
    public String getStandByPn() {
        return standByPn;
    }

    /**
     * 替代件号
     */
    public void setStandByPn(String standByPn) {
        this.standByPn = standByPn;
    }

    /**
     * 件号描述
     */
    public String getPartdescr() {
        return partdescr;
    }

    /**
     * 件号描述
     */
    public void setPartdescr(String partdescr) {
        this.partdescr = partdescr;
    }

    /**
     * 工具类型
     */
    public String getCatalogueType() {
        return catalogueType;
    }

    /**
     * 工具类型
     */
    public void setCatalogueType(String catalogueType) {
        this.catalogueType = catalogueType;
    }

    /**
     * 工具状态（借出/在库）
     */
    public String getTlStatus() {
        return tlStatus;
    }

    /**
     * 工具状态（借出/在库）
     */
    public void setTlStatus(String tlStatus) {
        this.tlStatus = tlStatus;
    }


}