package com.eaero.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author zb
 * @since 2023-03-14
 */
@TableName("trace_tool_deliver_sign")
public class TraceToolDeliverSign implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 配送流水
     */
    private String id;

    /**
     * 配送明细id
     */
    private String tddId;

    /**
     * 配送单号
     */
    private String deliverCode;

    /**
     * 更新人
     */
    private String updateUser;

    /**
     * 更新日期
     */
    private String updateDate;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getTddId() {
        return tddId;
    }

    public void setTddId(String tddId) {
        this.tddId = tddId;
    }
    public String getDeliverCode() {
        return deliverCode;
    }

    public void setDeliverCode(String deliverCode) {
        this.deliverCode = deliverCode;
    }
    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }
    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "TraceToolDeliverSign{" +
            "id=" + id +
            ", tddId=" + tddId +
            ", deliverCode=" + deliverCode +
            ", updateUser=" + updateUser +
            ", updateDate=" + updateDate +
            ", createTime=" + createTime +
        "}";
    }
}
