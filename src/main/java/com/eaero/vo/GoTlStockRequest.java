package com.eaero.vo;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author zb
 * @date 2023/03/18 018 13:42
 * userCode": "007632", "origin": "A", "warehouse": "N2CK", "remark": "特殊盘点", "newStock": [
 * 64 / 70
 * { "storesloc": "102C"
 * }, { "storesloc": "立柜"
 * }
 * ]
 * }
 */
@Data
public class GoTlStockRequest {
    @NotBlank(message = "员工号，必填")
    private String userCode;
    @NotBlank(message = "来源，必填")
    private String origin;
    @NotBlank(message = "仓库，必填")
    private String warehouse;
    private String remark;
    @Valid
    private List<Storesloc> newStock;
}
