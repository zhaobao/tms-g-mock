package com.eaero.vo;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author zb
 * @date 2023/03/18 018 13:46
 */
@Data
public class CancelTlStockRequest {
    @NotBlank(message = "员工号，必填")
    private String userCode;
    @Valid
    private List<StockPnumVo> cancelStock;
}
