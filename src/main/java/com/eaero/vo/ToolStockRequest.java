package com.eaero.vo;

import lombok.Data;

/**
 * Description:
 *
 * @author Ye Zhihong
 * @date 2023/3/17 16:54
 */
@Data
public class ToolStockRequest {

    /**
     * 盘点单号
     */
    private String stockPnum;

    /**
     * 仓库
     */
    private String warehouse;

    /**
     * 创建者
     */
    private String createdUser;

    /**
     * 创建时间（yyyy-mm-dd）
     */
    private String created;

    /**
     * 状态（开放/盘点中/关闭/作废）
     */
    private String status;

    /**
     * 盘点单来源（P:PC;A:ATM;H:手持设备;D:工作台）
     */
    private String origin;
}
