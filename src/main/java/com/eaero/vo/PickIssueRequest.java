package com.eaero.vo;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * @author luofeng
 * @create 2023/3/15 16:14
 */
@Data
public class PickIssueRequest {
    @Pattern(regexp = "^[1|2|3|4]$", message = "终端类型错误")
    @NotBlank(message = "终端类型不能来空")
    private String origin;
    @Pattern(regexp = "^[Y|N]$", message = "配送类型错误")
    @NotBlank(message = "配送类型不能为空")
    private String deliver;
    @NotBlank(message = "操作人员工号不能为空")
    private String empCode;
    @Valid
    private List<PickIssue> toolPickIssue;
}
