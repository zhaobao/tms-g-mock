package com.eaero.vo;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author luofeng
 * @create 2023/3/15 16:51
 */
@Data
public class UpdateToolReqdetailPnRequest {
    @NotBlank(message = "员工号必填")
    private String userCode;
    @Valid
    private List<UpdateToolReqdetailPn> updatePn;
}
