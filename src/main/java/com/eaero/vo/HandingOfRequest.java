package com.eaero.vo;

import lombok.Data;

/**
 * Description:
 *
 * @author Ye Zhihong
 * @date 2023/3/14 14:41
 */
@Data
public class HandingOfRequest {

    /**
     * 仓库 必填
     */
    private String warhouse;

    /**
     * 交班人 必填
     */
    private String userCode;

    /**
     * 交班日期（YYYY-MM-DD），必填
     */
    private String hoDate;

    /**
     * 班次（早班/晚班），必填
     */
    private String shift;

    /**
     * 交班单备注
     */
    private String hoRemark;

    /**
     * 来源，必填（P:PC;A:ATM;H:手持设备;D:工作台）
     */
    private String origin;
}
