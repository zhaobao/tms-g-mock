package com.eaero.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author luofeng
 * @create 2023/3/15 16:15
 */
@Data
public class PickIssue {
    @NotBlank(message = "建议批号ID不能为空")
    private String tpdlId;
    @NotBlank(message = "数量必填")
    private String qty;
    private String opRemark;
    private String planReturn;
}
