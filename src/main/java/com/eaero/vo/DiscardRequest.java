package com.eaero.vo;

import lombok.Data;

import java.util.List;

/**
 * @author luofeng
 * @create 2023/3/14 15:44
 */
@Data
public class DiscardRequest {
    private String userCode;
    private String origin;
    private List<DiscardVo> discardList;
}
