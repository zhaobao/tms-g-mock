package com.eaero.vo;

import lombok.Data;

/**
 * @author luofeng
 * @create 2023/3/16 14:03
 */
@Data
public class GetToolReqdetailSumRequest {

    private String tailNo;

    private String rcvBy;

    private String reqDate;

    private String whsType;

}
