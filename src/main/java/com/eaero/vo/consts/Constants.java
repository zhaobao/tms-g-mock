package com.eaero.vo.consts;

/**
 * @author luofeng
 * @create 2023/3/14 13:30
 */
public class Constants {

    public static final class ResponseCode {
        public static final String SUCCESS = "0";// 成功
        public static final String ERROR = "1"; // 失败
    }

    public static final class STATE {
        public static final String DELETE = "-1"; // 删除
        public static final String NORMAL = "0"; // 正常
    }
}
