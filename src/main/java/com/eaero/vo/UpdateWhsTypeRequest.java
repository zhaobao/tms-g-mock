package com.eaero.vo;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author luofeng
 * @create 2023/3/15 17:35
 */
@Data
public class UpdateWhsTypeRequest {
    @NotBlank(message = "员工号必填")
    private String userCode;
    @NotBlank(message = "仓库必填")
    private String whsType;
    @Valid
    private List<UpdateWhsType> updateWhs;
}
