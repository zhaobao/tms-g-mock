package com.eaero.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author luofeng
 * @create 2023/3/14 16:40
 */
@Data
public class CancelToolPick {
    @NotBlank(message = "拣料单号必填")
    private String tpNo;
}
