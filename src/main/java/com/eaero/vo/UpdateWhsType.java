package com.eaero.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author luofeng
 * @create 2023/3/15 17:36
 */
@Data
public class UpdateWhsType {
    @NotBlank(message = "申请单明细id必填")
    private String rid;
}
