package com.eaero.vo;

import lombok.Data;

/**
 * Description:
 *
 * @author Ye Zhihong
 * @date 2023/3/14 14:18
 */
@Data
public class HandoverInfoRequest {

    /**
     * 仓库
     */
    private String warhouse;

    /**
     * 交班日期（yyyy-mm-dd）
     */
    private String hoDate;

    /**
     * 班次
     */
    private String shift;

    /**
     * 状态（交班/接班/作废/关闭）
     */
    private String status;
}
