package com.eaero.vo;

/**
 * @author zzj
 * @date 2023/3/14 17:00
 * @description
 */
public class ToolDeliverDetailRequest {

    /**
     * 配送单号，支持模糊查询
     */
    private String deliverCode;
    /**
     * 发料人，支持模糊查询
     */
    private String issueUser;
    /**
     * 发料时间（yyyy-mm-dd）
     */
    private String issueDate;
    /**
     * 创建人，支持模糊查询
     */
    private String createUser;
    /**
     * 状态(待配送、配送中、工作者签收、回收中、关闭)
     */
    private String status;
    /**
     * 工卡指令号，支持模糊查询
     */
    private String workorderno;
    /**
     * 飞机号，支持模糊查询
     */
    private String tailNo;
    /**
     * 定检级别，支持模糊查询
     */
    private String visitDesc;
    /**
     * 件号，支持模糊查询
     */
    private String partno;
    /**
     * 批号，支持模糊查询
     */
    private String serialno;
    /**
     * 配送人，支持模糊查询
     */
    private String deliverUser;
    /**
     * 配送时间
     */
    private String deliverDate;
    /**
     * 更新时间
     */
    private String updateDate;
    /**
     * 更新人，支持模糊查询
     */
    private String updateUser;
    /**
     * 发料仓库
     */
    private String warehouse;

    public String getDeliverCode() {
        return deliverCode;
    }

    public void setDeliverCode(String deliverCode) {
        this.deliverCode = deliverCode;
    }

    public String getIssueUser() {
        return issueUser;
    }

    public void setIssueUser(String issueUser) {
        this.issueUser = issueUser;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWorkorderno() {
        return workorderno;
    }

    public void setWorkorderno(String workorderno) {
        this.workorderno = workorderno;
    }

    public String getTailNo() {
        return tailNo;
    }

    public void setTailNo(String tailNo) {
        this.tailNo = tailNo;
    }

    public String getVisitDesc() {
        return visitDesc;
    }

    public void setVisitDesc(String visitDesc) {
        this.visitDesc = visitDesc;
    }

    public String getPartno() {
        return partno;
    }

    public void setPartno(String partno) {
        this.partno = partno;
    }

    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    public String getDeliverUser() {
        return deliverUser;
    }

    public void setDeliverUser(String deliverUser) {
        this.deliverUser = deliverUser;
    }

    public String getDeliverDate() {
        return deliverDate;
    }

    public void setDeliverDate(String deliverDate) {
        this.deliverDate = deliverDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }
}
