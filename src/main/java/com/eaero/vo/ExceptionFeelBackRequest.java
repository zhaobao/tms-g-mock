package com.eaero.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * @author luofeng
 * @create 2023/3/14 15:06
 */
@Data
public class ExceptionFeelBackRequest {

    private String fbBy;
    private String origin;
    @JsonProperty("ExceptionFeelBack")
    private List<ExceptionFeelBack> ExceptionFeelBack;
}
