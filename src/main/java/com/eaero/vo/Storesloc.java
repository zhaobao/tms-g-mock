package com.eaero.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author zb
 * @date 2023/03/18 018 13:44
 */
@Data
public class Storesloc {
    @NotBlank(message = "定位，必填")
    private String storesloc;
}
