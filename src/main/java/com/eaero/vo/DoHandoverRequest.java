package com.eaero.vo;

import lombok.Data;

import java.util.List;

/**
 * Description:
 *
 * @author Ye Zhihong
 * @date 2023/3/14 17:05
 */
@Data
public class DoHandoverRequest {
    private String userCode;
    private String toRemark;
    private List<DoHandoverItem> doHandoverDtl;
}
