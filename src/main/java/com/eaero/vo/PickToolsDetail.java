package com.eaero.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author zzj
 * @date 2023/3/15 11:16
 * @description
 */
@Data
public class PickToolsDetail {

    /**
     * 申请单明细id
     */
    @NotBlank(message = "申请单明细id必填")
    private String rid;

}
