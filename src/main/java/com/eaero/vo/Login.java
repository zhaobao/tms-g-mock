package com.eaero.vo;

/**
 * @author luofeng
 * @create 2023/3/14 15:13
 */
public class Login {
    private String userCode;
    private String userPassword;

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
}
