package com.eaero.vo;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author luofeng
 * @create 2023/3/17 11:19
 */
@Data
public class DeleteToolPickDetailListRequest {
    @NotBlank(message = "员工号必填")
    private String userCode;
    @Valid
    private List<DeleteToolPickDetailList> deleteToolPickDetailList;
}
