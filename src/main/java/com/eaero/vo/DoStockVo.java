package com.eaero.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author zb
 * @date 2023/03/18 018 13:48
 */
@Data
public class DoStockVo {
    @NotBlank(message = " 盘点明细 ID，必填")
    private String tsdId;
    @NotBlank(message = " 盘点状态 ID，必填")
    private String status;
    private String checkRemark;
}
