package com.eaero.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author luofeng
 * @create 2023/3/16 14:48
 */
@Data
public class GetImpageByPartnoVo {
    @NotBlank(message = "件号必填")
    private String partno;
    @NotBlank(message = "刻号必填")
    private String trno;

    private String imgName;

}
