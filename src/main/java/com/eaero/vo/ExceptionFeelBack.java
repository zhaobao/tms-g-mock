package com.eaero.vo;

import lombok.Data;

/**
 * @author luofeng
 * @create 2023/3/14 15:07
 */
@Data
public class ExceptionFeelBack {

    private String tlPn;
    private String tlTr;
    private String fbDesc;
    private String normalDamage;
}
