package com.eaero.vo;

import java.time.LocalDateTime;

/**
 * @author luofeng
 * @create 2023/3/31 12:16
 */
public class CardLoginResponse {
    /**
     * 员工id
     */
    private String id;

    /**
     * 员工号
     */
    private String userCode;

    /**
     * 员工名称
     */
    private String userName;

    /**
     * 员工密码
     */
    private String userPwd;

    /**
     * 员工电话号
     */
    private String comMobile;

    /**
     * 所在部门
     */
    private String userDept;

    /**
     * 员工考勤卡id
     */
    private String snrBox;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime modifyTime;

    private String empCode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public String getComMobile() {
        return comMobile;
    }

    public void setComMobile(String comMobile) {
        this.comMobile = comMobile;
    }

    public String getUserDept() {
        return userDept;
    }

    public void setUserDept(String userDept) {
        this.userDept = userDept;
    }

    public String getSnrBox() {
        return snrBox;
    }

    public void setSnrBox(String snrBox) {
        this.snrBox = snrBox;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(LocalDateTime modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }
}
