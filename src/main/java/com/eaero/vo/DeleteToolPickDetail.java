package com.eaero.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author luofeng
 * @create 2023/3/14 17:46
 */
@Data
public class DeleteToolPickDetail {
    @NotBlank(message = "拣料单明细id必填")
    private String tpdId;
}
