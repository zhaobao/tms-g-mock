package com.eaero.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author luofeng
 * @create 2023/3/17 11:19
 */
@Data
public class DeleteToolPickDetailList {
    @NotBlank(message = "申请单号必填")
    private String tpdlId;
}
