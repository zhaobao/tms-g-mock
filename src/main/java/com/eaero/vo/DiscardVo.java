package com.eaero.vo;

import lombok.Data;

/**
 * @author luofeng
 * @create 2023/3/14 15:44
 */
@Data
public class DiscardVo {
    private String partno;
    private String trno;
    private String remark;
}
