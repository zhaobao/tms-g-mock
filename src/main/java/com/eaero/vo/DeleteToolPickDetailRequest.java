package com.eaero.vo;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author luofeng
 * @create 2023/3/14 17:46
 */
@Data
public class DeleteToolPickDetailRequest {
    @NotBlank(message = "员工单号必填")
    private String userCode;
    @Valid
    private List<DeleteToolPickDetail> deletePickdetail;
}
