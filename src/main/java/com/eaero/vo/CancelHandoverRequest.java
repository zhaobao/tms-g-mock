package com.eaero.vo;

import lombok.Data;

import java.util.List;

/**
 * Description:
 *
 * @author Ye Zhihong
 * @date 2023/3/14 16:30
 */
@Data
public class CancelHandoverRequest {
    private String userCode;
    private List<CancelHandoverDtlItem> cancelHandoverDtl;
}