package com.eaero.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author luofeng
 * @create 2023/3/15 16:52
 */
@Data
public class UpdateToolReqdetailPn {
    @NotBlank(message = "申请单明细id必填")
    private String rid;
    private String pn;
    private String qty;
}
