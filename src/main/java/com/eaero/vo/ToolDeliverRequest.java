package com.eaero.vo;

/**
 * @author zzj
 * @date 2023/3/14 15:40
 * @description
 */
public class ToolDeliverRequest {

    /**
     * 配送单号，支持模糊查询
     */
    private String deliverCode;
    /**
     * 发料单号，支持模糊查询
     */
    private String tpNo;
    /**
     *  发料人，支持模糊查询
     */
    private String createUser;
    /**
     *  发料时间,支持模糊查询
     */
    private String createDate;
    /**
     * 状态
     */
    private String status;
    /**
     * 飞机号
     */
    private String tailNo;
    /**
     * 发料仓库
     */
    private String warehouse;

    public String getDeliverCode() {
        return deliverCode;
    }

    public void setDeliverCode(String deliverCode) {
        this.deliverCode = deliverCode;
    }

    public String getTpNo() {
        return tpNo;
    }

    public void setTpNo(String tpNo) {
        this.tpNo = tpNo;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTailNo() {
        return tailNo;
    }

    public void setTailNo(String tailNo) {
        this.tailNo = tailNo;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }
}
