package com.eaero.vo;

import lombok.Data;

/**
 * Description:
 *
 * @author Ye Zhihong
 * @date 2023/3/14 16:51
 */
@Data
public class CancelHandoverDtlItem {
    private String tsId;
}
