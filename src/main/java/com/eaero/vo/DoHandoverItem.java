package com.eaero.vo;

import lombok.Data;

/**
 * Description:
 *
 * @author Ye Zhihong
 * @date 2023/3/14 17:06
 */
@Data
public class DoHandoverItem {
    private String tsdId;
    private String whseCheck;
    private String remark;
}
