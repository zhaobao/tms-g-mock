package com.eaero.vo;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author zzj
 * @date 2023/3/15 11:15
 * @description
 */
@Data
public class PickToolsRequest {

    /**
     * 员工号 必填
     */
    @NotBlank(message = "员工号必填")
    private String userCode;
    /**
     * 来源，必填（P:PC;A:ATM;H:手持设备;D:工作台）
     */
    @NotBlank(message = "来源必填")
    private String origin;
    /**
     * 申请单明细id 必填
     */
    @Valid
    private List<PickToolsDetail> newPick;

}
