package com.eaero.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author luofeng
 * @create 2023/3/15 9:38
 */
@Data
public class UpdateToolPickdetailPn {
    @NotBlank(message = "拣料单明细id必填")
    private String tpdId;
    @NotBlank(message = "件号必填")
    private String pn;
}
