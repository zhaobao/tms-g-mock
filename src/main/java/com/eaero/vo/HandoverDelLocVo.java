package com.eaero.vo;

import lombok.Data;

/**
 * Description:
 *
 * @author Ye Zhihong
 * @date 2023/3/15 16:54
 */
@Data
public class HandoverDelLocVo {
    private String checkReq;
    private String hoRemark;
    private String whseIssue;
    private String toRemark;
    private String remark;
    private String tsdId;
    private String whseIn;
    private String storesloc;
    private String whseCheck;
    private String warehouse;
}
