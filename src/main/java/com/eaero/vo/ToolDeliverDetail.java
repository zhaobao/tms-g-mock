package com.eaero.vo;


/**
 * @author zzj
 * @date 2023/3/14 17:12
 * @description
 */
public class ToolDeliverDetail {

    /**
     * 工具配送明细tddId
     */
    private String tddId;

    /**
     * 配送单号
     */
    private String deliverCode;

    /**
     * 工卡指令号
     */
    private String workorderno;

    /**
     * 工卡号
     */
    private String jobcard;

    /**
     * 件号
     */
    private String partno;

    /**
     * 批号
     */
    private String serialno;

    /**
     * 件号描述
     */
    private String partdescr;

    /**
     * 工具类型
     */
    private String catalogueType;

    /**
     * 飞机号
     */
    private String tailNo;

    /**
     * 定检级别
     */
    private String visitDesc;

    /**
     * 发料人
     */
    private String issueUser;

    /**
     * 发料时间（yyyy-mm-dd）
     */
    private String issueDate;

    /**
     * 仓库
     */
    private String warehouse;

    /**
     * 更新人
     */
    private String updateUser;

    /**
     * 更新时间
     */
    private String updateDate;

    /**
     * 借用人
     */
    private String reqBy;

    /**
     * 借用人手机号
     */
    private String comMobile;

    /**
     * 状态(待配送、配送中、工作者签收、回收中、关闭)
     */
    private String status;

    /**
     * 配送人
     */
    private String deliverUser;

    /**
     * 配送时间
     */
    private String deliverDate;

    /**
     * 故障状态（空值/故障信息）
     */
    private String tlFlag;

    /**
     * 图片（Y/N）
     */
    private String imgFlag;

    /**
     * 工具顺序号
     */
    private String rid;

    /**
     * 单位
     */
    private String unitofmeas;

    /**
     * 条形码
     */
    private String barcode;

    /**
     * RFID号
     */
    private String rfidId;

    /**
     * 父件号
     */
    private String parentPartno;

    /**
     * 父刻号
     */
    private String parentTrno;

    /**
     * 父件顺序号
     */
    private String parentId;

    /**
     * 仓库描述,
     */
    private String whseDesc;

    /**
     * 站点
     */
    private String station;

    /**
     * 真实件号
     */
    private String alternatePartno;

    /**
     * 定位
     */
    private String storesloc;

    /**
     * 子件数量
     */
    private String childNum;

    /**
     * 计量号
     */
    private String measuringNo;

    /**
     * 替代件号
     */
    private String standByPn;

    /**
     * 工具状态（借出/在库）
     */
    private String tlStatus;

    private String reqNo;

    private String empCode;

    private String createDate;

    private String tpNo;

    private String vid;

    private String opDate;

    private String issueNum;

    private String tdId;

    /**
     * 创建人
     */
    private String createUser;

    private String mechanic;

    private String issueRid;

    public String getTddId() {
        return tddId;
    }

    public void setTddId(String tddId) {
        this.tddId = tddId;
    }

    public String getDeliverCode() {
        return deliverCode;
    }

    public void setDeliverCode(String deliverCode) {
        this.deliverCode = deliverCode;
    }

    public String getWorkorderno() {
        return workorderno;
    }

    public void setWorkorderno(String workorderno) {
        this.workorderno = workorderno;
    }

    public String getJobcard() {
        return jobcard;
    }

    public void setJobcard(String jobcard) {
        this.jobcard = jobcard;
    }

    public String getPartno() {
        return partno;
    }

    public void setPartno(String partno) {
        this.partno = partno;
    }

    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    public String getPartdescr() {
        return partdescr;
    }

    public void setPartdescr(String partdescr) {
        this.partdescr = partdescr;
    }

    public String getCatalogueType() {
        return catalogueType;
    }

    public void setCatalogueType(String catalogueType) {
        this.catalogueType = catalogueType;
    }

    public String getTailNo() {
        return tailNo;
    }

    public void setTailNo(String tailNo) {
        this.tailNo = tailNo;
    }

    public String getVisitDesc() {
        return visitDesc;
    }

    public void setVisitDesc(String visitDesc) {
        this.visitDesc = visitDesc;
    }

    public String getIssueUser() {
        return issueUser;
    }

    public void setIssueUser(String issueUser) {
        this.issueUser = issueUser;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getReqBy() {
        return reqBy;
    }

    public void setReqBy(String reqBy) {
        this.reqBy = reqBy;
    }

    public String getComMobile() {
        return comMobile;
    }

    public void setComMobile(String comMobile) {
        this.comMobile = comMobile;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeliverUser() {
        return deliverUser;
    }

    public void setDeliverUser(String deliverUser) {
        this.deliverUser = deliverUser;
    }

    public String getDeliverDate() {
        return deliverDate;
    }

    public void setDeliverDate(String deliverDate) {
        this.deliverDate = deliverDate;
    }

    public String getTlFlag() {
        return tlFlag;
    }

    public void setTlFlag(String tlFlag) {
        this.tlFlag = tlFlag;
    }

    public String getImgFlag() {
        return imgFlag;
    }

    public void setImgFlag(String imgFlag) {
        this.imgFlag = imgFlag;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getUnitofmeas() {
        return unitofmeas;
    }

    public void setUnitofmeas(String unitofmeas) {
        this.unitofmeas = unitofmeas;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getRfidId() {
        return rfidId;
    }

    public void setRfidId(String rfidId) {
        this.rfidId = rfidId;
    }

    public String getParentPartno() {
        return parentPartno;
    }

    public void setParentPartno(String parentPartno) {
        this.parentPartno = parentPartno;
    }

    public String getParentTrno() {
        return parentTrno;
    }

    public void setParentTrno(String parentTrno) {
        this.parentTrno = parentTrno;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getWhseDesc() {
        return whseDesc;
    }

    public void setWhseDesc(String whseDesc) {
        this.whseDesc = whseDesc;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getAlternatePartno() {
        return alternatePartno;
    }

    public void setAlternatePartno(String alternatePartno) {
        this.alternatePartno = alternatePartno;
    }

    public String getStoresloc() {
        return storesloc;
    }

    public void setStoresloc(String storesloc) {
        this.storesloc = storesloc;
    }

    public String getChildNum() {
        return childNum;
    }

    public void setChildNum(String childNum) {
        this.childNum = childNum;
    }

    public String getMeasuringNo() {
        return measuringNo;
    }

    public void setMeasuringNo(String measuringNo) {
        this.measuringNo = measuringNo;
    }

    public String getStandByPn() {
        return standByPn;
    }

    public void setStandByPn(String standByPn) {
        this.standByPn = standByPn;
    }

    public String getTlStatus() {
        return tlStatus;
    }

    public void setTlStatus(String tlStatus) {
        this.tlStatus = tlStatus;
    }

    public String getReqNo() {
        return reqNo;
    }

    public void setReqNo(String reqNo) {
        this.reqNo = reqNo;
    }

    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getTpNo() {
        return tpNo;
    }

    public void setTpNo(String tpNo) {
        this.tpNo = tpNo;
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public String getOpDate() {
        return opDate;
    }

    public void setOpDate(String opDate) {
        this.opDate = opDate;
    }

    public String getIssueNum() {
        return issueNum;
    }

    public void setIssueNum(String issueNum) {
        this.issueNum = issueNum;
    }

    public String getTdId() {
        return tdId;
    }

    public void setTdId(String tdId) {
        this.tdId = tdId;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getMechanic() {
        return mechanic;
    }

    public void setMechanic(String mechanic) {
        this.mechanic = mechanic;
    }

    public String getIssueRid() {
        return issueRid;
    }

    public void setIssueRid(String issueRid) {
        this.issueRid = issueRid;
    }
}
