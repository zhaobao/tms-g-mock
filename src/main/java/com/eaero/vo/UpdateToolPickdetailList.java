package com.eaero.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author luofeng
 * @create 2023/3/15 10:08
 */
@Data
public class UpdateToolPickdetailList {
    @NotBlank(message = "拣料单明细id必填")
    private String tpdId;
    @NotBlank(message = "批号必填")
    private String serialno;
}
