package com.eaero.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author luofeng
 * @create 2023/3/17 13:35
 */
@Data
public class BindRFID {
    @NotBlank(message = "工具顺序号必填")
    private String rid;
    @NotBlank(message = "RFID必填")
    private String rfidId;
    @NotBlank(message = "员工号与姓名必填")
    private String codeAndNmae;

    public interface Delete{}
}
