package com.eaero.vo;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author zb
 * @date 2023/03/18 018 13:47
 */
@Data
public class DoTlStockRequest {
    @NotBlank(message = "员工号，必填")
    private String userCode;
    @Valid
    private List<DoStockVo> doStock;
}
