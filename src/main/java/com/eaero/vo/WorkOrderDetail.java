package com.eaero.vo;

import com.eaero.entity.TraceWorkOrderDetail;
import lombok.Data;

/**
 * @author zzj
 * @date 2023/3/16 9:27
 * @description
 */
@Data
public class WorkOrderDetail extends TraceWorkOrderDetail {

    /**
     * 定检级别
     */
    private String visitDesc;
    /**
     * 申请状态（N 草稿/R 提交申请/D 作废/P 拣料中/U 关闭）
     */
    private String reqSts;
    /**
     * 申请来源（P:PC;A:ATM;H:手持设备;D:工作台）
     */
    private String origin;
    /**
     * 申请单备注
     */
    private String workOrderRemark;
    /**
     * 工具申请提交人
     */
    private String reqBy;
    /**
     * 申请明细更新人
     */
    private String uptBy;

}
