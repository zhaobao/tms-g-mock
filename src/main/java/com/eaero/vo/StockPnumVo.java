package com.eaero.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author zb
 * @date 2023/03/18 018 13:46
 */
@Data
public class StockPnumVo {

    private String stockPnum;
}
