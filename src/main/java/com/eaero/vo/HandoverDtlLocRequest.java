package com.eaero.vo;

import lombok.Data;

/**
 * Description:
 *
 * @author Ye Zhihong
 * @date 2023/3/14 15:27
 */
@Data
public class HandoverDtlLocRequest {

    /**
     * 交班单 ID
     */
    private Integer tsId;

    /**
     * 仓库
     */
    private String warehouse;

    /**
     * 交班日期（yyyy-mm-dd）
     */
    private String hoDate;

    /**
     * 班次
     */
    private String shift;
}
