package com.eaero.vo;

import com.eaero.entity.TraceToolDeliverSign;

import java.util.List;

/**
 * @author zzj
 * @date 2023/3/14 17:26
 * @description
 */
public class ToolDeliverSign {

    /**
     *  配送明细 ID
     */
    private String tddId;
    /**
     * 配送单号
     */
    private String deliverCode;
    /**
     * 签收流水明细
     */
    private List<TraceToolDeliverSign> signList;

    public String getTddId() {
        return tddId;
    }

    public void setTddId(String tddId) {
        this.tddId = tddId;
    }

    public String getDeliverCode() {
        return deliverCode;
    }

    public void setDeliverCode(String deliverCode) {
        this.deliverCode = deliverCode;
    }

    public List<TraceToolDeliverSign> getSignList() {
        return signList;
    }

    public void setSignList(List<TraceToolDeliverSign> signList) {
        this.signList = signList;
    }
}
