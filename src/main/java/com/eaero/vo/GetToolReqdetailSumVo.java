package com.eaero.vo;

import lombok.Data;

/**
 * @author luofeng
 * @create 2023/3/16 14:16
 */
@Data
public class GetToolReqdetailSumVo {
    private String tailNo;
    private String rcvBy;
    private String reqDate;
    private String whsType;
    private Integer reqNum;
    private Integer clsNum;
    private Integer unNum;
    private Integer issueNum;
}
