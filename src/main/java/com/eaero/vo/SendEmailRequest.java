package com.eaero.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author zzj
 * @date 2023/3/20 9:35
 * @description
 */
@Data
public class SendEmailRequest {

    /**
     * 标题 必填
     */
    @NotBlank(message = "标题必填")
    private String title;
    /**
     * 内容 必填
     */
    @NotBlank(message = "内容必填")
    private String details;
    /**
     * 收件人员工号 必填
     */
    @NotBlank(message = "收件人员工号必填")
    private String userCode;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }
}
